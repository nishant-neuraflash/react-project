module.exports = {
    "extends": ["airbnb", "prettier"],
    "plugins": ["prettier"],
    "parser": "babel-eslint",
    "rules": {
        "prettier/prettier": ["error"],
        "react/jsx-filename-extension": [1, { "extensions": [".js", ".jsx"] }],
        "linebreak-style": 0,
        "no-plusplus": [1, { "allowForLoopAfterthoughts": true }],
        "react/no-multi-comp": [0, { "ignoreStateless": false }],
        "react/no-array-index-key": 0,
        "jsx-a11y/label-has-for": 0,
        "jsx-a11y/click-events-have-key-events": 0,
        "jsx-a11y/no-static-element-interactions": 0,
    },
    "globals": {
      "document": true,
      "window": true
    }
};
