import React from 'react';
import { Route } from 'react-router';
import Layout from './components/Layout';
import Home from './components/Home/Home';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import accountActions from './actions/accountActions';
import userActions from './actions/userActions';

class App extends React.Component<any, any> {
  public componentDidMount() {
    const { accountAC, userAC } = this.props;
    accountAC.requestAccount();
    userAC.requestUser();
  }

  public render() {
    return (
      <Layout>
        <Route exact={true} path="/" component={Home} />
      </Layout>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return {
    accountAC: bindActionCreators(accountActions, dispatch),
    userAC: bindActionCreators(userActions, dispatch),
  };
}

export default connect(
  null,
  mapDispatchToProps,
)(App);
