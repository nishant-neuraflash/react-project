// get assets
export function loadAssets(props) {
  window.Connect_AssetPageController.loadAssets(
    (result, event) => {
      if (event.statusCode === 200) {
        // update assets
        props.assetActions.UpdateAssets(result);
        // update selected asset to be the first asset
        if (result !== undefined && result.length > 0) {
          props.assetActions.UpdateSelectedAsset(result[0].Id);
          props.rpsActions.UpdateAssetId(result[0].Id);
          props.eqpActions.UpdateAssetId(result[0].Id);
        }
        props.appActions.UpdateCallBackCount();
      }
    },
    { escape: false, dataType: 'json', timeout: 60000 },
  );
}

// get rope prod specs
export function loadRopeProductSpecs(props) {
  window.Connect_AssetPageController.loadRopeProductSpecs(
    (result, event) => {
      if (event.statusCode === 200) {
        props.rpsActions.UpdateRpsesAll(result);
        props.rpsActions.UpdateRpsesAsset(true, result);
        props.rpsActions.UpdateRpsesAssetFiltered(true, result);
        // update selected equipment detail to be the first equipment detail of the asset
        // props.UpdateSelectedRopeProdSpec(-1);
        props.appActions.UpdateCallBackCount();
      }
    },
    { escape: false, dataType: 'json', timeout: 60000 },
  );
}

// get equipment details
export function loadEquipmentDetails(props) {
  window.Connect_AssetPageController.loadEquipmentDetails(
    (result, event) => {
      if (event.statusCode === 200) {
        props.eqpActions.UpdateEquipmentDetailsAll(result);
        props.eqpActions.UpdateEquipmentDetailsAsset();
        props.appActions.UpdateCallBackCount();
      }
    },
    { escape: false, dataType: 'json', timeout: 60000 },
  );
}

export function loadLineMaintenanceDetails(props) {
  window.Connect_AssetPageController.loadLineMaintenanceDetails(
    (result, event) => {
      if (event.statusCode === 200) {
        props.lmdActions.UpdateLmdsAll(result);
        props.appActions.UpdateCallBackCount();
      }
    },
    { escape: false, dataType: 'json', timeout: 60000 },
  );
}

export function getCDLs(props) {
  window.Connect_AssetPageController.getCDLs(
    (cdls, event) => {
      if (event.statusCode === 200) {
        props.lmdActions.UpdateCDLs(cdls);
        props.appActions.UpdateCallBackCount();
      }
    },
    { escape: false, dataType: 'json', timeout: 60000 },
  );
}

export function updateEqpXYCord(eqp) {
  window.Connect_AssetPageController.updateEqpXYCord(JSON.stringify(eqp), () => {}, {
    escape: false,
    dataType: 'json',
    timeout: 60000,
  });
}
