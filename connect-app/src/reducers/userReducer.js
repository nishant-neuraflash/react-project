const userIS = {
  userLoading: true,
  user: null,
};

const userR = (stateP, action) => {
  const state = stateP || userIS;
  switch (action.type) {
    case 'REQUEST_USER':
      return { ...state, userLoading: true };
    case 'RECEIVE_USER':
      return { ...state, user: action.user, userLoading: false };
    default:
      return state;
  }
};

export default userR;
