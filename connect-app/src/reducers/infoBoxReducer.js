export default function reducer(state = {
  data: null,
}, action) {
  switch (action.type) {
    // update data
    case 'UPDATE_DATA':
      return {
        ...state,
        data: action.data,
      };
    default:
      return state;
  }
}
