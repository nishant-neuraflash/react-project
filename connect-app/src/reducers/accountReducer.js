const accountIS = {
  accountLoading: true,
  account: null,
};

const accountR = (stateP, action) => {
  const state = stateP || accountIS;
  switch (action.type) {
    case 'REQUEST_ACCOUNT':
      return { ...state, accountLoading: true };
    case 'RECEIVE_ACCOUNT':
      return { ...state, account: action.account, accountLoading: false };
    default:
      return state;
  }
};

export default accountR;
