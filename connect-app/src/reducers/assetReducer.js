export default function reducer(state = {
  assets: null,
  selectedAsset: null,
}, action) {
  switch (action.type) {
    // update assets
    case 'UPDATE_ASSETS':
      return {
        ...state,
        assets: action.assets,
      };
    // update selected asset
    case 'UPDATE_SELECTED_ASSET': {
      return {
        ...state,
        selectedAsset: state.assets.find(x => x.Id === action.selectedAssetId),
      };
    }
    default:
      return state;
  }
}
