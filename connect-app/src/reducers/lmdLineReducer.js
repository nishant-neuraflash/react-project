export default function reducer(state = {
  lmdLine: null,
}, action) {
  switch (action.type) {
    case 'UPDATE_LMDS_LINE': {
      return {
        ...state,
        lmdsLine: action.lmdsLine,
      };
    }
    default:
      return state;
  }
}
