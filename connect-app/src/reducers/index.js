import accountR from './accountReducer';
import userR from './userReducer';
import assetReducer from './assetReducer';
import rpsReducerAll from './rpsReducerAll';
import rpsReducerAsset from './rpsReducerAsset';
import rpsReducerSel from './rpsReducerSel';
import eqpReducer from './eqpReducer';
import lmdReducer from './lmdReducer';
import lmdLineReducer from './lmdLineReducer';
import infoBoxReducer from './infoBoxReducer';
import appReducer from './appReducer';
import headerReducer from './headerReducer';
import dataReducer from './dataReducer';

export default {
  userR,
  accountR,
  assetReducer,
  rpsReducerAll,
  rpsReducerAsset,
  rpsReducerSel,
  eqpReducer,
  lmdReducer,
  lmdLineReducer,
  infoBoxReducer,
  appReducer,
  headerReducer,
  dataReducer,
};
