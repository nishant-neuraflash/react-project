export default function reducer(state = {
  path: '',
}, action) {
  switch (action.type) {
    // update path
    case 'UPDATE_PATH':
      return {
        ...state,
        path: action.path,
      };
    default:
      return state;
  }
}
