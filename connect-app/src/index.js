import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap-theme.css';
import './index.css';
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'react-router-redux';
import createHashHistory from 'history/createHashHistory';
import configureStore from './store';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

const history = createHashHistory();
const initialState = window.initialReduxState;
const store = configureStore(history, initialState);
const rootElement = document.getElementById('root');

if (process.env.NODE_ENV !== 'production') {
  console.log('Looks like we are in development mode!');
}

ReactDOM.render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <App {...root.dataset} />
    </ConnectedRouter>
  </Provider>,
  rootElement,
);

registerServiceWorker();
