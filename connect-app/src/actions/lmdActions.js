
// update line maintenance detials all
export function UpdateLmdsAll(lmdsAll) {
  return {
    type: 'UPDATE_LMDS_ALL',
    lmdsAll,
  };
}

// update lmd line
export function UpdateLmdsLine(lmdsLine) {
  return {
    type: 'UPDATE_LMDS_LINE',
    lmdsLine,
  };
}

// update content document links
export function UpdateCDLs(cdls) {
  return { type: 'UPDATE_CDLS', cdls };
}
