const userActions = {
  requestUser: () => async (dispatch) => {
    dispatch({ type: 'REQUEST_USER' });
    window.Connect_Controller.getUser(
      (user, event) => {
        if (event.statusCode === 200) {
          dispatch({ type: 'RECEIVE_USER', user });
        }
      },
      { escape: false, dataType: 'json', timeout: 60000 },
    );
  },
};

export default userActions;
