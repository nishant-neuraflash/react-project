
// update all assets
export function UpdateAllAssets(allAssets) {
  return {
    type: 'UPDATE_ALL_ASSETS',
    allAssets,
  };
}

// update assets
export function UpdateAssets(assets) {
  return {
    type: 'UPDATE_ASSETS',
    assets,
  };
}

// update selected asset
export function UpdateSelectedAsset(selectedAssetId) {
  return {
    type: 'UPDATE_SELECTED_ASSET',
    selectedAssetId,
  };
}
