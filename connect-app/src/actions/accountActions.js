const accountActions = {
  requestAccount: () => async (dispatch) => {
    dispatch({ type: 'REQUEST_ACCOUNT' });
    window.Connect_Controller.getAccount(
      (account, event) => {
        if (event.statusCode === 200) {
          dispatch({ type: 'RECEIVE_ACCOUNT', account });
        }
      },
      { escape: false, dataType: 'json', timeout: 60000 },
    );
  },
};

export default accountActions;
