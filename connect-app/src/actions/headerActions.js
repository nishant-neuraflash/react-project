
export function UpdateToggle(toggle) {
  return {
    type: 'UPDATE_TOGGLE',
    toggle,
  };
}

export function dummy() {
  return '';
}
