
// extract folder path from resource
export function getPath(url) {
  const lastIndex = url.lastIndexOf('/');
  return url.substring(0, lastIndex + 1);
}

export function getProperty(propertyName, object) {
  const parts = propertyName.split('.');
  let i;

  let property = object || this;
  for (i = 0; i < parts.length; i++) {
    if (!property) {
      return '';
    }
    property = property[parts[i]];
  }
  return property;
}

// get salesforce environment
export function environment() {
  return window.parent.location.href.includes('sambox') ? 'sandbox' : 'production';
}
