export function getMaxLineLength(text) {
  const lines = text.split('\n');
  let maxLine = -1;
  const count = lines.length;
  let i = 0;

  for (i = 0; i < count; i++) {
    if (lines[i].length > maxLine) {
      maxLine = lines[i].length;
    }
  }

  return maxLine;
}

export function getLineCount(text) {
  return text.split('\n').length;
}
