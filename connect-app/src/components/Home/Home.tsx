import React from 'react';
import PropTypes from 'prop-types';
// styles
import styles from './Home.less';
import lineCareIcon from '../../images/HOME_LineCare_60px.png';
import classroomIcon from '../../images/HOME_Classroom_60px.png';
import complianceIcon from '../../images/HOME_Compliance_60px.png';
import maintActIcon from '../../images/HOME_Maintenance_Activities_Square_CYAN_60px.png';
import vesselsIcon from '../../images/HOME_Vessels_CYAN_60px.png';
import certificatesIcon from '../../images/HOME_Certificates_CYAN_80_60px.png';
import lineTrackerIcon from '../../images/HOME_LineTracker_Square_CYAN_60px.png';
import invoicesIcon from '../../images/HOME_Invoices_CYAN_60_70px.png';
import contSuppIcon from '../../images/Connect_HELP_Icon_90_60px.png';

const iconImages = [
  lineCareIcon,
  classroomIcon,
  complianceIcon,
  maintActIcon,
  vesselsIcon,
  certificatesIcon,
  lineTrackerIcon,
  invoicesIcon,
  contSuppIcon,
];

const names = [
  'LineCare',
  'Classroom',
  'Compliance',
  'Maintenance Activities',
  'Vessels',
  'Certificates',
  'LineTracker',
  'Invoices',
  'Contact Support',
];

const Home = () => {
  const rows = [];
  // for every row
  for (let i = 0; i < 3; i++) {
    const items = []; // items inside the row
    // for every item inside a row
    for (let j = 0; j < 3; j++) {
      const name = names[i * 3 + j];
      const image = iconImages[i * 3 + j];
      const item = (
        <div key={`item_${name}`} className={styles.gridItemStyle}>
          <div key={`icon_${name}`} className={styles.buttonStyle}>
            <img key={`img_${name}`} src={image} alt={name} />
            <span key={`span_${name}`} className={styles.captionStyle} style={{ width: '110px' }}>
              {name}
            </span>
          </div>
        </div>
      );
      items.push(item);
    }
    rows.push(
      <div key={`${i}`} className={styles.gridStyle}>
        {' '}
        {items}{' '}
      </div>,
    );
  }
  return <div className={styles.cardStyle}>{rows}</div>;
};

export default Home;
