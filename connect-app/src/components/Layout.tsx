import React from 'react';
import { Col, Grid, Row } from 'react-bootstrap';
import MyNav from './Navbar';
import Header from './Header/Header';

export default ({ children }) => (
  <Grid fluid={true}>
    <Row>
      <Col sm={3}>
        <MyNav />
      </Col>
      <Col sm={9}>
        <Row>
          <Header />
        </Row>
        <Row>{children}</Row>
        <Row>FooterS</Row>
      </Col>
    </Row>
  </Grid>
);
