import React from 'react';
import { toNumber, degreeOf3Points } from '../../../../helpers/Math';
import { updateEqpXYCord } from '../../../../controllers/controller';

function withDrapAndDrop(WrappedComponent) {
  return class extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        x: toNumber(this.props.data.X_Coordinate__c),
        y: toNumber(this.props.data.Y_Coordinate__c),
        dragging: false,
        rel: null,
        initClickPos: null, // initial click position for rotation
        degree: 0,
      };
    }

    componentDidUpdate(prevProps, prevState) {
      if (this.state.dragging && !prevState.dragging) {
        document.addEventListener('mousemove', this.handleMouseMove);
        document.addEventListener('mouseup', this.handleMouseUp);
      } else if (!this.state.dragging && prevState.dragging) {
        document.removeEventListener('mousemove', this.handleMouseMove);
        document.removeEventListener('mouseup', this.handleMouseUp);
      }
    }

    handleMouseDown = (e) => {
      if (e.button !== 0) return;
      if (e.ctrlKey) {
        this.setState({
          dragging: true,
          initClickPos: {
            x: e.pageX,
            y: e.pageY,
          },
        });
      } else {
        this.setState({
          dragging: true,
          rel: {
            x: e.pageX - this.state.x,
            y: e.pageY - this.state.y,
          },
        });
      }
      e.stopPropagation();
      e.preventDefault();
    };

    handleMouseUp = (e) => {
      const rotation = (toNumber(this.props.data.Rotation__c) + this.state.degree) % 360;
      const eqp = {
        id: this.props.data.Id,
        x: this.state.x,
        y: this.state.y,
        rotation,
      };
      updateEqpXYCord(eqp);
      this.props.handleUpdateEqpXYCord(eqp);
      this.setState({ dragging: false, degree: 0 });
      e.stopPropagation();
      e.preventDefault();
    };

    handleMouseMove = (e) => {
      if (!this.state.dragging) return;
      // the ctrl key is pressed so update the rotation
      if (e.ctrlKey) {
        const p1 = { x: this.state.x + 20, y: this.state.y + 81 };
        const p2 = this.state.initClickPos;
        const p3 = { x: e.pageX, y: e.pageY };
        const degree = degreeOf3Points(p1, p2, p3);
        this.setState({ degree });
        // the ctrl key is not pressed so update the x,y coordinates
      } else {
        this.setState({
          x: Math.min(1040, Math.max(0, e.pageX - this.state.rel.x)),
          y: Math.min(190, Math.max(0, e.pageY - this.state.rel.y)),
        });
      }
      e.stopPropagation();
      e.preventDefault();
    };

    render() {
      const { x, y, degree } = this.state;
      const rotation = toNumber(this.props.data.Rotation__c) + degree;
      return (
        <WrappedComponent
          {...this.props}
          x={x}
          y={y}
          rotation={rotation}
          handleMouseDown={this.handleMouseDown}
        />
      );
    }
  };
}

export default withDrapAndDrop;
