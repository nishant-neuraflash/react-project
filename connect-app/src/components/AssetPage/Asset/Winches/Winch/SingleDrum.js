import React from 'react';
import PropTypes from 'prop-types';
// helpers
import { toNumber } from '../../../../../helpers/Math';
import { getTransform } from '../../../../../helpers/EquipmentDetailHelper';

const SingleDrum = (props) => {
  const x = toNumber(props.x);
  const y = toNumber(props.y);
  const maxEqupVer = Math.max(3, toNumber(props.maxEqupVer));
  const vesselVerWidth = 146;
  const eqpWidth = (vesselVerWidth - maxEqupVer * 8) / maxEqupVer;
  const radius = eqpWidth * 0.9;
  const circleDisX = eqpWidth * 0.5;
  const circleDisY = eqpWidth * 0.4;
  const wallHeight = eqpWidth * 0.9;
  const wallWidth = eqpWidth * 0.15;
  const drumHeight = eqpWidth * 0.3;
  const drumWidth = eqpWidth * 0.7;
  const width = wallWidth * 2 + drumWidth;

  return (
    <g>
      {props.glow && (
        <circle
          key={`${props.id}_glow`}
          cx={x + circleDisX}
          cy={y + circleDisY}
          r={radius}
          style={{ stroke: '#00AEEF', strokeWidth: '3px', strokeDasharray: '3' }}
          fill="#FFF"
        />
      )}
      <g
        style={{ cursor: 'pointer' }}
        onClick={props.handleClick}
        onMouseDown={props.handleMouseDown}
        transform={getTransform(props.rotation, x, y, width, wallHeight)}
      >
        {/* rec 1 */}
        <rect
          x={x}
          y={y}
          width={wallWidth}
          height={wallHeight}
          fill={props.wallFillColor}
          strokeWidth="1"
          stroke="rgb(0, 0, 0)"
        />
        {/* rec 2 */}
        <rect
          x={x + wallWidth}
          y={y + wallHeight / 3}
          width={drumWidth}
          height={drumHeight}
          fill={props.drumFillColor}
          strokeWidth="1"
          stroke="rgb(0, 0, 0)"
        />
        {/* rec 3 */}
        <rect
          x={x + wallWidth + drumWidth}
          y={y}
          width={wallWidth}
          height={wallHeight}
          fill={props.wallFillColor}
          strokeWidth="1"
          stroke="rgb(0, 0, 0)"
        />
      </g>
    </g>
  );
};

SingleDrum.defaultProps = {
  rotation: 0,
};

SingleDrum.propTypes = {
  id: PropTypes.string.isRequired,
  wallFillColor: PropTypes.string.isRequired,
  drumFillColor: PropTypes.string.isRequired,
  x: PropTypes.number.isRequired,
  y: PropTypes.number.isRequired,
  maxEqupVer: PropTypes.string.isRequired,
  rotation: PropTypes.number,
  glow: PropTypes.bool.isRequired,
  handleClick: PropTypes.func.isRequired, // eslint-disable-line react/forbid-prop-types
  handleMouseDown: PropTypes.func.isRequired, // eslint-disable-line react/forbid-prop-types
};

export default SingleDrum;
