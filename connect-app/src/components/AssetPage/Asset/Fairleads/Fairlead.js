import React from 'react';
import PropTypes from 'prop-types';
import withDrapAndDrop from '../Equipment/Equipment';
// helpers
import { toNumber } from '../../../../helpers/Math';
import { getEqpFillColor } from '../../../../helpers/EquipmentDetailHelper';

const Fairlead = (props) => {
  const { data, selEqpId, x, y } = props;
  const { Id, Asset__r: asset, Disposition__c: disposition } = data;
  const glow = selEqpId === Id;
  let raduis = 0;
  const maxEqupVer = Math.max(3, toNumber(asset.Max_Num_Equip_Vertical__c));
  const vesselVerWidth = 146;
  const eqpWidth = vesselVerWidth / maxEqupVer;
  raduis = eqpWidth / 6;

  return (
    <g>
      {glow && (
        <circle
          key={`${Id}_glow`}
          cx={x}
          cy={y}
          r={raduis + 4}
          style={{ stroke: '#00AEEF', strokeWidth: '3px', strokeDasharray: '3' }}
          fill="#FFF"
        />
      )}
      <circle
        key={data.Id}
        cx={x}
        cy={y}
        r={raduis}
        stroke="black"
        strokeWidth="1"
        fill={getEqpFillColor(disposition)}
        style={{ cursor: 'pointer' }}
        onClick={props.handleClick(data)}
        onMouseDown={props.handleMouseDown}
      />
    </g>
  );
};

Fairlead.propTypes = {
  data: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
  handleClick: PropTypes.func.isRequired, // eslint-disable-line react/forbid-prop-types
  selEqpId: PropTypes.string.isRequired,
  x: PropTypes.number.isRequired,
  y: PropTypes.number.isRequired,
  handleMouseDown: PropTypes.func.isRequired, // eslint-disable-line react/forbid-prop-types
};

export default withDrapAndDrop(Fairlead);
