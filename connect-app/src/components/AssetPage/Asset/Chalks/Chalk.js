import React from 'react';
import PropTypes from 'prop-types';
import withDrapAndDrop from '../Equipment/Equipment';
// helpers
import { getEqpFillColor, getTransform } from '../../../../helpers/EquipmentDetailHelper';

const Chalk = (props) => {
  const { data, selEqpId, x, y, rotation } = props;
  const { Id, Disposition__c: disposition } = data;
  const glow = selEqpId === Id;
  return (
    <g>
      {glow && (
        <circle
          key={`${Id}_glow`}
          cx={x + 6}
          cy={y + 3}
          r={10}
          style={{ stroke: '#00AEEF', strokeWidth: '3px', strokeDasharray: '3' }}
          fill="#FFF"
        />
      )}
      <rect
        key={Id}
        x={x}
        y={y}
        rx={2}
        ry={2}
        width={12}
        height={6}
        transform={getTransform(rotation, x, y, 12, 6)}
        fill={getEqpFillColor(disposition, 'Chalk')}
        strokeWidth="1"
        stroke="#000"
        style={{ cursor: 'pointer' }}
        onClick={props.handleClick(data)}
        onMouseDown={props.handleMouseDown}
      />
    </g>
  );
};

Chalk.propTypes = {
  data: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
  selEqpId: PropTypes.string.isRequired,
  x: PropTypes.number.isRequired,
  y: PropTypes.number.isRequired,
  rotation: PropTypes.number.isRequired,
  handleClick: PropTypes.func.isRequired, // eslint-disable-line react/forbid-prop-types
  handleMouseDown: PropTypes.func.isRequired, // eslint-disable-line react/forbid-prop-types
};

export default withDrapAndDrop(Chalk);
