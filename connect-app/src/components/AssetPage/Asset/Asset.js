/* eslint no-underscore-dangle: ["error", { "allow": ["_stage"] }] */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
// components
import Fairleads from './Fairleads/Fairleads';
import Winches from './Winches/Winches';
import Chalks from './Chalks/Chalks';
// actions
import * as infoBoxActions from '../../../actions/infoBoxActions';
import * as rpsActions from '../../../actions/rpsActions';
import * as eqpActions from '../../../actions/eqpActions';
// styles
import styles from './Asset.less';

class EquipmentDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  handleWinchClick = (winch) => () => {
    const { toggle } = this.props.headerReducer;
    let mainline = null;
    let tail = null;
    const infoBoxData = { equipment: winch, line: null, tail: null };
    if (winch.Rope_Product_Specs__r) {
      mainline = winch.Rope_Product_Specs__r.find((x) => x.Rope_Line__c === 'Mainline');
      tail = winch.Rope_Product_Specs__r.find((x) => x.Rope_Line__c === 'Mooring Tails');
    }

    if (mainline) {
      mainline = this.props.rpsReducerAsset.rpsesAsset.find((x) => x.Id === mainline.Id);
      infoBoxData.line = mainline;
    }
    if (tail) {
      tail = this.props.rpsReducerAsset.rpsesAsset.find((x) => x.Id === tail.Id);
      infoBoxData.tail = tail;
    }

    if (toggle === 'Mainline') {
      this.props.rpsActions.UpdateSelectedRps(mainline); // reset selected rope prod spec
    } else {
      this.props.rpsActions.UpdateSelectedRps(tail); // reset selected rope prod spec
    }
    this.props.eqpActions.UpdateSelEqpId(winch.Id);
    this.props.infoBoxActions.UpdateInfoBoxData(infoBoxData);
  };

  handleFairleadClick = (fairlead) => () => {
    this.props.eqpActions.UpdateSelEqpId(fairlead.Id);
    this.props.rpsActions.UpdateSelectedRps(null); // reset selected rope prod spec
    this.props.infoBoxActions.UpdateInfoBoxData({ equipment: fairlead });
  };

  handleChalkClick = (chalk) => () => {
    this.props.eqpActions.UpdateSelEqpId(chalk.Id);
    this.props.rpsActions.UpdateSelectedRps(null); // reset selected rope prod spec
    this.props.infoBoxActions.UpdateInfoBoxData({ equipment: chalk });
  };

  handleUpdateEqpXYCord = (equipment) => {
    const eqpAsset = this.props.eqpReducer.equipmentDetailsAsset;
    const eqpIndex = eqpAsset.findIndex((x) => x.Id === equipment.id);
    eqpAsset[eqpIndex].X_Coordinate__c = equipment.x;
    eqpAsset[eqpIndex].Y_Coordinate__c = equipment.y;
    eqpAsset[eqpIndex].Rotation__c = equipment.rotation;
    this.props.eqpActions.UpdateEquipmentDetailsAsset(null, eqpAsset); // update asset equipments
  };

  render() {
    const eqpAsset = this.props.eqpReducer.equipmentDetailsAsset;
    const { rpsesAssetFiltered } = this.props.rpsReducerAsset;
    const { rpsesAsset } = this.props.rpsReducerAsset;

    if (!eqpAsset || !rpsesAssetFiltered) {
      return null;
    }
    const fairleads = eqpAsset.filter((x) => x.RecordType.Name === 'Fairlead - Pedestal Fairleads');
    const chalks = eqpAsset.filter(
      (x) => x.RecordType.Name !== 'Winch' && x.RecordType.Name !== 'Fairlead - Pedestal Fairleads',
    );
    const winches = eqpAsset.filter((x) => x.RecordType.Name === 'Winch');

    return (
      <div>
        <div className={styles.cardStyle}>
          <div>
            <div className={styles.boxStyle} style={{ backgroundColor: '#41AD49' }} />
            <div style={{ float: 'left' }}>Continue Use</div>
            <div className={styles.boxStyle} style={{ backgroundColor: '#FFD400' }} />
            <div style={{ float: 'left' }}>Under Review</div>
            <div className={styles.boxStyle} style={{ backgroundColor: '#D2232A' }} />
            <div style={{ float: 'left' }}>Repair/Upgrade/Replace</div>
            <div className={styles.boxStyle} style={{ backgroundColor: '#909090' }} />
            <div style={{ float: 'left' }}>Non-Samson Product</div>
          </div>
          <svg width={1050} height={204}>
            {/* Filter goes here */}

            <image
              x={0}
              y={0}
              xlinkHref={`${this.props.dataReducer.path}asset.png`}
              width={1050}
              height={204}
            />
            {/* draw Fairleads */}
            <Fairleads
              data={fairleads}
              selEqpId={this.props.eqpReducer.selEqpId}
              handleClick={this.handleFairleadClick}
              handleUpdateEqpXYCord={this.handleUpdateEqpXYCord}
            />
            {/* draw Winches */}
            <Winches
              data={winches}
              rpsesAsset={rpsesAsset}
              selEqpId={this.props.eqpReducer.selEqpId}
              handleClick={this.handleWinchClick}
              handleUpdateEqpXYCord={this.handleUpdateEqpXYCord}
            />
            {/* draw Chalks */}
            <Chalks
              data={chalks}
              selEqpId={this.props.eqpReducer.selEqpId}
              handleClick={this.handleChalkClick}
              handleUpdateEqpXYCord={this.handleUpdateEqpXYCord}
            />
          </svg>
        </div>
      </div>
    );
  }
}

EquipmentDetails.propTypes = {
  eqpReducer: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
  dataReducer: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
  rpsReducerAsset: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types

  rpsActions: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
  infoBoxActions: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
  eqpActions: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
  headerReducer: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
};

const mapStateToProps = (state) => ({
  eqpReducer: state.eqpReducer,
  dataReducer: state.dataReducer,
  rpsReducerAsset: state.rpsReducerAsset,
  rpsReducerSel: state.rpsReducerSel,
  headerReducer: state.headerReducer,
});

function mapDispatchToProps(dispatch) {
  return {
    rpsActions: bindActionCreators(rpsActions, dispatch),
    infoBoxActions: bindActionCreators(infoBoxActions, dispatch),
    eqpActions: bindActionCreators(eqpActions, dispatch),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(EquipmentDetails);
