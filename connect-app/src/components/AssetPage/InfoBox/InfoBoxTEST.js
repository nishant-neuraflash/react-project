import React from 'react';


class InfoBox extends React.Component {
  constructor(props) {
    super(props);
    this.state = { id: 1 };
  }

  render() {
    return (
      <a
        id={this.state.id}
        rel="noopener noreferrer"
        href="http://samsonrope.com"
        onContextMenu={() => { this.setState(prevState => ({ id: prevState.id + 1 })); }}
      >Details
      </a>
    );
  }
}

export default InfoBox;
