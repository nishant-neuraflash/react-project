import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';

import * as assetActions from '../../../actions/assetActions';
import * as rpsActions from '../../../actions/rpsActions';
import * as eqpActions from '../../../actions/eqpActions';
import * as headerActions from '../../../actions/headerActions';
import * as infoBoxActions from '../../../actions/infoBoxActions';
import * as appActions from '../../../actions/appActions';

// styles
import styles from './Header.less';
// helpers
import { environment } from '../../../helpers/helper';

// const grayButton = {
//   backgroundColor: '#e7e7e7',
//   background: '#e7e7e7',
//   border: 'none',
//   color: '#000000',
//   padding: '1px',
//   textAlign: 'center',
//   textDecoration: 'none',
//   display: 'inline-block',
//   margin: '.5px',
//   cursor: 'pointer',
//   width: '70px',
// };
//
// const greenButton = {
//   backgroundColor: '#00AEEF',
//   background: '#00AEEF',
//   border: '1px solid #00AEEF',
//   boxShadow: '0 0 5px #00AEEF',
//   color: '#ffffff',
//   padding: '1px',
//   textAlign: 'center',
//   textDecoration: 'none',
//   display: 'inline-block',
//   margin: '.5px',
//   cursor: 'pointer',
//   width: '70px',
// };

// function getOptionValue(rps) {
//   let position = '';
//   const lmds = rps.Line_Maintenance_Details__r;
//   if (lmds && lmds.length > 0) {
//     position = lmds[0].Line_Position__c;
//   }
//   let text = rps.Rope_Line__c;
//   if (position) {
//     text += ` - ${position}`;
//   }
//   text += ` - ${rps.Equipment_Detail__r.Name}`;
//   return text;
// }

class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      lineType: 'Mainline',
    };
    if (environment() === 'production') {
      this.selectURL = 'https://samsonportal.force.com/connect/resource/Select_Arrow';
      this.commLink = 'https://samsonportal.force.com/connect/s/'; // production
    } else {
      this.selectURL = 'https://sambox-samsonportal.cs21.force.com/chevron/resource/Select_Arrow';
      this.commLink = 'https://sambox-samsonportal.cs21.force.com/chevron/s/'; // sandbox
    }
  }

  componentWillMount() {
    if (this.props.assetId) {
      this.changeAsset(this.props.assetId);
    } else {
      const { rpsesAssetFiltered } = this.props.rpsReducerAsset;
      const eqpsAsset = this.props.eqpReducer.equipmentDetailsAsset;
      this.updateRpsAndLmd(rpsesAssetFiltered, eqpsAsset);
    }
  }

  handleChangeAsset = (event) => {
    this.changeAsset(event.target.value);
  };

  changeAsset = (assetId) => {
    this.props.assetActions.UpdateSelectedAsset(assetId); // update asset
    const rpsesAsset = this.props.rpsReducerAll.rpsesAll.filter((x) => x.Asset__c === assetId);
    this.props.rpsActions.UpdateRpsesAsset(false, rpsesAsset); // update rpses
    const rpsesAssetFiltered = rpsesAsset.filter((x) => x.Rope_Line__c === this.state.lineType);
    this.props.rpsActions.UpdateRpsesAssetFiltered(false, rpsesAssetFiltered);
    this.props.eqpActions.UpdateEquipmentDetailsAsset(assetId); // update equipments
    const eqpsAsset = this.props.eqpReducer.equipmentDetailsAll.filter(
      (x) => x.Asset__c === assetId,
    );
    this.updateRpsAndLmd(rpsesAssetFiltered, eqpsAsset);
  };

  updateRpsAndLmd = (rpsesAssetFiltered, eqpsAsset) => {
    let selectedRps = null;
    let winch = null;
    if (rpsesAssetFiltered && rpsesAssetFiltered.length > 0) {
      [selectedRps] = rpsesAssetFiltered;
      winch = eqpsAsset.find((x) => x.Id === selectedRps.Equipment_Detail__c);
    }
    this.props.rpsActions.UpdateSelectedRps(selectedRps); // update selected rope prod spec
    this.props.eqpActions.UpdateSelEqpId(winch ? winch.Id : ''); // update selected winch
    this.updateInfoBox(selectedRps, winch); // update info box with selected rps
  };

  // handleChangeRopeProdSpec = (event) => {
  //   const rps =
  //   this.props.rpsReducerAsset.rpsesAssetFiltered.find(x => x.Id === event.target.value);
  //   const winch =
  //   this.props.eqpReducer.equipmentDetailsAsset.find(x => x.Id === rps.Equipment_Detail__c);
  //
  //   this.props.rpsActions.UpdateSelectedRps(rps); // update selected rps
  //   this.props.eqpActions.UpdateSelEqpId(winch ? winch.Id : ''); // update selected winch
  //   this.updateInfoBox(rps, winch); // update info box with selected rps
  // };

  // update the info box with the given rps
  updateInfoBox = (rps, winch) => {
    let infoBoxData = null;
    if (rps) {
      infoBoxData = { line: null, tail: null };
      // assign the selected mainline/tail
      if (rps.Rope_Line__c === 'Mainline') {
        infoBoxData.line = rps;
      } else {
        infoBoxData.tail = rps;
      }
      // get the mainline or tail from the winch
      if (winch) {
        infoBoxData.equipment = winch;
        let mainline = null;
        let tail = null;
        // if winch has rope prod specs
        if (winch.Rope_Product_Specs__r) {
          mainline = winch.Rope_Product_Specs__r.find((x) => x.Rope_Line__c === 'Mainline');
          tail = winch.Rope_Product_Specs__r.find((x) => x.Rope_Line__c === 'Mooring Tails');
        }
        // if the winch has a mainline and the passed rps is not a line
        if (mainline && !infoBoxData.line) {
          infoBoxData.line = this.props.rpsReducerAsset.rpsesAsset.find(
            (x) => x.Id === mainline.Id,
          );
        }
        // if the winch has a tail and the passed rps is not a tail
        if (tail && !infoBoxData.tail) {
          infoBoxData.tail = this.props.rpsReducerAsset.rpsesAsset.find((x) => x.Id === tail.Id);
        }
      }
    }
    this.props.infoBoxActions.UpdateInfoBoxData(infoBoxData);
  };

  // handleFleetClick = () => {
  //   this.props.appActions.UpdatePageStatus({ View: 'Fleet', FleetId:
  // this.props.assetReducer.selectedAsset.Fleet__r.Id });
  // };

  handleLmpClilck = (assetId) => () => {
    window.parent.location.href = `${
      this.commLink
    }connect-reports?assetId=${assetId}&workTypeNameId=Line Management Plan`;
  };

  handleMsClilck = (assetId) => () => {
    window.parent.location.href = `${this.commLink}maintenance-activities?assetId=${assetId}`;
  };

  handleReportsClilck = (assetId) => () => {
    window.parent.location.href = `${this.commLink}connect-reports?assetId=${assetId}`;
  };

  // handlMainlineClick = () => {
  //   this.handleToggleClick('Mainline');
  // };
  //
  // handlTailClick = () => {
  //   this.handleToggleClick('Mooring Tails');
  // };

  // handleToggleClick = (lineType) => {
  //   if (this.state.lineType !== lineType) {
  //     const rpsesAssetFiltered = this.props.rpsReducerAsset.rpsesAsset.filter(x =>
  //       x.Rope_Line__c === lineType);
  //     this.props.headerActions.UpdateToggle(lineType);
  //     this.props.rpsActions.UpdateRpsesAssetFiltered(false, rpsesAssetFiltered);
  //     const { selEqpId } = this.props.eqpReducer;
  //     let selectedRps = null;
  //     if (rpsesAssetFiltered && selEqpId) {
  //       selectedRps = rpsesAssetFiltered.find(x => x.Equipment_Detail__c === selEqpId);
  //     }
  //     this.props.rpsActions.UpdateSelectedRps(selectedRps); // update selected rope prod spec
  //     this.setState({ lineType });
  //   }
  // }

  render() {
    const assetR = this.props.assetReducer;
    // // const rpsRedAsset = this.props.rpsReducerAsset;
    // const rpsRedSel = this.props.rpsReducerSel;
    // // filter rpses to only those that are assigned to an eqp
    // const rpsesAssetFiltered =
    // this.props.rpsReducerAsset.rpsesAssetFiltered.filter(x => x.Equipment_Detail__c);
    if (!assetR.assets || assetR.assets.length === 0 || !assetR.selectedAsset) {
      if (!assetR.assets || assetR.assets.length === 0) {
        return <div> No Assets! </div>;
      }
      return <div> No Selected Asset! </div>;
    }
    // const assetType = `${assetR.selectedAsset.Type_of_Asset__c},
    // ${assetR.selectedAsset.Asset_Class__c}`;
    return (
      <div className={styles.cardStyle}>
        <div className={styles.gridStyle}>
          {/* (Column 1) Vessel Dropdown List */}
          <div className={styles.gridItemStyle}>
            <div className={styles.selectContainer}>
              <label className={styles.selectLableStyle}>Vessels</label>
              <select
                value={assetR.selectedAsset.Id}
                onChange={this.handleChangeAsset}
                className={styles.selectStyle}
                style={{ backgroundImage: `url(${this.selectURL})` }}
              >
                {assetR.assets.map((asset, index) => (
                  <option key={index} value={asset.Id}>
                    {asset.Name}
                  </option>
                ))}
              </select>
            </div>
          </div>

          {/* (Column 2) Lines Dropdown List */}
          <div className={styles.gridItemStyle}>
            <div>
              {/* <div className={styles.selectContainer}>
                <label className={styles.selectLableStyle}>Lines</label>
                <select
                  value={rpsRedSel.selectedRps ? rpsRedSel.selectedRps.Id : ''}
                  onChange={this.handleChangeRopeProdSpec}
                  className={styles.selectStyle}
                  style={{ backgroundImage: `url(${this.selectURL})` }}
                >
                  {rpsesAssetFiltered &&
                    rpsesAssetFiltered.map((rps, index) => (
                      <option key={index} value={rps.Id}>{getOptionValue(rps)}</option>
                    ))
                  }
                </select>
              </div> */}
            </div>
          </div>

          {/* (Column 3) Reports Button */}
          <div className={styles.gridItemStyle}>
            <div
              className={styles.buttonStyle}
              onClick={this.handleReportsClilck(assetR.selectedAsset.Id)}
            >
              <img src={`${this.props.dataReducer.path}Report.png`} alt="Reports" width="35" />
              Reports
            </div>
          </div>

          {/* (Column 4) Line Management Plan */}
          <div className={styles.gridItemStyle}>
            <div
              className={styles.buttonStyle}
              onClick={this.handleLmpClilck(assetR.selectedAsset.Id)}
            >
              <img
                src={`${this.props.dataReducer.path}Compliance.png`}
                alt="Line Management Plan"
                width="35"
              />
              Line Management Plan
            </div>
          </div>

          {/* (Column 5) Maintenance Schedule */}
          <div className={styles.gridItemStyle}>
            <div
              className={styles.buttonStyle}
              onClick={this.handleMsClilck(assetR.selectedAsset.Id)}
            >
              <img
                src={`${this.props.dataReducer.path}Calendar.png`}
                alt="Maintenance Schedule"
                width="32"
              />
              Maintenance Schedule
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Header.defaultProps = {
  assetId: '',
};

Header.propTypes = {
  assetReducer: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
  rpsReducerAll: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
  rpsReducerAsset: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
  rpsReducerSel: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
  eqpReducer: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
  dataReducer: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types

  assetActions: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
  rpsActions: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
  eqpActions: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
  infoBoxActions: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
  headerActions: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
  appActions: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types

  assetId: PropTypes.string,
};

const mapStateToProps = (state) => ({
  assetReducer: state.assetReducer,
  rpsReducerAll: state.rpsReducerAll,
  rpsReducerAsset: state.rpsReducerAsset,
  rpsReducerSel: state.rpsReducerSel,
  eqpReducer: state.eqpReducer,
  dataReducer: state.dataReducer,
});

function mapDispatchToProps(dispatch) {
  return {
    assetActions: bindActionCreators(assetActions, dispatch),
    rpsActions: bindActionCreators(rpsActions, dispatch),
    eqpActions: bindActionCreators(eqpActions, dispatch),
    infoBoxActions: bindActionCreators(infoBoxActions, dispatch),
    headerActions: bindActionCreators(headerActions, dispatch),
    appActions: bindActionCreators(appActions, dispatch),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Header);
