import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Table from 'custom_react_table';
import { bindActionCreators } from 'redux';
import * as rpsActions from '../../../actions/rpsActions';
import * as eqpActions from '../../../actions/eqpActions';
import * as infoBoxActions from '../../../actions/infoBoxActions';

const columns = [
  {
    id: 'Cert__r.Name',
    head: {
      type: 'TEXT',
      info: { label: 'CERTIFICATE #' },
    },
    body: {
      type: 'LINK',
      info: { link: 'https://samsonportal.force.com/connect/s/detail/', linkId: 'Cert__r.Id' },
    },
  },
  {
    id: 'Product_List__c',
    head: {
      type: 'TEXT',
      info: { label: 'PRODUCT NAME' },
    },
    body: {
      type: 'TEXT',
      info: {},
    },
  },
  {
    id: 'Rope_Line__c',
    head: {
      type: 'TEXT',
      info: { label: 'NAME' },
    },
    body: {
      type: 'TEXT',
      info: {},
    },
  },
  {
    id: 'Equipment_Detail__r.Name',
    head: {
      type: 'TEXT',
      info: { label: 'WINCH #' },
    },
    body: {
      type: 'LINK',
      info: { link: 'https://samsonportal.force.com/connect/s/detail/', linkId: 'Equipment_Detail__r.Id' },
    },
  },
  {
    id: 'Disposition__c',
    head: {
      type: 'TEXT',
      info: { label: 'DISPOSITION' },
    },
    body: {
      type: 'TEXT',
      info: {},
    },
  },
  {
    id: 'Status__c',
    head: {
      type: 'TEXT',
      info: { label: 'STATUS' },
    },
    body: {
      type: 'TEXT',
      info: {},
    },
  },
  {
    id: 'Line_Maintenance_Details__r.Inspection_Date__c',
    head: {
      type: 'TEXT',
      info: { label: 'LAST EVENT DATE' },
    },
    body: {
      type: 'DATE_TIME',
      info: { dateTimeFormat: 'll' },
    },
  },
  {
    id: 'Install_Date__c',
    head: {
      type: 'TEXT',
      info: { label: 'INSTALL DATE' },
    },
    body: {
      type: 'DATE_TIME',
      info: { dateTimeFormat: 'll' },
    },
  },
  {
    id: 'Accumulated_Working_Hours__c',
    head: {
      type: 'NUMBER',
      info: { label: 'ACC. HOURS' },
    },
    body: {
      type: 'NUMBER',
      info: { toFixed: 1 },
    },
  },
];

class RpsTable extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  // update the info box with the given rps
  updateInfoBox = (rps, winch) => {
    let infoBoxData = null;
    if (rps) {
      infoBoxData = { line: null, tail: null };
      // assign the selected mainline/tail
      if (rps.Rope_Line__c === 'Mainline') {
        infoBoxData.line = rps;
      } else {
        infoBoxData.tail = rps;
      }
      // get the mainline or tail from the winch
      if (winch) {
        infoBoxData.equipment = winch;
        let mainline = null;
        let tail = null;
        // if winch has rope prod specs
        if (winch.Rope_Product_Specs__r) {
          mainline = winch.Rope_Product_Specs__r.find(x => x.Rope_Line__c === 'Mainline');
          tail = winch.Rope_Product_Specs__r.find(x => x.Rope_Line__c === 'Mooring Tails');
        }
        // if the winch has a mainline and the passed rps is not a line
        if (mainline && !infoBoxData.line) {
          infoBoxData.line = this.props.rpsReducerAsset.rpsesAsset.find(x => x.Id === mainline.Id);
        }
        // if the winch has a tail and the passed rps is not a tail
        if (tail && !infoBoxData.tail) {
          infoBoxData.tail = this.props.rpsReducerAsset.rpsesAsset.find(x => x.Id === tail.Id);
        }
      }
    }
    this.props.infoBoxActions.UpdateInfoBoxData(infoBoxData);
  }

  rowClickHandler = (rps) => {
    // const rps =
    // this.props.rpsReducerAsset.rpsesAssetFiltered.find(x => x.Id === id);
    const winch =
    this.props.eqpReducer.equipmentDetailsAsset.find(x => x.Id === rps.Equipment_Detail__c);

    this.props.rpsActions.UpdateSelectedRps(rps); // update selected rps
    this.props.eqpActions.UpdateSelEqpId(winch ? winch.Id : ''); // update selected winch
    this.updateInfoBox(rps, winch); // update info box with selected rps
  }

  render() {
    return (
      <Table
        columns={columns}
        data={this.props.rpsReducerAsset.rpsesAsset ?
          this.props.rpsReducerAsset.rpsesAssetFiltered : []}
        selectedRow={this.props.rpsReducerSel.selectedRps || {}}
        width={1020}
        height={150}
        orderBy="Cert__r.Name"
        rowHandleClick={this.rowClickHandler}
      />
    );
  }
}

RpsTable.propTypes = {
  rpsReducerAsset: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
  rpsReducerSel: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
  eqpReducer: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
  rpsActions: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
  eqpActions: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
  infoBoxActions: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
};

const mapStateToProps = state => ({
  rpsReducerAsset: state.rpsReducerAsset,
  rpsReducerSel: state.rpsReducerSel,
  eqpReducer: state.eqpReducer,
});

function mapDispatchToProps(dispatch) {
  return {
    rpsActions: bindActionCreators(rpsActions, dispatch),
    eqpActions: bindActionCreators(eqpActions, dispatch),
    infoBoxActions: bindActionCreators(infoBoxActions, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(RpsTable);
