import React from 'react';
import { Link } from 'react-router-dom';
import { Glyphicon, Nav, Navbar, NavItem, NavDropdown, MenuItem } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';
import './Navbar.css';

export default () => (
  <Navbar inverse={true} fixedTop={true} fluid={true} collapseOnSelect={true}>
    <Navbar.Header>
      <Navbar.Brand>
        <Link to="/">ICARIA</Link>
      </Navbar.Brand>
      <Navbar.Toggle />
    </Navbar.Header>
    <Navbar.Collapse>
      <Nav>
        <LinkContainer to="/" exact={true}>
          <NavItem>
            <Glyphicon glyph="home" />
          </NavItem>
        </LinkContainer>
        <NavDropdown eventKey={3} title="LINECARE" id="linecare-nav-dropdown">
          <MenuItem eventKey={3.1}>LINECARE HOME</MenuItem>
          <MenuItem eventKey={3.2}>FLEETS</MenuItem>
          <MenuItem eventKey={3.2}>VESSELS</MenuItem>
          <MenuItem eventKey={3.2}>MAINTENANCE POLICIES</MenuItem>
          <MenuItem eventKey={3.2}>MAINTENANCE ACTIVITIES</MenuItem>
          <MenuItem eventKey={3.2}>LINETRACKER</MenuItem>
        </NavDropdown>
        <NavDropdown eventKey={3} title="COMPLIANCE" id="compliance-nav-dropdown">
          <MenuItem eventKey={3.1}>COMPLIANCE HOME</MenuItem>
          <MenuItem eventKey={3.1}>LINE MANAGEMENT PLAN</MenuItem>
          <MenuItem eventKey={3.2}>EQUIPMENT COMPATIBILITY</MenuItem>
        </NavDropdown>
        <NavDropdown eventKey={3} title="CLASSROOM" id="classroom-nav-dropdown">
          <MenuItem eventKey={3.1}>CLASSROOM HOME</MenuItem>
          <MenuItem eventKey={3.2}>TRAINING COURSES</MenuItem>
          <MenuItem eventKey={3.2}>SPLICE INSTRUCTIONS</MenuItem>
        </NavDropdown>
        <LinkContainer to="/" exact={true}>
          <NavItem>CONTACT US</NavItem>
        </LinkContainer>
        <NavDropdown eventKey={3} title="MY ACCOUNT" id="myaccount-nav-dropdown">
          <MenuItem eventKey={3.1}>CERTS</MenuItem>
        </NavDropdown>
      </Nav>
    </Navbar.Collapse>
  </Navbar>
);
