export function getEmailSignature(user, account) {
  let emailSignature = '%0A%0A%0A';
  if (user.Name) {
    emailSignature += `${user.Name}%0A`;
  }
  if (user.Email) {
    emailSignature += `${user.Email}%0A`;
  }
  if (user.Phone) {
    emailSignature += `${user.Phone}%0A`;
  }
  if (account.Name) {
    emailSignature += `${account.Name}`;
  }

  return emailSignature;
}

export function getMailToHref(receiver, cc, subject, signature) {
  if (receiver) {
    if (cc) {
      return `mailto:${receiver}?cc=${cc}&subject=${subject}&body=${signature}`;
    }
    return `mailto:${receiver}&subject=${subject}&body=${signature}`;
  }
  return `mailto:${cc}&subject=${subject}&body=${signature}`;
}
