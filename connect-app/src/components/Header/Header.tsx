import React from 'react';
import { connect } from 'react-redux';
import { getEmailSignature, getMailToHref } from './helper';
import styles from './Header.less';
import fleetsIcon from '../../images/Icaria_FLEETS_Icon_90_60px.png';
import assetIcon from '../../images/Icaria_VESSEL_Icon_60_60px.png';
import helpIcon from '../../images/Connect_HELP_Icon_90_60px.png';

interface IProps {
  accountR: {
    accountLoading: boolean;
    account: {
      Name: string;
      Field_Service_Lead_Email__c: string;
      CSR_Email__c: string;
      Owner_Email__c: string;
      Field_Service_Lead__r: {
        Name: string;
      };
      CSR__r: {
        Name: string;
      };
      Owner: {
        Name: string;
      };
    };
  };
  userR: {
    userLoading: boolean;
    user: {
      Name: string;
    };
  };
}

const Header: React.SFC<IProps> = (props) => {
  const { accountLoading, account } = props.accountR;
  const { userLoading, user } = props.userR;
  if (accountLoading || userLoading) {
    return <span>Loading...</span>;
  }
  // check to make sure account is not null
  if (!account || !user) {
    return null;
  }

  const emailSubject = `${user.Name} from ${account.Name} requests follow-up`;
  const emailSignature = getEmailSignature(user, account);
  const techMailToHref = getMailToHref(
    account.Field_Service_Lead_Email__c,
    'fieldservice@samsonrope.com',
    emailSubject,
    emailSignature,
  );
  const csrMailToHref = getMailToHref(
    account.CSR_Email__c,
    'custserv@samsonrope.com',
    emailSubject,
    emailSignature,
  );
  const salesMailToHref = getMailToHref(account.Owner_Email__c, '', emailSubject, emailSignature);

  return (
    <div className={styles.headerStyle}>
      <div className={styles.footerStyle}>
        <div className={styles.cardStyle}>
          <div className={styles.gridStyle}>
            {/* column 1 */}
            <div className={styles.gridItemStyle}>
              {/* Account Name */}
              <div style={{ fontWeight: 'bold', marginBottom: '2px' }}>
                {account.Name.toUpperCase()}
              </div>
              {/* Fleets & Vessels */}
              <table>
                <tbody>
                  <tr>
                    <td>
                      <img src={fleetsIcon} alt="Fleets" />
                    </td>
                    <td>
                      {' '}
                      <div className={styles.verLine} />{' '}
                    </td>
                    <td>
                      <button className={styles.buttonLinkStyle}>Fleets</button>
                    </td>
                    <td style={{ width: '20px' }} />
                    <td>
                      <img src={assetIcon} alt="Vessels" />
                    </td>
                    <td>
                      {' '}
                      <div className={styles.verLine} />{' '}
                    </td>
                    <td>
                      <button className={styles.buttonLinkStyle}>Vessels</button>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            {/* column 2 */}
            <div className={styles.gridItemStyle} style={{ lineHeight: '18px' }}>
              <div style={{ fontWeight: 'bold' }}>KEY SAMSON CONTACTS:</div>
              <div>
                Technical Support:&nbsp;
                <a className={styles.linkStyle} href={techMailToHref}>
                  {account.Field_Service_Lead__r
                    ? account.Field_Service_Lead__r.Name
                    : 'Tech Support Group'}
                </a>
              </div>
              <div>
                Customer Service:&nbsp;
                <a className={styles.linkStyle} href={csrMailToHref}>
                  {account.CSR__r ? account.CSR__r.Name : 'Customer Service Group'}
                </a>
              </div>
              <div>
                My Account Rep:&nbsp;
                {account.Owner_Email__c ? (
                  <a className={styles.linkStyle} href={salesMailToHref}>
                    {account.Owner.Name}
                  </a>
                ) : (
                  <span>Not Assigned</span>
                )}
              </div>
            </div>
            {/* column 3 */}
            <div className={styles.gridItemStyle} style={{ display: 'flex', alignItems: 'center' }}>
              <div className={styles.buttonStyle}>
                <img src={helpIcon} alt="Help!" />
              </div>
            </div>
          </div>
        </div>
      </div>
      <hr style={{ margin: '0px' }} />
    </div>
  );
};

const mapStateToProps = (state) => ({
  accountR: state.accountR,
  userR: state.userR,
});

export default connect(
  mapStateToProps,
  null,
)(Header);
