// import React from 'react';
// import { NavLink, Switch, Route } from 'react-router-dom';

// const Navigation = () => (
//   <nav>
//     <ul>
//       <li>
//         <NavLink to="/">Home</NavLink>
//       </li>
//       <li>
//         <NavLink to="/about">About</NavLink>
//       </li>
//       <li>
//         <NavLink to="/contact">Contact</NavLink>
//       </li>
//     </ul>
//   </nav>
// );

// const Home = () => <div>Home</div>;
// const About = () => <div>About</div>;
// const Contact = () => <div>Contact</div>;

// const Main = () => (
//   <Switch>
//     <Route exact="true" path="/" component={Home} />
//     <Route exact="true" path="/about" component={About} />
//     <Route excat="ture" path="/contact" component={Contact} />
//   </Switch>
// );

// const App = () => (
//   <div className="app">
//     <h1>React Router Demo</h1>
//     <Navigation />
//     <Main />
//   </div>
// );

// export default App;

import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import styled, { keyframes } from 'styled-components';
// components
import * as Controller from './controllers/controller';
// tslint:disable-next-line:ordered-imports
import AssetPage from './components/AssetPage/AssetPage';
// actions
import * as assetActions from './actions/assetActions';
import * as rpsActions from './actions/rpsActions';
import * as eqpActions from './actions/eqpActions';
import * as lmdActions from './actions/lmdActions';
import * as appActions from './actions/appActions';
import * as dataActions from './actions/dataActions';

// helper js function
import { getPath } from './helpers/helper';

const spin = keyframes`
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
`;

const Container = styled.div`
  /* Center align */
  margin: auto;
  width: 1px;
  text-align: center;
`;

const Loader = styled.div`
  border: 5px solid #f3f3f3;
  border-radius: 50%;
  border-top: 5px solid #000;
  width: 50px;
  height: 50px;
  animation: ${spin} 2s linear infinite;
  margin-left: 50px;
`;

const divStyle = {
  margin: '10px',
  width: '1070px',
};

function getUrlParameter() {
  const parts = window.parent.location.href.split('/');
  let i = 0;
  for (i = 0; i < parts.length; i++) {
    if (parts[i] === 'asset') {
      return parts[i + 1];
    }
  }

  const query = window.parent.location.search.substring(1);
  const vars = query.split('&');
  for (i = 0; i < vars.length; i++) {
    const pair = vars[i].split('=');
    if (pair[0] === 'assetId') {
      return pair[1];
    }
  }
  return '';
}

interface IProps {
  appReducer: {
    callBackCount: number;
  };
  dataActions: {
    UpdatePath: (title: string) => string;
  };
  title: string;
}

class App extends React.Component<IProps> {
  private assetId = getUrlParameter();

  public componentWillMount() {
    this.props.dataActions.UpdatePath(getPath(this.props.title)); // update path to the images
    Controller.loadAssets(this.props); // get/update assets also update selected asset
    Controller.loadRopeProductSpecs(this.props); // get/update rope prod specs
    Controller.loadEquipmentDetails(this.props); // get/update equipment details
    Controller.loadLineMaintenanceDetails(this.props); // get/update line maintenance details
    Controller.getCDLs(this.props); // get/update content document links
  }

  public render() {
    if (this.props.appReducer.callBackCount <= 4) {
      return (
        <Container>
          <h1> Loading... </h1>
          <Loader />
        </Container>
      );
    }

    return (
      <div style={divStyle}>
        {/* <AssetPage AssetId={this.assetId} /> */}
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  appReducer: state.appReducer,
  rpsReducerAll: state.rpsReducerAll,
});

function mapDispatchToProps(dispatch) {
  return {
    appActions: bindActionCreators(appActions, dispatch),
    assetActions: bindActionCreators(assetActions, dispatch),
    dataActions: bindActionCreators(dataActions, dispatch),
    eqpActions: bindActionCreators(eqpActions, dispatch),
    lmdActions: bindActionCreators(lmdActions, dispatch),
    rpsActions: bindActionCreators(rpsActions, dispatch),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(App);
