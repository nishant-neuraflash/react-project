@isTest
public class Upd_Asset_File_URLTest {
    /*
     * Test Rout_Insp_Link__c 'after insert' and 'after delete' trigger
     */
    static testMethod void Rout_Insp_Update() {
        
        Account acct = new Account(Name='TEST_ACCT');
		insert acct;
        
        Id recordTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get('Incident Resolution').getRecordTypeId();
        WorkType wt = new WorkType(Name = 'Test', EstimatedDuration = 12.0, DurationType = 'Hours');
        insert wt;
        Id workTypeId = [SELECT Id FROM WorkType WHERE Name = 'Test'].Id;
        
        WorkOrder workOrder = new WorkOrder();
        workOrder.AccountId = acct.Id;
        workOrder.RecordTypeId = recordTypeId;
        workOrder.WorkTypeId = workTypeId;
        insert workOrder;
        
        ContentVersion cdl = new ContentVersion(); 
        cdl.Title ='INSP REPORT'; 
        cdl.PathOnClient = '/' + cdl.Title + '.jpg'; 
        cdl.VersionData = Blob.valueOf('Samson'); 
        cdl.origin = 'H';
        insert cdl;
        
        ContentDocumentLink contentlink = new ContentDocumentLink();
        contentlink.LinkedEntityId = workOrder.id;
        contentlink.contentdocumentid = [select contentdocumentid from contentversion where id =: cdl.id].contentdocumentid;
        contentlink.ShareType = 'V';
        insert contentlink;
        
        // test after insert 
        String contVerUrl = [Select Field_Image_URL__c From ContentVersion Where Id=: cdl.Id].Field_Image_URL__c;
        String workOrderUrl = [Select Rout_Insp_Link__c From WorkOrder Where Id=: workOrder.Id].Rout_Insp_Link__c;
        System.assertEquals(contVerUrl, workOrderUrl);

        delete contentlink;
        workOrderUrl = [Select Rout_Insp_Link__c From WorkOrder Where Id=: workOrder.Id].Rout_Insp_Link__c; 
        System.assertEquals(null, workOrderUrl);
    }
    
    /*
     * Test CDL Visibility for Work Order Before Insert trigger
     */
    static testMethod void WorkOrder_CDL_Visibility_Before_Insert() {
        
        Account acct = new Account(Name='TEST_ACCT');
		insert acct;
        
        Id recordTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get('Incident Resolution').getRecordTypeId();
        WorkType wt = new WorkType(Name = 'Test', EstimatedDuration = 12.0, DurationType = 'Hours');
        insert wt;
        Id workTypeId = [SELECT Id FROM WorkType WHERE Name = 'Test'].Id;
        
        WorkOrder workOrder = new WorkOrder();
        workOrder.AccountId = acct.Id;
        workOrder.RecordTypeId = recordTypeId;
        workOrder.WorkTypeId = workTypeId;
        insert workOrder;
        
        List<String> titles = new List<String> {'INSP REPORT', 'INSP REPORT', 'INSP REPORT', '1', '2', '3'};
        List<Boolean> results = new List<Boolean> {True, True, True, False, False, False};
                
        for (Integer i = 0; i < titles.Size(); i++) {
            ContentVersion cv = new ContentVersion(); 
            cv.Title = titles[i]; 
            cv.PathOnClient = '/' + cv.Title + '.jpg'; 
            cv.VersionData = Blob.valueOf('Samson'); 
            cv.origin = 'H';
            insert cv;
            
            ContentDocumentLink cdl = new ContentDocumentLink();
            cdl.LinkedEntityId = workOrder.id;
            cdl.contentdocumentid = [select contentdocumentid from contentversion where id =: cv.id].contentdocumentid;
            cdl.ShareType = 'V';
            insert cdl;
            
            // test after insert 
            String cdlVisibility = [Select Visibility From ContentDocumentLink Where Id=: cdl.Id].Visibility;
            System.assertEquals(cdlVisibility == 'AllUsers', results[i]);
        }
    }
    
    /*
     * Test Equip_Doc_Link__c 'after insert' and 'after delete' trigger
     */
    static testMethod void Equip_Doc_Link_Update() {
        
        Account acct = new Account(Name='TEST_ACCT');
		insert acct;
        
        Asset asset = new Asset(Name='TEST_ASSET');
        asset.AccountId = acct.Id;
        insert asset;
        
        ContentVersion cdl = new ContentVersion(); 
        cdl.Title ='Equipment Report 123'; 
        cdl.PathOnClient = '/' + cdl.Title + '.jpg'; 
        cdl.VersionData = Blob.valueOf('Samson'); 
        cdl.origin = 'H';
        insert cdl;
        
        ContentDocumentLink contentlink = new ContentDocumentLink();
        contentlink.LinkedEntityId = asset.id;
        contentlink.contentdocumentid = [select contentdocumentid from contentversion where id =: cdl.id].contentdocumentid;
        contentlink.ShareType = 'V';
        insert contentlink;
        
        // test after insert 
        String contVerUrl = [Select Field_Image_URL__c From ContentVersion Where Id=: cdl.Id].Field_Image_URL__c;
        String assetUrl = [Select Equip_Doc_Link__c From Asset Where Id=: asset.Id].Equip_Doc_Link__c; 
        System.assertEquals(contVerUrl, assetUrl);

        delete contentlink;
        assetUrl = [Select Equip_Doc_Link__c From Asset Where Id=: asset.Id].Equip_Doc_Link__c; 
        System.assertEquals(null, assetUrl);
    }
    
    /*
     * Test LMP_Doc_Link__c 'after insert'  and 'after delete' trigger
     */
    static testMethod void LMP_Doc_Link_Update() {
        
        Account acct = new Account(Name='TEST_ACCT');
		insert acct;
        
        Asset asset = new Asset(Name='TEST_ASSET');
        asset.AccountId = acct.Id;
        insert asset;
        
        ContentVersion cdl = new ContentVersion(); 
        cdl.Title ='LINE MANAGEMENT PLAN 123'; 
        cdl.PathOnClient = '/' + cdl.Title + '.jpg'; 
        cdl.VersionData = Blob.valueOf('Samson'); 
        cdl.origin = 'H';
        insert cdl;
        
        ContentDocumentLink contentlink = new ContentDocumentLink();
        contentlink.LinkedEntityId = asset.id;
        contentlink.contentdocumentid = [select contentdocumentid from contentversion where id =: cdl.id].contentdocumentid;
        contentlink.ShareType = 'V';
        insert contentlink;
        
        // test after insert 
        String contVerUrl = [Select Field_Image_URL__c From ContentVersion Where Id=: cdl.Id].Field_Image_URL__c;
        String assetUrl = [Select LMP_Doc_Link__c From Asset Where Id=: asset.Id].LMP_Doc_Link__c; 
        System.assertEquals(contVerUrl, assetUrl);

        delete contentlink;
        assetUrl = [Select LMP_Doc_Link__c From Asset Where Id=: asset.Id].LMP_Doc_Link__c; 
        System.assertEquals(null, assetUrl);
    }
    
    /*
     * Test CDL Visibility Before Insert trigger
     */
    static testMethod void Asset_CDL_Visibility_Before_Insert() {
        
        Account acct = new Account(Name='TEST_ACCT');
		insert acct;
        
        Asset asset = new Asset(Name='TEST_ASSET');
        asset.AccountId = acct.Id;
        insert asset;
        
        List<String> titles = new List<String> {'Equipment Report - 6_19_2018.docx '};//, 'LINE MANAGEMENT PLAN', 'ROUTINE INSPECTION SUMMARY', '1', '2', '3'};
            List<Boolean> results = new List<Boolean> {True};//, True, True, False, False, False};
                
        for (Integer i = 0; i < titles.Size(); i++) {
            ContentVersion cv = new ContentVersion(); 
            cv.Title = titles[i]; 
            cv.PathOnClient = '/' + cv.Title + '.jpg'; 
            cv.VersionData = Blob.valueOf('Samson'); 
            cv.origin = 'H';
            insert cv;
            
            ContentDocumentLink cdl = new ContentDocumentLink();
            cdl.LinkedEntityId = asset.id;
            cdl.contentdocumentid = [select contentdocumentid from contentversion where id =: cv.id].contentdocumentid;
            cdl.ShareType = 'V';
            insert cdl;
            
            // test after insert 
            String cdlVisibility = [Select Visibility From ContentDocumentLink Where Id=: cdl.Id].Visibility;
            System.assertEquals(cdlVisibility == 'AllUsers', results[i]);
        }
    }
}