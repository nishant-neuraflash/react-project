public class SamsonTestHelper
{
    static Map<String, Id> lmdRecordTypeIds;
    /**
     * Creates a map of LMD record type ids 
     * 
     */
    public static void createLmdRecordTypeIds()
    {
        lmdRecordTypeIds = new Map<String, Id>();
        lmdRecordTypeIds.put('Status Update', Schema.SObjectType.Line_Maintenance_Detail__c.getRecordTypeInfosByName().get('Status Update').getRecordTypeId());
        lmdRecordTypeIds.put('Rotation', Schema.SObjectType.Line_Maintenance_Detail__c.getRecordTypeInfosByName().get('Rotation').getRecordTypeId());
        lmdRecordTypeIds.put('EFE', Schema.SObjectType.Line_Maintenance_Detail__c.getRecordTypeInfosByName().get('EFE').getRecordTypeId());
        lmdRecordTypeIds.put('Inspection', Schema.SObjectType.Line_Maintenance_Detail__c.getRecordTypeInfosByName().get('Inspection').getRecordTypeId());
        lmdRecordTypeIds.put('Cropping', Schema.SObjectType.Line_Maintenance_Detail__c.getRecordTypeInfosByName().get('Cropping').getRecordTypeId());
        lmdRecordTypeIds.put('Asset Activity', Schema.SObjectType.Line_Maintenance_Detail__c.getRecordTypeInfosByName().get('Asset Activity').getRecordTypeId());
        lmdRecordTypeIds.put('Testing', Schema.SObjectType.Line_Maintenance_Detail__c.getRecordTypeInfosByName().get('Testing').getRecordTypeId());
        lmdRecordTypeIds.put('Equipment Survey', Schema.SObjectType.Line_Maintenance_Detail__c.getRecordTypeInfosByName().get('Equipment Survey').getRecordTypeId());
    }
    
    /**
     * Creates a list of accounts and inserts them
     * in the database and then it returns the list 
     * of accounts
     * 
     * @param count	number of accounts to create
     * @return		the list of created accounts
     */
    public static List<Account> createAccounts(Integer count)
    {
        List<Account> accounts = new List<Account>();
        for (Integer i = 1; i <= count; i++)
        {
            Account account = new Account();
            account.Name = 'Test Account: ' + i;
            accounts.add(account);
        }
        insert accounts;
        return accounts;
    }
    
    /**
     * Creates a list of fleets and inserts them in the database and 
     * then it returns the list of fleets
     * 
     * @param accounts	creates one fleet for every account in the list
     * @return			the list of created fleets
     */
    public static List<Fleet__c> createFleets(List<Account> accounts)
    {
        List<Fleet__c> fleets = new List<Fleet__c>();
        Integer count = accounts.Size();
        for (Integer i = 1; i <= count; i++)
        {
            Fleet__c fleet = new Fleet__c();
            fleet.Name = 'Test Fleet: ' + i;
            fleet.Operator__c = accounts[i-1].Id;
            fleet.Retirement_Criteria_in_Years__c = 2;
            fleets.add(fleet);
        }
        insert fleets;
        return fleets;
    }
    
    /**
     * Creates a list of assets and inserts them in the database and 
     * then it returns the list of assets
     * 
     * @param accounts	creates one asset for every account in the list
     * @return			the list of created assets
     */
    public static List<Asset> createAssets(List<Account> accounts)
    {
        List<Asset> assets = new List<Asset>();
        Integer count = accounts.Size();
        for (Integer i = 1; i <= count; i++)
        {
            Asset asset = new Asset();
            asset.Name = 'Test Asset: ' + i;
            asset.AccountId = accounts[i-1].Id;
            asset.Max_Num_Equip_Vertical__c = '3';
            assets.add(asset);
        }
        insert assets;
        return assets;
    }
    
    /**
     * Creates a list of rope prod specs and inserts them in the database and 
     * then it returns the list of rope prod specs
     * 
     * @param assets	creates one RPS for every asset in the list
     * @return			the list of created rope prod specs
     */
    public static List<Rope_Product_Spec__c> createRpses(List<Asset> assets)
    {
        List<Rope_Product_Spec__c> rpses = new List<Rope_Product_Spec__c>();
        Integer count = assets.Size();
        for (Integer i = 1; i <= count; i++)
        {
            Rope_Product_Spec__c rps = new Rope_Product_Spec__c();
            rps.Asset__c = assets[i-1].Id;
            rpses.add(rps);
        }
        insert rpses;
        return rpses;
    }
    
    public static List<Line_Maintenance_Detail__c> createLmds(Account account, Asset asset, Rope_Product_Spec__c rps, List<String> lmdRecordTypes)
    {
        List<Line_Maintenance_Detail__c> lmds = new List<Line_Maintenance_Detail__c>();
     	for (String lmdRecordType : lmdRecordTypes)
        {
            Line_Maintenance_Detail__c lmd = new Line_Maintenance_Detail__c();
            lmd.RecordTypeId = lmdRecordTypeIds.get(lmdRecordType);
            lmd.Rope_Product_Spec__c = rps.id;
            lmd.Account__c = account.Id;
            lmd.Asset__c = asset.Id;
            lmd.Inspection_Date__c = Date.newInstance(2018, 04, 04);
            lmd.Outboard_End_in_use_1__c = 'B';
            lmd.Status__c = 'Installed';
            
			lmds.add(lmd);
        }
        insert lmds;
        return lmds;
    }
    
    public static  Map<Integer, List<String>> createLmdRecordTypes()
    {
        Map<Integer, List<String>> lmdRecordTypes = new Map<Integer, List<String>>();
        lmdRecordTypes.put(0, new List<String>
                           {
                               'Status Update', 'Rotation', 'EFE', 'Inspection',
                               'Cropping', 'Asset Activity', 'Testing'
                           });
        lmdRecordTypes.put(1, new List<String>
                           {
                               'Status Update', 'Rotation', 'EFE', 'Inspection',
                               'Cropping', 'Asset Activity', 'Testing'
                           });
        lmdRecordTypes.put(2, new List<String>
                           {
                               'Status Update', 'Rotation', 'EFE', 'Inspection',
                               'Cropping', 'Asset Activity', 'Testing'
                           });
        lmdRecordTypes.put(3, new List<String>
                           {
                               'Status Update', 'Rotation', 'EFE', 'Inspection',
                               'Cropping', 'Asset Activity', 'Testing'
                           });
        lmdRecordTypes.put(4, new List<String>
                           {
                               'Status Update', 'Rotation', 'EFE', 'Inspection',
                               'Cropping', 'Asset Activity', 'Testing'
                           });
        lmdRecordTypes.put(5, new List<String>
                           {
                               'Status Update', 'Rotation', 'EFE', 'Inspection',
                               'Cropping', 'Asset Activity', 'Testing'
                           });
        lmdRecordTypes.put(6, new List<String>
                           {
                               'Status Update', 'Rotation', 'EFE', 'Inspection',
                               'Cropping', 'Asset Activity', 'Testing'
                           });
        lmdRecordTypes.put(7, new List<String>
                           {
                               'Status Update', 'Rotation', 'EFE', 'Inspection',
                               'Cropping', 'Asset Activity', 'Testing'
                           });
        lmdRecordTypes.put(8, new List<String>
                           {
                               'Status Update', 'Rotation', 'EFE', 'Inspection',
                               'Cropping', 'Asset Activity', 'Testing'
                           });
        lmdRecordTypes.put(9, new List<String>
                           {
                               'Status Update', 'Rotation', 'EFE', 'Inspection',
                               'Cropping', 'Asset Activity', 'Testing'
                           });        
        return lmdRecordTypes;
    }
    
}