public class Connect_FleetPageController {
    private static Id contactId;
    private static Id accountId;
    private static List<Id> assetIds;
    
    static {
        if (UserInfo.getUserType() == 'Standard') {
            contactId = [SELECT Id FROM Contact WHERE Email = :UserInfo.getUserEmail()].Id;
        }
        else {
            contactId = [SELECT ContactId FROM User WHERE Id = :UserInfo.getUserId()].ContactId;
        }
        accountId = [SELECT AccountId FROM Contact WHERE Id = :contactId].AccountId;
        assetIds = new List<Id>();
        for (Related_Contacts__c rc : [SELECT Asset__c FROM Related_Contacts__c WHERE Asset__c != '' AND Contact__c = :contactId]) {
            assetIds.add(rc.Asset__c);
        }
        
        // id of fleets that have a related contact for this contact and that the 'Share_All_Asset_Data__c' is checked
        List<Id> fleetIds = new List<Id>();
        for (Related_Contacts__c rc : [SELECT Fleet__c FROM Related_Contacts__c WHERE Fleet__c != '' AND Contact__c = :contactId AND Share_All_Asset_Data__c = True]){
            fleetIds.add(rc.Fleet__c);
        }
		
        // get all that assets that thier assigned fleet is in the fleet list above
        for (Asset asset : [SELECT Id FROM Asset WHERE AccountId = :accountId AND Fleet__c IN :fleetIds]) {
            assetIds.add(asset.Id);
        }

    }
 
    // load fleets for the given account
    @RemoteAction
    public static List<Fleet__c> loadFleets() {
        return [SELECT 
                Id, 
                Name,
                Fleet_Type__c,
                (SELECT Id, Name FROM Related_Asset__r),
                (SELECT Id, Name FROM Service_Contracts__r),
                Owner__r.Name,
                Operator__r.Name
                FROM Fleet__c
                WHERE Owner__c = :accountId 
            	ORDER BY Name];
    }
    
    // load assets for the given account
    @RemoteAction
    public static List<Asset> loadAllAssets() {
        return [SELECT 
                Id, 
                Name, 
                AccountId, 
                Fleet__c,
                Status__c,
                Installation_Type__c,
                Commissioned_Date__c
                FROM Asset
                WHERE AccountId = :accountId
            	ORDER BY Name];
    }
    
    // load assets for the given account and contact
    @RemoteAction
    public static List<Asset> loadAssets() {
        return [SELECT 
                Id
                FROM Asset
                WHERE AccountId = :accountId AND Id IN :assetIds
            	ORDER BY Name];
    }
        
    // load all rope prod specs for the given account
    @RemoteAction
    public static List<Rope_Product_Spec__c> loadRopeProductSpecs() {
        return [SELECT 
              	Id, Asset__c, Disposition__c
                FROM Rope_Product_Spec__c
            	WHERE Account__c = :accountId AND Asset__c IN :assetIds];
    }
    
    // load equipment detaisl for the given account
    @RemoteAction
    public static List<Equipment_Detail__c> loadEquipmentDetails() {
        return [SELECT 
                Id, Asset__c, Disposition__c
                FROM Equipment_Detail__c
                WHERE Asset__c IN (SELECT Id FROM Asset WHERE AccountId = :accountId) AND Asset__c IN :assetIds AND
                X_Coordinate__c != '' AND
                Y_Coordinate__c != '' AND
                RecordTypeId != ''];
    }
    
    // get all WorkOrders for the account
    @RemoteAction
    public static List<WorkOrder> loadWorkOrders() {
        return [SELECT 
                Id, AssetId, WorkType.Name, Inspection_Date__c
                FROM WorkOrder
                WHERE AccountId = :accountId AND 
                (Status = 'New' OR Status = 'In Progress') AND 
                Inspection_Date__c <= Next_N_Days:45 AND
                WorkTypeId != null AND
                RecordTypeId != null AND 
                RecordType.Name != 'Service Onboarding' AND 
                RecordType.Name != 'Internal Samson Support'
				ORDER BY AssetId, Inspection_Date__c];
    }
}