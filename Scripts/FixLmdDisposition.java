/*
List<Line_Maintenance_Detail__c> lmds = [Select Id, Pitting_Rust__c, Scoring__c, Equipment_Disposition__c From Line_Maintenance_Detail__c Where RecordType.Name = 'Equipment Survey'];

for (Line_Maintenance_Detail__c lmd : lmds)
{
    lmd.Activity_Processed__c = False;
    if (lmd.Pitting_Rust__c && lmd.Scoring__c)
    {
        lmd.Equipment_Disposition__c = 'Under Review';
    }
    else
    {
        lmd.Equipment_Disposition__c = 'Continue Use';
    }
}

update lmds;
*/

LMDtoRPSAutomatonHelper.updateRPSwithLMD();