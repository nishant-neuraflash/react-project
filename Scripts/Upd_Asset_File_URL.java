trigger Upd_Asset_File_URL on ContentDocumentLink (before insert, after insert, before delete) {    
    List<ContentDocumentLink> cdls;	// list of content document links
    /* get the old links if this is a delete trigger */
    /*
    if (Trigger.isDelete) {
        cdls = trigger.old;
    }
	*/
    /* get the new links if this is an update trigger */
    else if (Trigger.isInsert) {
        cdls = trigger.new;
        /* update the visibility of all the links before inserting */
        if (Trigger.isBefore) {
            ContentDocumentLinkTriggerHelper.updateCDVVisibility(cdls);
            return;
        }
    }
    
    /* iterate through all the links for 'before delete' and 'after insert' triggers */
    /*
    for (ContentDocumentLink cdl : cdls) {
        ContentDocumentLinkTriggerHelper.updateAssetDocLink(cdl, Trigger.isDelete);
        ContentDocumentLinkTriggerHelper.updateWorkOrderDocLink(cdl, Trigger.isDelete);
    }
	*/
}