public with sharing class ContentDocumentLinkTriggerHelper {

    // update content document version visibility when uploaded to a work order 
    // and the file extension of the file is pdf
    public static void updateCDVVisibility(List<ContentDocumentLink> cdls){
        for (ContentDocumentLink cdl : cdls) {
            if (cdl.ContentDocument.FileExtension.toUpperCase() == 'PDF') {
                List<WorkOrder> workOrders = [Select Id From WorkOrder Where Id = :cdl.LinkedEntityId];
                if (workOrders != null && !workOrders.isEmpty()) {
                    cdl.Visibility = 'AllUsers';
                }
            }
        }
    }
    
    /*
	// update content document version visibility    
    public static void updateCDVVisibility(List<ContentDocumentLink> cdls){
        for (ContentDocumentLink cdl : cdls) {
            List<ContentVersion> conts = [Select Id, Title, Field_Image_URL__c From ContentVersion Where ContentDocumentId=: cdl.ContentDocumentId];
            ContentVersion cont = null;
            // we are only intereted in the first content version
            if (conts != null && !conts.isEmpty()) {
                cont = conts[0];
                if (cont.Title.mid(0, 16).toUpperCase() == 'EQUIPMENT REPORT' ||
                    cont.Title.mid(0, 20).toUpperCase() == 'LINE MANAGEMENT PLAN' ||
                    cont.Title.mid(0, 26).toUpperCase() == 'ROUTINE INSPECTION SUMMARY' ||
                   	cont.Title.mid(0, 11).toUpperCase() == 'INSP REPORT' ) {
                        
                    cdl.Visibility = 'AllUsers';
                }
            }
        }
    }
	*/
    
    public static void updateWorkOrderDocLink(ContentDocumentLink cdl, boolean isDelete){
        // get all the work orders which have id = cdl.LinkedEntityId 
        List<WorkOrder> workOrders = [Select Id, Rout_Insp_Link__c From WorkOrder Where Id=:cdl.LinkedEntityId];
        // get all the content versions which have ContentDocumentId = cdl.ContentDocumentId
        List<ContentVersion> conts = [Select Id, Title, Field_Image_URL__c From ContentVersion Where ContentDocumentId=: cdl.ContentDocumentId];
        
        WorkOrder workOrder = null;
        ContentVersion cont = null;
        
        // we are only interested in the first work order
        if (workOrders != null && !workOrders.isEmpty()) {
            workOrder = workOrders[0];
        }
        // we are only intereted in the first content version
        if (conts != null && !conts.isEmpty()) {
            cont = conts[0];
        }
        
        if (isDelete) {
            deleteRoutInspLink(workOrder, cont);
        } else {
            changeRoutInspLink(workOrder, cont);
        }        
    }
    
    private static void deleteRoutInspLink(WorkOrder workOrder, ContentVersion cont){
        // make sure we have a work order and a content version
        if (WorkOrder != null && cont != null) {
            // logic when left of title is 'INSP REPORT'
            if (cont.Title.mid(0, 11).toUpperCase() == 'INSP REPORT' && workOrder.Rout_Insp_Link__c == cont.Field_Image_URL__c) {
                workOrder.Rout_Insp_Link__c = '';
                update workOrder;
            }
        }
    }
    
    private static void changeRoutInspLink(WorkOrder workOrder, ContentVersion cont){
        // make sure we have a work order and a content version
        if (workOrder != null && cont != null) {
            // logic when left of title is 'EQUIPMENT REPORT'
            if (cont.Title.mid(0, 11).toUpperCase() == 'INSP REPORT') {
                workOrder.Rout_Insp_Link__c = cont.Field_Image_URL__c;
                update workOrder;
            }            
        }
    }
    
    public static void updateAssetDocLink(ContentDocumentLink cdl, boolean isDelete){
        // get all the assets which have id = cdl.LinkedEntityId 
        List<Asset> assets = [Select Id, Name, Equip_Doc_Link__c, LMP_Doc_Link__c From Asset Where Id=:cdl.LinkedEntityId];
        // get all the content versions which have ContentDocumentId = cdl.ContentDocumentId
        List<ContentVersion> conts = [Select Id, Title, Field_Image_URL__c From ContentVersion Where ContentDocumentId=: cdl.ContentDocumentId];
        
        Asset asset = null;
        ContentVersion cont = null;
        
        // we are only interested in the first asset
        if (assets != null && !assets.isEmpty()) {
            asset = assets[0];
        }
        // we are only intereted in the first content version
        if (conts != null && !conts.isEmpty()) {
            cont = conts[0];
        }
        
        if (isDelete) {
            deleteAssetDocLink(asset, cont);
        } else {
            changeAssetDocLink(asset, cont);
        }        
    }
    
    private static void deleteAssetDocLink(Asset asset, ContentVersion cont){
        // make sure we have an asset and a content version
        if (asset != null && cont != null) {
            // logic when left of title is 'EQUIPMENT REPORT'
            if (cont.Title.mid(0, 16).toUpperCase() == 'EQUIPMENT REPORT' && asset.Equip_Doc_Link__c == cont.Field_Image_URL__c) {
                asset.Equip_Doc_Link__c = '';
            }
            // logic when left of title is 'LINE MANAGEMENT PLAN'
            else if (cont.Title.mid(0, 20).toUpperCase() == 'LINE MANAGEMENT PLAN' && asset.LMP_Doc_Link__c == cont.Field_Image_URL__c) {
                asset.LMP_Doc_Link__c = '';
            }
            // we have changed the asset Equip_Doc_Link__c or LMP_Doc_Link__c now let's update it
            update asset;
        }
    }
    
    private static void changeAssetDocLink(Asset asset, ContentVersion cont){
        // make sure we have an asset and a content version
        if (asset != null && cont != null) {
            // logic when left of title is 'EQUIPMENT REPORT'
            if (cont.Title.mid(0, 16).toUpperCase() == 'EQUIPMENT REPORT') {
                asset.Equip_Doc_Link__c = cont.Field_Image_URL__c;
            }
            // logic when left of title is 'LINE MANAGEMENT PLAN'
            else if (cont.Title.mid(0, 20).toUpperCase() == 'LINE MANAGEMENT PLAN') {
                asset.LMP_Doc_Link__c = cont.Field_Image_URL__c;
            }
            // we have changed the asset Equip_Doc_Link__c or LMP_Doc_Link__c now let's update it
            update asset;
        }
    }
}