/**
 * This class is used to test 'LMDtoRPSAutomationTest' class
 * 
 * @author		Samsonrope Technologies
 * @version		1.0
 * @since		01/04/2019
 */
@isTest
private class LMDtoRPSAutomationTest
{
    private static Map<String, Id> lmdRecordTypeIds;
    
    private static final String DIS_CONT_USE = 'Continue Use';
    private static final String DIS_REPAIR = 'Repair';
    private static final String DIS_UNDR_REV = 'Under Review';
    
    private static final String RT_STAT_UPD = 'Status Update';
    private static final String RT_REPAIR = 'Repair';
    private static final String RT_ROTATION = 'Rotation';
    private static final String RT_EFE = 'EFE';
    private static final String RT_INSPECTION = 'Inspection';
    private static final String RT_CROPPING = 'Cropping';
    private static final String RT_ASSET_ACT = 'Asset Activity';
    private static final String RT_TESTING = 'Testing';
    private static final string RT_EQP_SURVEY = 'Equipment Survey';
    private static final String RT_REQ_NEW_LINE_TRACK = 'Request New Line to Track';
    
    /**
     * This method is used to setup some variables that will be used during the testing
     */
    static void setup() {
        lmdRecordTypeIds = new Map<String, Id>();
        lmdRecordTypeIds.put(RT_STAT_UPD, Schema.SObjectType.Line_Maintenance_Detail__c.getRecordTypeInfosByName().get(RT_STAT_UPD).getRecordTypeId());
        lmdRecordTypeIds.put(RT_REPAIR, Schema.SObjectType.Line_Maintenance_Detail__c.getRecordTypeInfosByName().get(RT_REPAIR).getRecordTypeId());
        lmdRecordTypeIds.put(RT_ROTATION, Schema.SObjectType.Line_Maintenance_Detail__c.getRecordTypeInfosByName().get(RT_ROTATION).getRecordTypeId());
        lmdRecordTypeIds.put(RT_EFE, Schema.SObjectType.Line_Maintenance_Detail__c.getRecordTypeInfosByName().get(RT_EFE).getRecordTypeId());
        lmdRecordTypeIds.put(RT_INSPECTION, Schema.SObjectType.Line_Maintenance_Detail__c.getRecordTypeInfosByName().get(RT_INSPECTION).getRecordTypeId());
        lmdRecordTypeIds.put(RT_CROPPING, Schema.SObjectType.Line_Maintenance_Detail__c.getRecordTypeInfosByName().get(RT_CROPPING).getRecordTypeId());
        lmdRecordTypeIds.put(RT_ASSET_ACT, Schema.SObjectType.Line_Maintenance_Detail__c.getRecordTypeInfosByName().get(RT_ASSET_ACT).getRecordTypeId());
        lmdRecordTypeIds.put(RT_TESTING, Schema.SObjectType.Line_Maintenance_Detail__c.getRecordTypeInfosByName().get(RT_TESTING).getRecordTypeId());
        lmdRecordTypeIds.put(RT_EQP_SURVEY, Schema.SObjectType.Line_Maintenance_Detail__c.getRecordTypeInfosByName().get(RT_EQP_SURVEY).getRecordTypeId());
        lmdRecordTypeIds.put(RT_REQ_NEW_LINE_TRACK, Schema.SObjectType.Line_Maintenance_Detail__c.getRecordTypeInfosByName().get(RT_REQ_NEW_LINE_TRACK).getRecordTypeId());
    }
    
    /**
     * This method is used to start testing
     */
    @isTest static void runTests()
    {
        setup();
        
		// create/insert an account
		Account account = new Account();
        account.Name = 'Test Account';
        insert account;
        
		// create/insert an asset
        Asset asset = new Asset();
        asset.Name = 'Test Asset';
        asset.AccountId = account.Id;
        asset.Max_Num_Equip_Vertical__c = '3';
        insert asset;
                
        // create/insert lmd-000
        Line_Maintenance_Detail__c lmd000 = new Line_Maintenance_Detail__c();
        lmd000.RecordTypeId = lmdRecordTypeIds.get(RT_INSPECTION);
        lmd000.Account__c = account.Id;
        lmd000.Asset__c = asset.Id;
        lmd000.Inspection_Date__c = Date.newInstance(2001, 1, 1);
        insert lmd000;
        
        // create/insert equipment details
        Map<String, Equipment_Detail__c> eqps = createEqps(asset, lmd000);
        
        // create/insert rope prod specs
		Map<String, Rope_Product_Spec__c> rpses = createRpses(asset, lmd000, eqps.get('w1'));
        
        // create/insert line maintenance details
        Map<String, Line_Maintenance_Detail__c> lmds = createLmds(account, asset, eqps, rpses);
        
        LMDtoRPSAutomatonHelper.updateRPSwithLMD();
        
        // validate batch one
        validateRpsBatchOne(rpses, eqps, lmds, lmd000);
        validateLmdsBatchOne(lmds);
        validateEqpBatchOne(eqps, lmds, lmd000);
    }
    
    /**
     * This method will do assertion testing on the 3 rope prod specs to make sure 
     * that the current values of the rpses are what we expect them to be
     * 
     * @param	rpses	The 3 rpses that we will validate.
     * @param	eqps	The eqps that will be used in the validation.
     * @param	lmds	The lmds that will be used in the validation.
     * @param	lmd000	The lmd that will be used in the validation.
     */
    private static void validateRpsBatchOne(
        Map<String, Rope_Product_Spec__c> rpses, Map<String, Equipment_Detail__c> eqps,
        Map<String, Line_Maintenance_Detail__c> lmds, Line_Maintenance_Detail__c lmd000)
    {
        
        // get w1
        Equipment_Detail__c w1 = eqps.get('w1');
        // get 'LMD-001', 'LMD-003', LMD-004', 'LMD-010' and 'LMD-016'
        Line_Maintenance_Detail__c lmd001 = lmds.get('LMD-001');
        Line_Maintenance_Detail__c lmd003 = lmds.get('LMD-003');
        Line_Maintenance_Detail__c lmd004 = lmds.get('LMD-004');
        Line_Maintenance_Detail__c lmd010 = lmds.get('LMD-010');
        Line_Maintenance_Detail__c lmd016 = lmds.get('LMD-016');
        
        // query 'RPS-001', 'RPS-002' and 'RPS-003' from the database
        List<Id> rpsIds = new List<Id>();
        for (Rope_Product_Spec__c rps : rpses.values())
        {
            rpsIds.add(rps.Id);
        }
        Map<Id, Rope_Product_Spec__c> queriedRpses = new Map<Id, Rope_Product_Spec__c>
        (
            [
             SELECT
             Last_Maintenance_Event__c, Equipment_Detail__c, Install_Date__c, Current_End_in_Use__c,
             Disposition__c, Last_Rotation_Date__c, Last_Inspection__c, Last_Reported_Working_Hours__c,
             Last_Reported_Operations__c, Accumulated_Operations__c, Accumulated_Working_Hours__c,
             Status__c, Current_Length_meters__c 
             FROM Rope_Product_Spec__c
             WHERE Id IN :rpsIds
            ]
        );
            
        Rope_Product_Spec__c rps001 = queriedRpses.get(rpses.get('RPS-001').Id);
        Rope_Product_Spec__c rps002 = queriedRpses.get(rpses.get('RPS-002').Id);
        Rope_Product_Spec__c rps003 = queriedRpses.get(rpses.get('RPS-003').Id);
        Rope_Product_Spec__c rps004 = queriedRpses.get(rpses.get('RPS-004').Id);
        
        // validate rps 001 here
        System.assert(rps001.Last_Maintenance_Event__c == lmd003.Id);
        System.assert(rps001.Equipment_Detail__c == w1.Id);
        System.assert(rps001.Install_Date__c == Date.newInstance(2017, 4, 4));
        System.assert(rps001.Current_End_in_Use__c == 'A');
        System.assert(rps001.Disposition__c == DIS_UNDR_REV);
        System.assert(rps001.Last_Rotation_Date__c == null);
        System.assert(rps001.Last_Inspection__c == lmd000.Id);
        System.assert(rps001.Last_Reported_Working_Hours__c == 200);
        System.assert(rps001.Last_Reported_Operations__c == 20);
        System.assert(rps001.Accumulated_Operations__c == 11);
        System.assert(rps001.Accumulated_Working_Hours__c == (double)104.5);
        System.assert(rps001.Status__c == null);
        System.assert(rps001.Current_Length_meters__c == 85);
        
        // validate rps 002 here
        System.assert(rps002.Last_Maintenance_Event__c == lmd001.Id);
        System.assert(rps002.Equipment_Detail__c == w1.Id);
        System.assert(rps002.Install_Date__c == Date.newInstance(2018, 7, 1));
        System.assert(rps002.Current_End_in_Use__c == 'A');
        System.assert(rps002.Disposition__c == DIS_UNDR_REV);
        System.assert(rps002.Last_Rotation_Date__c == Date.newInstance(2018, 3, 1));
        System.assert(rps002.Last_Inspection__c == null);
        System.assert(rps002.Last_Reported_Working_Hours__c == 600);
        System.assert(rps002.Last_Reported_Operations__c == 40);
        System.assert(rps002.Accumulated_Operations__c == 2);
        System.assert(rps002.Accumulated_Working_Hours__c == (double)14.5);
        System.assert(rps002.Status__c == 'Installed');
        System.assert(rps002.Current_Length_meters__c == 150);
        
        // validate rps 003 here
        System.assert(rps003.Last_Maintenance_Event__c == lmd004.Id);
        System.assert(rps003.Equipment_Detail__c == null);
        System.assert(rps003.Install_Date__c == null);
        System.assert(rps003.Current_End_in_Use__c == 'A');
        System.assert(rps003.Disposition__c == DIS_UNDR_REV);
        System.assert(rps003.Last_Rotation_Date__c == null);
        System.assert(rps003.Last_Inspection__c == lmd010.Id);
        System.assert(rps003.Last_Reported_Working_Hours__c == 300);
        System.assert(rps003.Last_Reported_Operations__c == 30);
        System.assert(rps003.Accumulated_Operations__c == 0);
        System.assert(rps003.Accumulated_Working_Hours__c == 0);
        System.assert(rps003.Status__c == null);
        System.assert(rps003.Current_Length_meters__c == 183);
        
        // validate rps 004 here
        System.assert(rps004.Last_Maintenance_Event__c == lmd016.Id);
        System.assert(rps004.Equipment_Detail__c == null);
        System.assert(rps004.Install_Date__c == Date.newInstance(2019, 12, 4));
        System.assert(rps004.Current_End_in_Use__c == 'A');
        System.assert(rps004.Disposition__c == DIS_CONT_USE);
        System.assert(rps004.Last_Rotation_Date__c == null);
        System.assert(rps004.Last_Inspection__c == null);
        System.assert(rps004.Last_Reported_Working_Hours__c == 0);
        System.assert(rps004.Last_Reported_Operations__c == 0);
        System.assert(rps004.Accumulated_Operations__c == 0);
        System.assert(rps004.Accumulated_Working_Hours__c == 0);
        System.assert(rps004.Status__c == 'Spare');
        System.assert(rps004.Current_Length_meters__c == 250);
    }
    
    private static void validateLmdsBatchOne(Map<String, Line_Maintenance_Detail__c> lmds)
    {      
        List<Id> lmdIds = new List<Id>();
        for (Line_Maintenance_Detail__c lmd : lmds.values())
        {
            lmdIds.add(lmd.Id);
        }
        Map<Id, Line_Maintenance_Detail__c> queriedLmds = new Map<Id, Line_Maintenance_Detail__c>
        (
            [
             SELECT
             Activity_Processed__c, Field_Service_Notes__c, Support_Status__c
             FROM Line_Maintenance_Detail__c
             WHERE Id IN :lmdIds
            ]
        );
        Line_Maintenance_Detail__c lmd001 = queriedLmds.get(lmds.get('LMD-001').Id);
        Line_Maintenance_Detail__c lmd002 = queriedLmds.get(lmds.get('LMD-002').Id);
        Line_Maintenance_Detail__c lmd003 = queriedLmds.get(lmds.get('LMD-003').Id);
        Line_Maintenance_Detail__c lmd004 = queriedLmds.get(lmds.get('LMD-004').Id);
        Line_Maintenance_Detail__c lmd005 = queriedLmds.get(lmds.get('LMD-005').Id);
        Line_Maintenance_Detail__c lmd006 = queriedLmds.get(lmds.get('LMD-006').Id);
        Line_Maintenance_Detail__c lmd007 = queriedLmds.get(lmds.get('LMD-007').Id);
        Line_Maintenance_Detail__c lmd008 = queriedLmds.get(lmds.get('LMD-008').Id);
        Line_Maintenance_Detail__c lmd009 = queriedLmds.get(lmds.get('LMD-009').Id);
        Line_Maintenance_Detail__c lmd010 = queriedLmds.get(lmds.get('LMD-010').Id);
        Line_Maintenance_Detail__c lmd011 = queriedLmds.get(lmds.get('LMD-011').Id);
        Line_Maintenance_Detail__c lmd012 = queriedLmds.get(lmds.get('LMD-012').Id);
        Line_Maintenance_Detail__c lmd013 = queriedLmds.get(lmds.get('LMD-013').Id);
        Line_Maintenance_Detail__c lmd014 = queriedLmds.get(lmds.get('LMD-014').Id);
        Line_Maintenance_Detail__c lmd015 = queriedLmds.get(lmds.get('LMD-015').Id);
        Line_Maintenance_Detail__c lmd016 = queriedLmds.get(lmds.get('LMD-016').Id);
        
        validateLmd(lmd001, true, null, null);
        validateLmd(lmd002, true, null, null);
        validateLmd(lmd003, true, null, 'Under Review');
        validateLmd(lmd004, true, null, 'Under Review');
        validateLmd(lmd005, true, null, 'Under Review');
        validateLmd(lmd006, true, null, 'No Support Required');
        validateLmd(lmd007, true, 'Test Notes: ; Rotation from different equipment than indicated on RPS.', 'Under Review');
        validateLmd(lmd008, false, null, null);
        validateLmd(lmd009, true, null, null);
        validateLmd(lmd010, true, null, 'No Support Required');
        validateLmd(lmd011, false, null, 'No Support Required');
        validateLmd(lmd012, false, null, 'Under Review');
        validateLmd(lmd013, true, null, 'No Support Required');
        validateLmd(lmd014, true, null, null);
        System.debug(lmd015.Field_Service_Notes__c);
        validateLmd(lmd015, true, '; some notes', 'Under Review');
        validateLmd(lmd016, true, null, 'No Support Required');
    }
    
    private static void validateEqpBatchOne(
        Map<String, Equipment_Detail__c> eqps,
        Map<String, Line_Maintenance_Detail__c> lmds, Line_Maintenance_Detail__c lmd000)
    {
        
        // get 'LMD-006'
        Line_Maintenance_Detail__c lmd006 = lmds.get('LMD-006');
        
        List<Id> eqpIds = new List<Id>();
        for (Equipment_Detail__c eqp : eqps.values())
        {
            eqpIds.add(eqp.Id);
        }
        Map<Id, Equipment_Detail__c> queriedEqps = new Map<Id, Equipment_Detail__c>
        (
            [
             SELECT
             Last_Inspection__c, Disposition__c
             FROM Equipment_Detail__c
             WHERE Id IN :eqpIds
            ]
        );
            
        Equipment_Detail__c w1 = queriedEqps.get(eqps.get('w1').Id);
        Equipment_Detail__c w2 = queriedEqps.get(eqps.get('w2').Id);
        Equipment_Detail__c w3 = queriedEqps.get(eqps.get('w3').Id);
        
        // validate w1 here
        System.assert(w1.Last_Inspection__c == lmd000.Id);
        System.assert(w1.Disposition__c == 'Continue Use');
        
        // validate w2 002 here
        System.assert(w2.Last_Inspection__c == null);
        System.assert(w2.Disposition__c == 'Continue Use');
        
        // validate w3 003 here
		System.assert(w3.Last_Inspection__c == lmd006.Id);
        System.assert(w3.Disposition__c == 'Continue Use');
    }
    
    private static void validateLmd(Line_Maintenance_Detail__c lmd, boolean actProcessed, String fsNotes, String suppStatus)
    {
        System.assert(lmd.Activity_Processed__c == actProcessed);
        System.assert(lmd.Field_Service_Notes__c == fsNotes);
        System.assert(lmd.Support_Status__c == suppStatus);
    }
 
    /**
     * This helper method will create 3 equipment details and inserts them in the database.
     * 
     * @param	asset	The asset lookup to the 3 equipment details.
     * @param	lmd000	The line maintenance detail used for some of equipment details 'Last_Inspection__c'
     * 					lookup field.
     * 
     * return	The 3 created equipment details.
     */
    private static Map<String, Equipment_Detail__c> createEqps(Asset asset, Line_Maintenance_Detail__c lmd000)
    {
        Map<String, Equipment_Detail__c> eqps = new Map<String, Equipment_Detail__c>();
               
        Equipment_Master__c eqpMaster1 = new Equipment_Master__c();
        eqpMaster1.Name = 'Test Eqp Master 1';
		insert eqpMaster1;
		Equipment_Detail__c w1 = new Equipment_Detail__c();
        w1.name = 'w1';
        w1.Equipment_Master__c = eqpMaster1.Id;
        w1.Asset__c = asset.Id;
        w1.Last_Inspection__c = lmd000.Id;
        w1.Disposition__c = DIS_CONT_USE;
        eqps.put(w1.Name, w1);
        
        Equipment_Master__c eqpMaster2 = new Equipment_Master__c();
        eqpMaster2.Name = 'Test Eqp Master 2';
		insert eqpMaster2;
        Equipment_Detail__c w2 = new Equipment_Detail__c();
        w2.name = 'w2';
        w2.Equipment_Master__c = eqpMaster2.Id;
        w2.Asset__c = asset.Id;
        w2.Disposition__c = DIS_CONT_USE;
        eqps.put(w2.Name, w2);
        
        Equipment_Master__c eqpMaster3 = new Equipment_Master__c();
        eqpMaster3.Name = 'Test Eqp Master 3';
		insert eqpMaster3;
        Equipment_Detail__c w3 = new Equipment_Detail__c();
        w3.name = 'w3';
        w3.Equipment_Master__c = eqpMaster3.Id;
        w3.Asset__c = asset.Id;
        w3.Last_Inspection__c = lmd000.Id;
        w3.Disposition__c = DIS_REPAIR;
        eqps.put(w3.Name, w3);
        
        insert eqps.values();
        
        return eqps;
    }
    
    /**
     * This helper method will create 4 rope prod specs and inserts them in the database.
     * 
     * @param	asset	The asset lookup to the 4 rope prod specs.
     * @param	lmd000	The line maintenance detail used for some of rope prod specs 'Last_Maintenance_Event__c'
     * 					and 'Last_Inspection__c' lookup fields.
     * @param	w1		The equipment detail used for some of the rope prod specs 'Equipment_Detail__c'
     * 					lookup field.
     * 
     * return	The 4 created rope prod specs.
     */
    private static Map<String, Rope_Product_Spec__c> createRpses(Asset asset, Line_Maintenance_Detail__c lmd000, Equipment_Detail__c w1)
    {
        Map<String, Rope_Product_Spec__c> rpses = new Map<String, Rope_Product_Spec__c>();
        
        Rope_Product_Spec__c rps001 = new Rope_Product_Spec__c();
        rps001.Asset__c = asset.Id;
        rps001.Last_Maintenance_Event__c = lmd000.Id;
        rps001.Last_Inspection__c = lmd000.Id;
        rps001.Equipment_Detail__c = w1.Id;
        rps001.Install_Date__c = Date.newInstance(2017, 4, 4);
        rps001.Current_End_in_Use__c = 'A';
        rps001.Disposition__c = DIS_CONT_USE;
        rps001.Last_Reported_Working_Hours__c = 100;
        rps001.Last_Reported_Operations__c = 10;
        rps001.Accumulated_Operations__c = 9;
        rps001.Accumulated_Working_Hours__c = 90;
        rps001.Current_Length_meters__c = 100;
        rpses.put('RPS-001', rps001);
        
        Rope_Product_Spec__c rps002 = new Rope_Product_Spec__c();
        rps002.Asset__c = asset.Id;
        rps002.Last_Maintenance_Event__c = lmd000.Id;
        rps002.Disposition__c = DIS_CONT_USE;
        rps002.Last_Rotation_Date__c = Date.newInstance(2018, 1, 1);
        rps002.Last_Reported_Working_Hours__c = 0;
        rps002.Last_Reported_Operations__c = 0;
        rps002.Accumulated_Operations__c = 0;
        rps002.Accumulated_Working_Hours__c = 0;
        rps002.Status__c = 'Spare';
        rps002.Current_Length_meters__c = 150;
        rpses.put('RPS-002', rps002);
        
        Rope_Product_Spec__c rps003 = new Rope_Product_Spec__c();
        rps003.Asset__c = asset.Id;
        rps003.Last_Inspection__c = lmd000.Id;
        rps003.Disposition__c = DIS_UNDR_REV;
        rps003.Current_Length_meters__c = 200;
        rpses.put('RPS-003', rps003);
        
        Rope_Product_Spec__c rps004 = new Rope_Product_Spec__c();
        rps004.Asset__c = asset.Id;
        rps004.Last_Maintenance_Event__c = lmd000.Id;
        rps004.Current_End_in_Use__c = 'A';
        rps004.Disposition__c = DIS_CONT_USE;
        rps004.Current_Length_meters__c = 250;
        rpses.put('RPS-004', rps004);
        
        insert rpses.values();
        
        return rpses;
    }
    
    /**
     * This helper method will create 16 line maintenance details and inserts them in the database.
     * 
     * @param	account		Used for the 16 lmd's account lookup value.
     * @param	asset		Used for the 16 lmd's asset lookup value.
     * @param	eqps		Used for some of the 16 lmd's equipment detail lookup value.
     * @param	rpses		Used for some of the 16 lmd's rope prod spec lookup value.
     * 
     * return	The 16 created lmds.
     */
    private static Map<String, Line_Maintenance_Detail__c> createLmds(
        Account account, Asset asset, Map<String, Equipment_Detail__c> eqps, Map<String, Rope_Product_Spec__c> rpses)
    {
        Equipment_Detail__c w1 = eqps.get('w1');
        Equipment_Detail__c w2 = eqps.get('w2');
        Equipment_Detail__c w3 = eqps.get('w3');

        Rope_Product_Spec__c rps001 = rpses.get('RPS-001');
        Rope_Product_Spec__c rps002 = rpses.get('RPS-002');
        Rope_Product_Spec__c rps003 = rpses.get('RPS-003');
        Rope_Product_Spec__c rps004 = rpses.get('RPS-004');
        
        // create lmd-001
        Line_Maintenance_Detail__c lmd001 = createLmd
            (
                account, asset, Date.newInstance(2018, 7, 1), 'Installed', rps002.Id, null, RT_STAT_UPD,
                null, w1.Id, 'A', null, null, null, null, null, null, null
            );
        // create lmd-002
        Line_Maintenance_Detail__c lmd002 = createLmd
            (
                account, asset, Date.newInstance(2018, 8, 1), null, rps002.Id, rps001.Id, RT_ASSET_ACT,
                null, w2.Id, null, (double)10.2, null, null, null, null, null, null
            );
        // create lmd-003
        Line_Maintenance_Detail__c lmd003 = createLmd
            (
                account, asset, Date.newInstance(2018, 8, 1), null, rps001.Id, null, RT_CROPPING,
                12, w2.Id, null, null, null, 'Under Review', 200, 20, 'Under Review', null
            );
        // create lmd-004
        Line_Maintenance_Detail__c lmd004 = createLmd
            (
                account, asset, Date.newInstance(2018, 10, 1), null, rps003.Id, null, RT_REPAIR,
                14, w1.Id, null, null, null, 'Under Review', 300, 30, 'Under Review', null
            );
        // create lmd-005
        Line_Maintenance_Detail__c lmd005 = createLmd
            (
                account, asset, Date.newInstance(2018, 4, 1), null, rps003.Id, null, RT_INSPECTION,
                null, w3.Id, 'A', null, null, 'Under Review', 400, 40, 'Under Review', null
            );
        // create lmd-006
        Line_Maintenance_Detail__c lmd006 = createLmd
            (
                account, asset, Date.newInstance(2018, 2, 1), null, null, null, RT_EQP_SURVEY,
                null, w3.Id, null, null, null, 'No Support Required', null, null, null, 'Continue Use'
            );
        // create lmd-007
        Line_Maintenance_Detail__c lmd007 = createLmd
            (
                account, asset, Date.newInstance(2018, 3, 1), null, rps002.Id, null, RT_ROTATION,
                null, w3.Id, null, null, 'Test Notes:', null, 600, 40, null, null
            );
        // create lmd-008
        Line_Maintenance_Detail__c lmd008 = createLmd
            (
                account, asset, Date.newInstance(2018, 5, 1), null, null, null, RT_EFE,
                null, null, null, null, null, null, null, null, null, null
            );
        // create lmd-009
        Line_Maintenance_Detail__c lmd009 = createLmd
            (
                account, asset, Date.newInstance(2018, 11, 1), null, rps002.Id, rps001.Id, RT_ASSET_ACT,
                null, w2.Id, null, (double)4.3, null, null, null, null, null, null
            );
        // create lmd-010
        Line_Maintenance_Detail__c lmd010 = createLmd
            (
                account, asset, Date.newInstance(2018, 6, 1), null, rps003.Id, null, RT_INSPECTION,
                null, null, null, null, null, 'No Support Required', null, null, null, null
            );
        // create lmd-011
        Line_Maintenance_Detail__c lmd011 = createLmd
            (
                account, asset, Date.newInstance(2018, 1, 1), null, null, null, RT_TESTING,
                null, null, null, null, null, 'No Support Required', null, null, null, null
            );
        // create lmd-012
        Line_Maintenance_Detail__c lmd012 = createLmd
            (
                account, asset, Date.newInstance(2018, 12, 1), null, null, null, RT_REQ_NEW_LINE_TRACK,
                null, null, null, null, null, 'Under Review', null, null, null, null
            );

        // create lmd-013
        Line_Maintenance_Detail__c lmd013 = createLmd
            (
                account, asset, Date.newInstance(2018, 12, 2), null, rps004.Id, null, RT_TESTING,
                null, null, null, null, null, 'No Support Required', null, null, null, null
            );
        // create lmd-014
        Line_Maintenance_Detail__c lmd014 = createLmd
            (
                account, asset, Date.newInstance(2018, 12, 3), null, rps004.Id, null, RT_EFE,
                null, w3.Id, 'B', null, null, null, null, null, null, null
            );
        // create lmd-015
        Line_Maintenance_Detail__c lmd015 = createLmd
            (
                account, asset, Date.newInstance(2019, 12, 4), 'Installed', rps004.Id, null, RT_STAT_UPD,
                null, null, 'A', null, null, 'No Support Required', null, null, null, null
            );
        // create lmd-016
        Line_Maintenance_Detail__c lmd016 = createLmd
            (
                account, asset, Date.newInstance(2019, 12, 6), 'Spare', rps004.Id, null, RT_STAT_UPD,
                null, null, null, null, null, 'No Support Required', null, null, null, null
            );
        
        Map<String, Line_Maintenance_Detail__c> lmds = new Map<String, Line_Maintenance_Detail__c>();
        lmds.put('LMD-001', lmd001);
        lmds.put('LMD-002', lmd002);
        lmds.put('LMD-003', lmd003);
        lmds.put('LMD-004', lmd004);
        lmds.put('LMD-005', lmd005);
        lmds.put('LMD-006', lmd006);
        lmds.put('LMD-007', lmd007);
        lmds.put('LMD-008', lmd008);
        lmds.put('LMD-009', lmd009);
        lmds.put('LMD-010', lmd010);
        lmds.put('LMD-011', lmd011);
        lmds.put('LMD-012', lmd012);
        lmds.put('LMD-013', lmd013);
        lmds.put('LMD-014', lmd014);
        lmds.put('LMD-015', lmd015);
        lmds.put('LMD-016', lmd016);
        
        insert lmds.values();
        
        return lmds;
    }
    
    /**
     * This helper method creates and lmd and inserts the lmd to database.
     * 
     * @param	account			Used for lmd account lookup value.
     * @param	asset			Used for lmd asset lookup value.
     * @param	inspectionDate	Used for lmd inspection date value.
     * @param	status			Used for lmd status value.
     * @param	rpsId			Used for lmd rps lookup value.
     * @param	relTailRpsId	Used for lmd related tail rps lookup value.
     * @param	recType			Used for lmd record type value.
     * @param	croppedLength	Used for lmd cropped length value.
     * @param	eqpId			Used for lmd equipment detail lookup value.
     * @param	endUse			Used for lmd Outboard_End_of_the_Line_in_Use_A_or_B__c value.
     * @param	hourseUsed		Used for lmd hours used value.
     * @param	fsNotes			Used for lmd field service notes.
     * @param	supportStatus	Used for lmd support status value.
     * @param	serviceHistory	Used for lmd service history value.
     * @param 	accNumOfOps		Used for lmd accumulated number of operations value.
     * @param	disposition		used for lmd disposition value.
     * @param	eqpDisposition	used for lmd equipment disposition value.
     * 
     * return	The newly created lmd. 
     */
    private static Line_Maintenance_Detail__c createLmd(
        Account account, Asset asset, Date inspectionDate, String status, Id rpsId, Id relTailRpsId,
        String recType, double croppedLength, Id eqpId, String endUse, double hoursUsed, String fsNotes,
        String supportStatus, double serviceHistory, double accNumOfOps, String disposition, String eqpDisposition)
    {
        Line_Maintenance_Detail__c lmd = new Line_Maintenance_Detail__c();
        lmd.Account__c = account.Id;
        lmd.Asset__c = asset.Id;
        lmd.Inspection_Date__c = inspectionDate;
        lmd.Status__c = status;
        lmd.Rope_Product_Spec__c = rpsId;
        lmd.Related_Tail_RPS__c = relTailRpsId;
        lmd.RecordTypeId = lmdRecordTypeIds.get(recType);
        lmd.Cropped_Length__c = croppedLength;
        lmd.Equipment_Detail__c = eqpId;
        lmd.Outboard_End_of_the_Line_in_Use_A_or_B__c = endUse;
        lmd.Hours_Used__c = hoursUsed;
        lmd.Field_Service_Notes__c = fsNotes;
        lmd.Support_Status__c = supportStatus;
        lmd.Service_History_hours__c = serviceHistory;
        lmd.Accumulative_Number_of_Operations__c = accNumOfOps;
        lmd.Disposition__c = disposition;
        lmd.Equipment_Disposition__c = eqpDisposition;
        
        return lmd;
    }
}