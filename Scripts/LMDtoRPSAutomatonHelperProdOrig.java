//Developed by NeuraFlash Inc on 04-04-2018
// This is a helper class to update the RPS wrt Latest LMD 

public with sharing class LMDtoRPSAutomatonHelper {
	public static void updateRPSwithLMD(){
		List<Line_Maintenance_Detail__c> toupdateLMDList = new List<Line_Maintenance_Detail__c>();
		List<Line_Maintenance_Detail__c> lmds = [SELECT Id,Winch__c,Post_Cropping_Overall_Length__c,Inspection_Date__c, Status__c,Activity_Processed__c, 
			Rope_Product_Spec__c,Outboard_End_In_Use_1__c,RecordType.Name,Cropped_Length__c,Equipment_Detail__c,Outboard_End_of_the_Line_in_Use_A_or_B__c,Hours_Used__c FROM Line_Maintenance_Detail__c 
			WHERE Rope_Product_Spec__c != null AND Activity_Processed__c = false AND Inspection_Date__c != null ORDER BY Inspection_Date__c];
		Map<Id,List<Line_Maintenance_Detail__c>> rpsLMDListMap = new Map<Id,List<Line_Maintenance_Detail__c>>();
		System.debug('LMDs queried '+lmds);
		for (Line_Maintenance_Detail__c individualLMD : lmds) {
			List<Line_Maintenance_Detail__c> temp = new List<Line_Maintenance_Detail__c>();
			if (rpsLMDListMap.containsKey(individualLMD.Rope_Product_Spec__c)) {
				temp = rpsLMDListMap.get(individualLMD.Rope_Product_Spec__c);
			}
			temp.add(individualLMD);
			rpsLMDListMap.put(individualLMD.Rope_Product_Spec__c, temp);
			System.debug('Rope - LMD Map '+rpsLMDListMap);
			individualLMD.Activity_Processed__c = true;
			toupdateLMDList.add(individualLMD);
		}

		List<Rope_Product_Spec__c> toupdateRPSList = new List<Rope_Product_Spec__c>();
		
		Map<Id,Rope_Product_Spec__c> mapRPS = new Map<Id,Rope_Product_Spec__c>([Select Id,Current_Length_meters__c,Accumulated_Working_Hours__c from Rope_Product_Spec__c where id in :rpsLMDListMap.keySet()]);
		for (Id individualRPSId:rpsLMDListMap.keySet()){
			Rope_Product_Spec__c toupdateRPS = new Rope_Product_Spec__c(Id =individualRPSId);
			Line_Maintenance_Detail__c mostRecentLMD = LMDtoRPSServiceClass.returnMostRecentLMD(rpsLMDListMap.get(individualRPSId));

			if(mostRecentLMD.RecordType.Name == 'Status Update' && String.isNotEmpty(mostRecentLMD.Status__c)) {
				toupdateRPS.Status__c = mostRecentLMD.Status__c;
				if(mostRecentLMD.Status__c == 'Installed') {
					toupdateRPS.Equipment_Detail__c = mostRecentLMD.Equipment_Detail__c;
					if(toupdateRPS.Install_Date__c == null) {
						toupdateRPS.Install_Date__c = mostRecentLMD.Inspection_Date__c;
					}
				} else if (mostRecentLMD.Status__c == 'Spare' || mostRecentLMD.Status__c == 'Retired') {
					toupdateRPS.Equipment_Detail__c = null;
				}
			}else {
				if(LMDtoRPSServiceClass.returnMostRecentLMDbyRecordType(rpsLMDListMap.get(individualRPSId),'Status Update') != null && (LMDtoRPSServiceClass.returnMostRecentLMDbyRecordType(rpsLMDListMap.get(individualRPSId),'Status Update')).Status__c != null) {
					toupdateRPS.Status__c = LMDtoRPSServiceClass.returnMostRecentLMDbyRecordType(rpsLMDListMap.get(individualRPSId),'Status Update').Status__c;
				}
			}
			if(mostRecentLMD.RecordType.Name == 'Rotation') {
				if(mostRecentLMD.Winch__c != null) {
					toupdateRPS.Current_Postion__c = mostRecentLMD.Winch__c;
				}
				if(mostRecentLMD.Equipment_Detail__c != null){
					toupdateRPS.Equipment_Detail__c = mostRecentLMD.Equipment_Detail__c;
				}
			} else {
				if(LMDtoRPSServiceClass.returnMostRecentLMDbyRecordType(rpsLMDListMap.get(individualRPSId),'Rotation') != null) {
					if(LMDtoRPSServiceClass.returnMostRecentLMDbyRecordType(rpsLMDListMap.get(individualRPSId),'Rotation').Winch__c != null) {
						toupdateRPS.Current_Postion__c = LMDtoRPSServiceClass.returnMostRecentLMDbyRecordType(rpsLMDListMap.get(individualRPSId),'Rotation').Winch__c;
					}
					if(LMDtoRPSServiceClass.returnMostRecentLMDbyRecordType(rpsLMDListMap.get(individualRPSId),'Rotation').Equipment_Detail__c != null) {
						toupdateRPS.Equipment_Detail__c = LMDtoRPSServiceClass.returnMostRecentLMDbyRecordType(rpsLMDListMap.get(individualRPSId),'Rotation').Equipment_Detail__c;
					}
				}
			}
			if (mostRecentLMD.RecordType.Name == 'EFE' && String.isNotEmpty(mostRecentLMD.Outboard_End_of_the_Line_in_Use_A_or_B__c)) {
				toupdateRPS.Current_End_in_Use__c =  mostRecentLMD.Outboard_End_of_the_Line_in_Use_A_or_B__c;
			} else {
				if(LMDtoRPSServiceClass.returnMostRecentLMDbyRecordType(rpsLMDListMap.get(individualRPSId),'EFE') != null && LMDtoRPSServiceClass.returnMostRecentLMDbyRecordType(rpsLMDListMap.get(individualRPSId),'EFE').Outboard_End_of_the_Line_in_Use_A_or_B__c != null ) {
					toupdateRPS.Current_End_in_Use__c = LMDtoRPSServiceClass.returnMostRecentLMDbyRecordType(rpsLMDListMap.get(individualRPSId),'EFE').Outboard_End_of_the_Line_in_Use_A_or_B__c;
				}
			}
			if (mostRecentLMD.RecordType.Name == 'Inspection'){
				toupdateRPS.Last_Inspection__c = mostRecentLMD.Id;
			} else {
				if (LMDtoRPSServiceClass.returnMostRecentInspectionLMD(rpsLMDListMap.get(individualRPSId)) != null){
					toupdateRPS.Last_Inspection__c = LMDtoRPSServiceClass.returnMostRecentInspectionLMD(rpsLMDListMap.get(individualRPSId)).Id;
				}
			}
			if (mostRecentLMD.RecordType.Name != 'Inspection'){
				toupdateRPS.Last_Maintenance_Event__c = mostRecentLMD.Id;
			} else {
				if (LMDtoRPSServiceClass.returnMostRecentNonInspectionLMD(rpsLMDListMap.get(individualRPSId)) != null ){
					toupdateRPS.Last_Maintenance_Event__c = LMDtoRPSServiceClass.returnMostRecentNonInspectionLMD(rpsLMDListMap.get(individualRPSId)).Id;
				}
			}
			if (mapRPS.get(individualRPSId).Current_Length_meters__c != null && mapRPS.get(individualRPSId).Current_Length_meters__c != 0){
				toupdateRPS.Current_Length_meters__c = mapRPS.get(individualRPSId).Current_Length_meters__c - LMDtoRPSServiceClass.returnTotalCroppedLength(rpsLMDListMap.get(individualRPSId));
			}
			if(mapRPS.get(individualRPSId).Accumulated_Working_Hours__c != null ) {
				toupdateRPS.Accumulated_Working_Hours__c = mapRPS.get(individualRPSId).Accumulated_Working_Hours__c + LMDtoRPSServiceClass.returnTotalWorkedHours(rpsLMDListMap.get(individualRPSId));
			}
			
			toupdateRPSList.add(toupdateRPS);
		}
		try{
			update toupdateRPSList;
			update toupdateLMDList;
		} catch(DmlException e) {
			throw e;
		}
		
	}
}