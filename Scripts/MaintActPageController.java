public with sharing class MaintActPageController {

    private static Id contactId;
    private static Id accountId;
    //private static List<Id> assetIds;
    
    static {
        if (UserInfo.getUserType() == 'Standard') {
            contactId = [SELECT Id FROM Contact WHERE Email = :UserInfo.getUserEmail()].Id;
        }
        else {
            contactId = [SELECT ContactId FROM User WHERE Id = :UserInfo.getUserId()].ContactId;
        }
        accountId = [SELECT AccountId FROM Contact WHERE Id = :contactId].AccountId;
        /*
        assetIds = new List<Id>();
        for (Related_Contacts__c rc : [SELECT Asset__c FROM Related_Contacts__c WHERE Asset__c != '' AND Contact__c = :contactId]) {
            assetIds.add(rc.Asset__c);
        }
		*/
    }
    
    // load assets for the given account and contact
    @RemoteAction
    public static List<Asset> loadAssets() {
        return [SELECT 
                Id, 
                Name
                FROM Asset
                WHERE AccountId = :accountId /*AND Id IN :assetIds*/
            	ORDER BY Name];
    }
    
    @RemoteAction
    public static List<Line_Maintenance_Detail__c> loadLineMaintenanceDetails() {
        return [SELECT 
                Id,
                Asset__r.Name,
                RecordType.Name, 
                Rope_Product_Spec__r.Disposition__c,
                Cert__r.Name,
                Equipment_Detail__r.Disposition__c,
                Port_Country__c,
                Port_Name__c,
                Date_Time_of_All_Fast__c,
                Date_Time_of_All_Let_Go__c,
                Hours_Used__c,
                Support_Status__c,
                Inspection_Date__c,
                Resolution_chafe__c,
                Observation_Chafe__c,
                Work_Order__c,
                Work_Order__r.Conclusions_Recommendations__c
                FROM Line_Maintenance_Detail__c
                WHERE Account__c = :accountId /*AND Asset__c IN :assetIds*/];
    }
    
    // get all WorkOrders for the account
    @RemoteAction
    public static List<WorkOrder> loadWorkOrders() {
        return [SELECT 
                Id,
                WorkType.Name,
                RecordType.Name,
                Asset.Id,
                Asset.Name,
                Inspection_Date__c,
                Status,
                Event_Type__c,
                Facilitating_Party__c,
                Location_of_Service__c,
                Rout_Insp_href__c
                FROM WorkOrder
                WHERE AccountId = :accountId /*AND AssetId IN :assetIds*/];
    }
    
    @RemoteAction
    public static List<ContentDocumentLink> getCDLs() {
        return [SELECT LinkedEntityId FROM ContentDocumentLink WHERE LinkedEntityId IN (SELECT Id FROM WorkOrder WHERE AccountId = :accountId /*AND Asset.Id IN :assetIds*/)];
    }
}