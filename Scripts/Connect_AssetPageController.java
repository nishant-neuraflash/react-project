public class Connect_AssetPageController {
    private static Id contactId;
    private static Id accountId;
    private static List<Id> assetIds;
    
    static {
        if (UserInfo.getUserType() == 'Standard') {
            contactId = [SELECT Id FROM Contact WHERE Email = :UserInfo.getUserEmail()].Id;
        }
        else {
            contactId = [SELECT ContactId FROM User WHERE Id = :UserInfo.getUserId()].ContactId;
        }
        accountId = [SELECT AccountId FROM Contact WHERE Id = :contactId].AccountId;
        assetIds = new List<Id>();
        for (Related_Contacts__c rc : [SELECT Asset__c FROM Related_Contacts__c WHERE Asset__c != '' AND Contact__c = :contactId]) {
            assetIds.add(rc.Asset__c);
        }
    }
    
    // load assets for the given account and contact
    @RemoteAction
    public static List<Asset> loadAssets() {
        return [SELECT 
                Id, 
                Name, 
                AccountId, 
                Fleet__c,
                RecordType.Name,
                Type_of_Asset__c,
                Asset_Class__c                
                FROM Asset
                WHERE AccountId = :accountId AND Id IN :assetIds
            	ORDER BY Name];
    }
    
    // load all rope prod specs for the given account
    @RemoteAction
    public static List<Rope_Product_Spec__c> loadRopeProductSpecs() {
        return [SELECT 
              	Id, 
                Cert__c, 
                Cert__r.Name,
                Product_List__c,
                Name,
                Equipment_Detail__c, 
                Equipment_Detail__r.Name,
                Install_Date__c,
                Accumulated_Working_Hours__c,
                Disposition__c,
                (SELECT 
                 Id, 
                 Work_Order__r.WorkType.Name, 
                 Inspection_Date__c,
                 Full_Cut_Strands__c,
                 Bearing_Point_Condition__c,
                 External_Abrasion_1__c,
                 External_Abrasion_2__c,
                 Internal_Abrasion_1__c,
                 Internal_Abrasion_2__c,
                 Support_Status__c,
                 RecordType.Name
                 FROM Line_Maintenance_Details__r 
                 Where Inspection_Date__c != null AND RecordType.Name != 'Asset Activity' AND RecordType.Name != 'Equipment Survey'
                 ORDER BY Inspection_Date__c DESC LIMIT 1
                ),
                Status__c,
                Rope_Line__c,
                Asset__c,
                Asset__r.Fleet__c,
                RecordType.Name
                FROM Rope_Product_Spec__c
            	WHERE 
                (Status__c = 'Installed' OR Status__c = 'Spare') AND
                Account__c = :accountId AND Asset__c IN :assetIds];
     }
    
    // load equipment detaisl for the given account
    @RemoteAction
    public static List<Equipment_Detail__c> loadEquipmentDetails() {
        return [SELECT 
                Id, 
                Name, 
                X_Coordinate__c, 
                Y_Coordinate__c,
                Rotation__c,
                Equipment_Type__c,
                Disposition__c,
                Location__c,
                Ship_Side__c,
                (SELECT 
                 Id, 
                 Rope_Line__c 
                 FROM Rope_Product_Specs__r
                ),
				(SELECT 
                 Id, 
                 Inspection_Date__c, 
                 Scoring__c, 
                 Pitting_Rust__c,
                 Support_Status__c,
                 Mobility__c
                 FROM Line_Maintenance_Details__r 
                 Where Inspection_Date__c != null AND RecordType.Name = 'Equipment Survey'
                 ORDER BY Inspection_Date__c DESC LIMIT 1
                ),
                Asset__c,
                Asset__r.Fleet__c,
                Asset__r.Max_Num_Equip_Vertical__c,
                Current_Surface_Rating__c,
                RecordType.Name,
                Winch_drum_Type__c
                FROM Equipment_Detail__c
                WHERE Asset__c IN (SELECT Id FROM Asset WHERE AccountId = :accountId) AND Asset__c IN :assetIds AND
                X_Coordinate__c != '' AND
                Y_Coordinate__c != '' AND
                RecordTypeId != ''];
    }
    
    @RemoteAction
    public static List<Line_Maintenance_Detail__c> loadLineMaintenanceDetails() {
        return [SELECT 
                Id, 
                RecordType.Name,
                Inspection_Date__c,
                Work_Order__c,
                Asset__c,
                Work_Order__r.Event_Type__c,
                Work_Order__r.Facilitating_Party__c,
                Work_Order__r.Rout_Insp_href__c,
                Support_Status__c,
                Ship_Side__c,
                Rope_Product_Spec__c,
                Work_Order__r.WorkType.Name,
                Port_Country__c,
                Port_Name__c,
                Date_Time_of_All_Fast__c,
                Date_Time_of_All_Let_Go__c,
                Hours_Used__c,
                Sheltered_Exposed_Port__c,
                Equipment_Detail__r.Name,
                Outboard_End_of_the_Line_in_Use_A_or_B__c,
                Severe_Loading_Conditions__c,
                Inspector_Observation__c
            FROM Line_Maintenance_Detail__c
            WHERE Account__c = :accountId AND Asset__c IN :assetIds
            AND Inspection_Date__c != null
            LIMIT 10000];
    }
    
    @RemoteAction
    public static List<ContentDocumentLink> getCDLs() {
        return [SELECT LinkedEntityId FROM ContentDocumentLink WHERE LinkedEntityId IN(SELECT Id FROM WorkOrder WHERE AccountId = :accountId AND Asset.Id IN :assetIds)];
    }
}