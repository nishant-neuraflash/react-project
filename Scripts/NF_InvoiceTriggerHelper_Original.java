/*
** Class:  NF_InvoiceTriggerHelper
** Created by Neuraflash
** Description: Helper class for sharing Invoice records with Community Users.
*/
public class NF_InvoiceTriggerHelper {
    /*
        Method Name: allocateCommunitySharing
        Param: List of Invoices from Trigger.new
        Description: This method will be called on After Insert event on Invoice Trigger
    */
    public static void allocateCommunitySharing(List<Invoice__c> listInvoices){
        //variable declaration
        set<Id> setofAccountId = new set<Id>();
        map<Id,Set<Id>> mapofAccountInvoiceOld = new map<Id,Set<Id>>();
        map<Id,Id> mapofAccountInvoicenew = new map<Id,Id>();
        map<Id,Set<Id>> mapofAccountwithUser = new map<Id,Set<Id>>();
        map<Id,List<Id>> mapofInvoiceShareUsers = new map<Id,List<Id>>();

        for(Invoice__c eachInvoice : listInvoices){
            setofAccountId.add(eachInvoice.Account__c);
            mapofAccountInvoicenew.put(eachInvoice.id,eachInvoice.Account__c);
        }//for ends
        if(!setofAccountId.isEmpty()){
            set<Id> fetchedinvoiceId = new set<Id>();
            List<Account> listAccounts = [SELECT Id,(SELECT Id FROM Invoices1__r) FROM account WHERE Id IN: setofAccountId]; // query to fetch all invoices realted to parents
            for(account eachAccount : listAccounts){
                List<Invoice__c> listInv = eachAccount.Invoices1__r;
                for(Invoice__c eachInvoice : listInv){
                    fetchedinvoiceId.add(eachInvoice.id);
                }
                mapofAccountInvoiceOld.put(eachAccount.id,fetchedinvoiceId);
            }
            if(!fetchedinvoiceId.isEmpty()){
                List<Invoice__share> listInvoiceShareRecords = [Select id,parentid,userorgroupId,accesslevel from Invoice__share where parentid in: fetchedinvoiceId];
                if(!listInvoiceShareRecords.isEmpty()){
                    for(Invoice__share eachShare : listInvoiceShareRecords){
                        if(mapofInvoiceShareUsers.containsKey(eachShare.parentid)){
                            list<id> listUserId = mapofInvoiceShareUsers.get(eachShare.parentid);
                            listUserId.add(eachShare.userorgroupId);
                            mapofInvoiceShareUsers.put(eachShare.parentid,listUserId);
                        }
                        else{
                            list<id> listUserId = new List<id>();
                            listUserId.add(eachShare.userorgroupId);
                            mapofInvoiceShareUsers.put(eachShare.parentid,listUserId);
                        }
                    }
                }
            }
            for(Id accId : mapofAccountInvoiceOld.keyset()){
                set<id> setAllusers = new set<Id>();
                set<id> allInvoiceid = mapofAccountInvoiceOld.get(accId);
                for(Id invId : allInvoiceid){
                    setAllusers.addAll(mapofInvoiceShareUsers.get(invId));
                }
                mapofAccountwithUser.put(accId,setAllusers); //creating map of one account with all community users derived from respective invoice sharing present in system
            }
        } // setofAccountId if ends
        if(mapofAccountwithUser.size() > 0){
            Map<Id,List<Id>> sobjectUserMap = new Map<Id,List<Id>>();
            for(Invoice__c eachInv : listInvoices){
                Id accId = mapofAccountInvoicenew.get(eachInv.id);
                set<Id> userIds = mapofAccountwithUser.get(accId);
                if(userIds != null){
                    list<id> userIdList = new List<Id>();
                    userIdList.addAll(userIds);
                    sobjectUserMap.put(eachInv.id,userIdList);
                }
            }
            if(sobjectUserMap.size() > 0){
                try{
                    list<Sobject> insertShareRecords = NF_InvoiceTriggerHelper.shareRecordInsert(sobjectUserMap); // helper method to create sharing records
                    Database.SaveResult[] srlist =  Database.insert(insertShareRecords,false);
                    for(Database.SaveResult sr : srlist){
                        if(sr.isSuccess()){
                            system.debug('Insert Success');
                        }
                    }
                }
                catch(Exception e){
                    NF_ExceptionEventHandler errorHandler = new NF_ExceptionEventHandler(e);
                    errorHandler.createErrorLog();
                }
            }
        }
    }

    /*
        Method Name: shareRecordInsert
        Param: Map of recordId and List of users to share this record with
        Description: This helper method is called to create share records for insertion
    */
    public static List<SObject> shareRecordInsert(Map<Id,List<Id>> SObjectUserMap){
        NF_ApexShareServiceUtilClass shareService = new NF_ApexShareServiceUtilClass(SObjectUserMap,Label.NF_Read);
        List<SObject> insertedShares = shareService.shareRecords(true);
        return insertedShares;
    }
}