/*
** Class:  NF_InvoiceTriggerHelper
** Created by Neuraflash
** Description: Helper class for sharing Invoice records with Community Users.
*/
public class NF_InvoiceTriggerHelper {
    /*
        Method Name: allocateCommunitySharing
        Param: List of Invoices from Trigger.new
        Description: This method will be called on After Insert event on Invoice Trigger
    */
    public static void allocateCommunitySharing(List<Invoice__c> listInvoices)
    {
        //variable declaration
        set<Id> setofAccountId = new set<Id>(); // unique account id of invoices passed for processing
        map<Id,Set<Id>> mapofAccountInvoiceOld = new map<Id,Set<Id>>();
        /**
         * map of our passed invoices' ids with their corresponding account ids
         * here key is invioce key and value is account id
         * Note: This map is not necessary ???(you can access the account id from the invoice record)???
         */ 
        map<Id,Id> mapofAccountInvoicenew = new map<Id,Id>();   
        /**
         * map of account id as key and list of unique user ids as value 
         * (DANGER -- list of users seem to be the users under all accounts)
         */
        map<Id,Set<Id>> mapofAccountwithUser = new map<Id,Set<Id>>();
        /**
         * map of invoice id as key and list of users tied to the invoice 
         * through the shares of the invoice as value
         */ 
        map<Id,List<Id>> mapofInvoiceShareUsers = new map<Id,List<Id>>();

        // iterate through each invoice
        for(Invoice__c eachInvoice : listInvoices)
        {
            setofAccountId.add(eachInvoice.Account__c); // add account id of invoice to our account id set
            mapofAccountInvoicenew.put(eachInvoice.id, eachInvoice.Account__c);  // add each invoice id with its corresponding account id
        }

        // check to make sure our list of unique accounts is not empty
        if(!setofAccountId.isEmpty())
        {
            /**
             * this list will contain the unique invoice id of all invoices 
             */
            set<Id> fetchedinvoiceId = new set<Id>();
            /**
             * query to fetch all invoices realted to parents
             * list of accounts with each account containing a reference to all its invoices
             * Note: Invoices1 here is a child relationship to the account. By querying Invoices1__r
             * under account we are pulling all child invoices for that account
            */
            List<Account> listAccounts = [SELECT Id, (SELECT Id FROM Invoices1__r) FROM account WHERE Id IN: setofAccountId]; 

            // iterate through each unique account derived from the passed invoices
            for(account eachAccount : listAccounts)
            {
                // fetchedinvoiceId = new set<Id>();
                
                List<Invoice__c> listInv = eachAccount.Invoices1__r;    // list of child invoices for each account
                // iterate through each child invoice for the account
                for(Invoice__c eachInvoice : listInv)
                {
                    fetchedinvoiceId.add(eachInvoice.id);   // add invoice id the list of unique invoice ids
                }
                /**
                 * ====================== DANGER DANGER DANGER =========================
                 * add an account with unique list of all invoices from ???<all the accounts>????
                 */
                mapofAccountInvoiceOld.put(eachAccount.id, fetchedinvoiceId);
            }

            // check to make sure we have invoices ???NOT NECESSARY???
            if(!fetchedinvoiceId.isEmpty())
            {
                /**
                 * this list will contain all our existing 
                 * share records for all invoices under all accounts derived 
                 * from our original passed list of invoices
                 */
                List<Invoice__share> listInvoiceShareRecords = [Select id, parentid, userorgroupId, accesslevel from Invoice__share where parentid in: fetchedinvoiceId];

                // check to make sure we have share records
                if(!listInvoiceShareRecords.isEmpty())
                {
                    // iterate through each invoice in our list of ALL invoices
                    for(Invoice__share eachShare : listInvoiceShareRecords)
                    {

                        // if we have already added a <invoice id, user id list> key, pair value to our map
                        if(mapofInvoiceShareUsers.containsKey(eachShare.parentid))
                        {
                            /**
                             * get the list of users for each share based on the share's parent id
                             * Note: parent id of share record here points to the invoice
                             */
                            list<id> listUserId = mapofInvoiceShareUsers.get(eachShare.parentid);
                            listUserId.add(eachShare.userorgroupId);    // add this user/group id to the list of user ids
                            // update our map wher key is invoice id and value is list of users for that invoice
                            mapofInvoiceShareUsers.put(eachShare.parentid, listUserId);
                        }
                        // this is a new invoice entry
                        else
                        {
                            list<id> listUserId = new List<id>();   // create a new user id list
                            listUserId.add(eachShare.userorgroupId);    // add our first user id
                            mapofInvoiceShareUsers.put(eachShare.parentid, listUserId); // add our first entry to the map
                        }
                    }
                }
            }

            // iterate through our unique list of account ids
            for(Id accId : mapofAccountInvoiceOld.keyset())
            {
                set<id> setAllusers = new set<Id>();
                /**
                 * ==================== DANGER DANGER DANGER =====================
                 * this is the list of all invoices for all the accounts not just for this account
                 */
                set<id> allInvoiceid = mapofAccountInvoiceOld.get(accId);
                // iterate through each invoices in our ALL invoices under all accounts
                for(Id invId : allInvoiceid)
                {
                    /**
                     * =================== DANGER =====================
                     * set of users here should be the users under current account
                     * but it seems it is all users under all accounts
                     */
                    setAllusers.addAll(mapofInvoiceShareUsers.get(invId));
                }
                /**
                 * creating map of one account with all community users 
                 * derived from respective invoice sharing present in system
                 * =================== DANGER =====================
                 * list of users is users across all accounts
                 */
                mapofAccountwithUser.put(accId, setAllusers); 
            }
        } // setofAccountId if ends

        // make sure we have accoutns
        if(mapofAccountwithUser.size() > 0)
        {
            /**
             * map where key is invoice id of original invoices and 
             * value is list of all users within the account
             * =================== DANGER =====================
             * list of users is users across all accounts
             */
            Map<Id,List<Id>> sobjectUserMap = new Map<Id,List<Id>>();

            // iterate through our original invoices
            for(Invoice__c eachInv : listInvoices)
            {
                Id accId = mapofAccountInvoicenew.get(eachInv.id);  // get account id of invoice ???(the map isn't necessary)???
                /**
                 * here we will get all the users under the account tied to this invoice
                 * we need all the users under the account that this invoice is so that 
                 * we can create a share record for every user for this invoice
                 */
                set<Id> userIds = mapofAccountwithUser.get(accId);
                // check to make sure we have suers
                if(userIds != null)
                {
                    list<id> userIdList = new List<Id>();   // create list of users
                    userIdList.addAll(userIds); // add users from user set to user list ???(do you really need to convert the set to a list)???
                    /**
                     * =================== DANGER =====================
                     * list of users is users across all accounts
                     */
                    sobjectUserMap.put(eachInv.id, userIdList);
                }
            }

            // check to make sure we have invoices with users
            if(sobjectUserMap.size() > 0)
            {
                try
                {
                    /**
                     * helper method to create sharing records
                     */
                    list<Sobject> insertShareRecords = NF_InvoiceTriggerHelper.shareRecordInsert(sobjectUserMap); 
                    // insert the newly created share records
                    Database.SaveResult[] srlist =  Database.insert(insertShareRecords,false);
                    // iterate through each newly created share record
                    for(Database.SaveResult sr : srlist)
                    {
                        if(sr.isSuccess()){
                            system.debug('Insert Success');
                        }
                    }
                }
                catch(Exception e){
                    NF_ExceptionEventHandler errorHandler = new NF_ExceptionEventHandler(e);
                    errorHandler.createErrorLog();
                }
            }
        }
    }

    /*
        Method Name: shareRecordInsert
        Param: Map of recordId and List of users to share this record with
        Description: This helper method is called to create share records for insertion
    */
    public static List<SObject> shareRecordInsert(Map<Id,List<Id>> SObjectUserMap){
        NF_ApexShareServiceUtilClass shareService = new NF_ApexShareServiceUtilClass(SObjectUserMap,Label.NF_Read);
        List<SObject> insertedShares = shareService.shareRecords(true);
        return insertedShares;
    }
}