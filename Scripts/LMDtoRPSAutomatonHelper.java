// Developed by NeuraFlash Inc on 04-04-2018
// This is a helper class to update the RPS wrt Latest LMD 
// Modified by Samsonrope Technologies on 12/28/2018
public with sharing class LMDtoRPSAutomatonHelper 
{
	public static void updateRPSwithLMD() 
	{
		// query lmds from the server 
		List<Line_Maintenance_Detail__c> lmds = 
		[
			SELECT
            Id,
            Inspection_Date__c,
            Status__c,
			Activity_Processed__c,
            Rope_Product_Spec__c,
            Related_Tail_RPS__c,
			RecordType.Name,
            Cropped_Length__c,
            Equipment_Detail__c, 
			Outboard_End_of_the_Line_in_Use_A_or_B__c,
            Hours_Used__c,
            Field_Service_Notes__c,
            Support_Status__c,
            Winch_Number_orig__c,
            Service_History_hours__c,
            Accumulative_Number_of_Operations__c,
            Disposition__c,
            Equipment_Disposition__c
			FROM Line_Maintenance_Detail__c 
			WHERE 
            	(
                    (Rope_Product_Spec__c != null AND
                     (
                      RecordType.Name = 'Status Update' OR RecordType.Name = 'Rotation' OR
                      RecordType.Name = 'EFE' OR RecordType.Name = 'Inspection' OR
                      RecordType.Name = 'Cropping' OR RecordType.Name = 'Repair' OR
                      RecordType.Name = 'Asset Activity' OR RecordType.Name = 'Testing' OR
                      RecordType.Name = 'Request New Line to Track'
                     )
                    ) OR 
                   (Equipment_Detail__c != null AND RecordType.Name = 'Equipment Survey')
                ) AND
                Activity_Processed__c = false AND Inspection_Date__c != null 
			ORDER BY Inspection_Date__c
		];
		// output the list of lmds queried
		// System.debug('LMDs queried ' + lmds);

		Set<Id> rpsIds = new Set<Id>();	// set of rps ids tied to lmds (mainline or tail)
        Set<Id> eqpIds = new Set<Id>();	// set of equipments tied to lmds (only lmd of equipment survey)
		// pre-process lmds to get uniqe rps ids
		for (Line_Maintenance_Detail__c lmd : lmds)
        {
            if (lmd.Rope_Product_Spec__c != null) 
            {
                rpsIds.add(lmd.Rope_Product_Spec__c);
            }
            if (lmd.Related_Tail_RPS__c != null) 
            {
                rpsIds.add(lmd.Related_Tail_RPS__c);
            }
            if (lmd.Equipment_Detail__c != null)
            {
                eqpIds.add(lmd.Equipment_Detail__c);
            }
		}
		// query rpses from database 
		// in this map the key is the RPS id and the value is the RPS
		Map<Id, Rope_Product_Spec__c> rpsMap = new Map<Id, Rope_Product_Spec__c>
            (
                [Select
                 Id,
                 Last_Maintenance_Event__c,
                 Last_Maintenance_Event__r.Inspection_Date__c,
                 Equipment_Detail__c,
                 Equipment_Detail__r.Name,
                 Install_Date__c,
                 Current_End_in_Use__c,
                 Disposition__c,
                 Last_Rotation_Date__c,
                 Last_Inspection__c,
                 Last_Inspection__r.Inspection_Date__c,
                 Last_Reported_Working_Hours__c,
                 Last_Reported_Operations__c,
                 Accumulated_Operations__c,
                 Accumulated_Working_Hours__c,
                 Current_Length_meters__c
                 FROM Rope_Product_Spec__c WHERE Id IN : rpsIds]
            );
        // query equipment details from database 
		// in this map the key is the equipment id and the value is the equipment
		Map<Id, Equipment_Detail__c> eqpMap = new Map<Id, Equipment_Detail__c>
            (
                [Select
                 Id,
                 Last_Inspection__c,
                 Last_Inspection__r.Inspection_Date__c,
                 Disposition__c
                 FROM Equipment_Detail__c WHERE Id IN : eqpIds]
            );
        Rope_Product_Spec__c rps;	// the RPS of this LMD to be updated
        Equipment_Detail__c eqp;	// the eqp of this lmd to be updated
        
		// iterate through the lmds queried from the server
        for (Line_Maintenance_Detail__c lmd : lmds)
        {
			rps = rpsMap.get(lmd.Rope_Product_Spec__c);
			switch on lmd.RecordType.Name 
			{
				when 'Status Update' 
				{	// when lmd 'Record Type' is 'Status Update'
					UpdateRpsWithLmdOfStatusUpdate(rps, lmd);
				}	
				when 'Rotation' 
				{	// when lmd 'Record Type' is 'Rotation'
					UpdateRpsWithLmdOfRotation(rps, lmd);
				}
				when 'EFE' 
				{	// when lmd 'Record Type' is 'EFE'
					UpdateRpsWithLmdOfEfe(rps, lmd);
				}
				when 'Inspection' 
				{	// when lmd 'Record Type' is 'Inspection'
					UpdateRpsWithLmdOfInspection(rps, lmd);
				}
				when 'Cropping', 'Repair' 
				{	// when lmd 'Record Type' is 'Cropping' or 'Repair'
					UpdateRpsWithLmdOfCroppingOrRepair(rps, lmd);
				}
				when 'Asset Activity' 
                {	// when lmd 'Record Type' is 'Asset Activity'
                    UpdateRpsWithLmdOfAssetActivity(rps, lmd);
                    rps = rpsMap.get(lmd.Related_Tail_RPS__c);
                    if (rps != null)
                    {
                        UpdateRpsWithLmdOfAssetActivity(rps, lmd);
                    }
				}
                when 'Testing' 
				{	// when lmd 'Record Type' is 'Testing'
					UpdateRpsWithLmdOfTesting(rps, lmd);
				}
                when 'Equipment Survey' 
				{	// when lmd 'Record Type' is 'Equipment Survey'
                    eqp = eqpMap.get(lmd.Equipment_Detail__c);
					UpdateRpsWithLmdOfEquipmentSurvey(eqp, lmd);
				}
				when else 
				{	// default block, optional
					// code block 4
				}
			}

			// set the 'Activity Processed' field of this LMD to true
			lmd.Activity_Processed__c = true;
		}

		try
		{
			update rpsMap.values();
            update eqpMap.values();
            update lmds;
		} 
		catch (DmlException e)
		{
			throw e;
		}
	}

	// this method is called to update the RPS from the LMD when the LMD 'Record Type' is 'Status Update'
	private static void UpdateRpsWithLmdOfStatusUpdate (Rope_Product_Spec__c rps, Line_Maintenance_Detail__c lmd) 
	{
		// if lmd status is not empty
		if (String.isNotEmpty(lmd.Status__c)) 
		{
            // set rps status to be the status of the lmd
			rps.Status__c = lmd.Status__c;	
			// if our lmd status is 'Installed'
			if (lmd.Status__c == 'Installed') 
			{
                if (lmd.Equipment_Detail__c == null) 
                {
                    UpdateSuppStatusAndFSNotes(lmd, 'some notes');
                }
                else 
                {
                    rps.Equipment_Detail__c = lmd.Equipment_Detail__c;
                }
                
                if (rps.Install_Date__c == null) 
                {
                    // set our rps 'Install Date' to be the lmd 'Inspection Date'
					rps.Install_Date__c = lmd.Inspection_Date__c;	
                }
                
                UpdateRpsEndInUse(rps, lmd, 'some notes');
			}
            
			// if our lmd status is 'Spare' or 'Retired'
			else if (lmd.Status__c == 'Spare' || lmd.Status__c == 'Retired') 
			{
				rps.Equipment_Detail__c = null;	// set our rps EQP to be null
			}
		}
        UpdateRpsLastMaintEventLmd(rps, lmd);
	}

	// this method is called to update the RPS from the LMD when the LMD 'Record Type' is 'Rotation'
	private static void UpdateRpsWithLmdOfRotation(Rope_Product_Spec__c rps, Line_Maintenance_Detail__c lmd) 
	{
        if (rps.Equipment_Detail__c == null || !rps.Equipment_Detail__r.Name.Equals(lmd.Winch_Number_orig__c)) 
        {
            rps.Disposition__c = 'Under Review';
            UpdateSuppStatusAndFSNotes(lmd, 'Rotation from different equipment than indicated on RPS.');
        }
        
		if (lmd.Equipment_Detail__c == null)
		{
			rps.Disposition__c = 'Under Review';
            UpdateSuppStatusAndFSNotes(lmd, 'Rotation entered with no Equipment Detail.');
		}
		else if (rps.Last_Rotation_Date__c == null || lmd.Inspection_Date__c > rps.Last_Rotation_Date__c) 
        {
        	rps.Equipment_Detail__c = lmd.Equipment_Detail__c;
            rps.Last_Rotation_Date__c = lmd.Inspection_Date__c;
        }
        UpdateRpsLastReportedHoursAndOps(rps, lmd);
	}

	// this method is called to update the RPS from the LMD when the LMD 'Record Type' is 'EFE'
	private static void UpdateRpsWithLmdOfEfe(Rope_Product_Spec__c rps, Line_Maintenance_Detail__c lmd) 
	{
        UpdateRpsEndInUse(rps, lmd, 'EFE entered without appropriate data');
        UpdateRpsLastReportedHoursAndOps(rps, lmd);
        UpdateRpsLastMaintEventLmd(rps, lmd);
	}

	// this method is called to update the RPS from the LMD when the LMD 'Record Type' is 'Inspection'
	private static void UpdateRpsWithLmdOfInspection(Rope_Product_Spec__c rps, Line_Maintenance_Detail__c lmd) 
	{
        UpdateRpsDisposition(rps, lmd);
        UpdateRpsLastReportedHoursAndOps(rps, lmd);
        UpdateRpsLastInspectionEventLmd(rps, lmd);
	}
    
    private static void UpdateRpsDisposition(Rope_Product_Spec__c rps, Line_Maintenance_Detail__c lmd)
    {
        if (lmd.Disposition__c == 'Under Review')
        {
            rps.Disposition__c = lmd.Disposition__c;
        }
    }

	// this method is called to update the RPS from the LMD when the LMD 'Record Type' is 'Cropping' or 'Repair'
	private static void UpdateRpsWithLmdOfCroppingOrRepair(Rope_Product_Spec__c rps, Line_Maintenance_Detail__c lmd) 
	{
        Decimal lmdCroppedLength = GetNumber(lmd.Cropped_Length__c);
		if (lmdCroppedLength > 0)
		{
			rps.Current_Length_meters__c = Math.Max(0, GetNumber(rps.Current_Length_meters__c) - (lmdCroppedLength + 3));
		}
        UpdateRpsDisposition(rps, lmd);
        UpdateRpsLastReportedHoursAndOps(rps, lmd);
        UpdateRpsLastMaintEventLmd(rps, lmd);
	}
    
    // helper to convert null number to 0
    private static Decimal GetNumber(Decimal num)
    {
        return num == null ? 0 : num;
    }
    
    // helper to convert null string to emtpy 
    private static String GetString(String text)
    {
        return text == null ? '' : text;
    }

	// this method is called to update the RPS from the LMD when the LMD 'Record Type' is 'Asset Activity'
	private static void UpdateRpsWithLmdOfAssetActivity(Rope_Product_Spec__c rps, Line_Maintenance_Detail__c lmd) 
	{
        Decimal lmdHoursUsed = GetNumber(lmd.Hours_Used__c);
        
		rps.Accumulated_Working_Hours__c = GetNumber(rps.Accumulated_Working_Hours__c) + lmdHoursUsed;
        rps.Accumulated_Operations__c = GetNumber(rps.Accumulated_Operations__c) + 1;
        
        if (lmdHoursUsed == 0)
        {
            UpdateSuppStatusAndFSNotes(lmd, 'Dates entered incorrectly, hours used not calculated correctly.');
        }
	}
    
    // this method is called to update the RPS from the LMD when the LMD 'Record Type' is 'Testing'
	private static void UpdateRpsWithLmdOfTesting(Rope_Product_Spec__c rps, Line_Maintenance_Detail__c lmd) 
	{
		UpdateRpsLastMaintEventLmd(rps, lmd);
	}
    
    // this method is called to update the EQP from the LMD when the LMD 'Record Type' is 'Equipment Survey'
	private static void UpdateRpsWithLmdOfEquipmentSurvey(Equipment_Detail__c eqp, Line_Maintenance_Detail__c lmd) 
	{
        // check to see if this lmd event date is greater than the 
        // previous inspection event lmd tied to the eqp
        if (eqp.Last_Inspection__c == null || lmd.Inspection_Date__c > eqp.Last_Inspection__r.Inspection_Date__c) 
        {
            // set eap 'Last Inspection Event Id' to be 'lmd Id'
            eqp.Last_Inspection__c = lmd.Id;	
        }
        eqp.Disposition__c = lmd.Equipment_Disposition__c;
    }
    
    private static void UpdateSuppStatusAndFSNotes(Line_Maintenance_Detail__c lmd, String notes)
    {
        lmd.Support_Status__c = 'Under Review';
        lmd.Field_Service_Notes__c = GetString(lmd.Field_Service_Notes__c) + ' ; ' + notes;
    }
    
    // set rps end in use to be the end in use of this lmd
    private static void UpdateRpsEndInUse(Rope_Product_Spec__c rps, Line_Maintenance_Detail__c lmd, String notes)
    {
        if (lmd.Outboard_End_of_the_Line_in_Use_A_or_B__c == null) 
        {
            UpdateSuppStatusAndFSNotes(lmd, notes);
        }
        else 
        {
            rps.Current_End_in_Use__c = lmd.Outboard_End_of_the_Line_in_Use_A_or_B__c;
        }
    }
    
    // set rps last maintenance event lmd to be this lmd
    private static void UpdateRpsLastMaintEventLmd(Rope_Product_Spec__c rps, Line_Maintenance_Detail__c lmd)
    {
        // check to see if this lmd event date is greater than the 
        // previous maintenance event lmd tied to the rps
        if (rps.Last_Maintenance_Event__c == null || lmd.Inspection_Date__c > rps.Last_Maintenance_Event__r.Inspection_Date__c) 
        {
            // set rps 'Last Maintenance Event Id' to be 'lmd Id'
            rps.Last_Maintenance_Event__c = lmd.Id;	
        }
    }
    
    // set rps last inspection event lmd to be this lmd
    private static void UpdateRpsLastInspectionEventLmd(Rope_Product_Spec__c rps, Line_Maintenance_Detail__c lmd)
    {
        // check to see if this lmd event date is greater than the 
        // previous inspection event lmd tied to the rps
        if (rps.Last_Inspection__c == null || lmd.Inspection_Date__c > rps.Last_Inspection__r.Inspection_Date__c) 
        {
            // set rps 'Last Inspection Event Id' to be 'lmd Id'
            rps.Last_Inspection__c = lmd.Id;	
        }
    }
    
    // set rps last reported working hours to be the last reported hours of the lmd
    // set rps last reported operations to be the last reported operations of the lmd
    private static void UpdateRpsLastReportedHoursAndOps(Rope_Product_Spec__c rps, Line_Maintenance_Detail__c lmd)
    {
        if ((rps.Last_Inspection__c == null || lmd.Inspection_Date__c > rps.Last_Inspection__r.Inspection_Date__c) && 
            (rps.Last_Maintenance_Event__c == null || lmd.Inspection_Date__c > rps.Last_Maintenance_Event__r.Inspection_Date__c))
        {
            if (lmd.Service_History_hours__c > 0) 
            {
                rps.Last_Reported_Working_Hours__c = lmd.Service_History_hours__c;
            }
            
            if (lmd.Accumulative_Number_of_Operations__c > 0)
            {
                rps.Last_Reported_Operations__c = lmd.Accumulative_Number_of_Operations__c;
            }
        }
    }
}