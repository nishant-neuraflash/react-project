@isTest
private class LMDtoRPSAutomationTest {
	public static String CRON_EXP = '0 0 0 15 3 ? 2022';

	
	@isTest static void test_method_one() {
		Account ac = new Account(Name='TestAcc',Account_Type__c='Agent',Category__c='1-Arborist');
		insert ac;
		Fleet__c f = new Fleet__c(Name='TestFleet',Operator__c=ac.Id,Retirement_Criteria_in_Years__c=2);
		insert f;
		Customer_Entitlement__c c = new Customer_Entitlement__c (Fleet__c =f.Id,Planned_Maintenance_Events_Per_Year__c ='1',Routine_Inspections_Per_Year__c ='1', Entitlement_Term__c ='2');
		insert c;
		Asset ast = new Asset(Name='TestAsset',AccountId=ac.id,End_User_Account__c=ac.id,Crane_Manufacturer__c='Manitex',Crane_Type__c='Crawler Crane',Asset_Entitlement__c=c.Id);
		insert ast;
		Rope_Product_Spec__c rps = new Rope_Product_Spec__c(Asset__c = ast.Id);
		insert rps;
		Id recordTypeInspection = Schema.SObjectType.Line_Maintenance_Detail__c.getRecordTypeInfosByName().get('Inspection').getRecordTypeId();
		Id recordTypeCropping = Schema.SObjectType.Line_Maintenance_Detail__c.getRecordTypeInfosByName().get('Cropping').getRecordTypeId();
		Id recordTypeStatus_update = Schema.SObjectType.Line_Maintenance_Detail__c.getRecordTypeInfosByName().get('Status Update').getRecordTypeId();
		Date d1 = Date.newInstance(2018, 04, 05);
		List<Line_Maintenance_Detail__c> lmdList = new List<Line_Maintenance_Detail__c>();
		Line_Maintenance_Detail__c lmd1 = new Line_Maintenance_Detail__c(RecordTypeId=recordTypeInspection,Rope_Product_Spec__c=rps.id,Account__c=ac.Id,Asset__c=ast.Id,Inspection_Date__c = d1,Outboard_End_in_use_1__c='A');
		lmdList.add(lmd1);
		Date d2 = Date.newInstance(2018, 04, 05);
		Line_Maintenance_Detail__c lmd2 = new Line_Maintenance_Detail__c(RecordTypeId=recordTypeInspection,Rope_Product_Spec__c=rps.id,Account__c=ac.Id,Asset__c=ast.Id ,Inspection_Date__c = d2,Outboard_End_in_use_1__c='B');
		lmdList.add(lmd2);
		Date d3 = Date.newInstance(2018, 04, 04);
		Line_Maintenance_Detail__c lmd3 = new Line_Maintenance_Detail__c(RecordTypeId=recordTypeCropping,Rope_Product_Spec__c=rps.id,Account__c=ac.Id,Asset__c=ast.Id ,Inspection_Date__c = d3,Outboard_End_in_use_1__c='B');
		lmdList.add(lmd3);
		Date d7 = Date.newInstance(2018, 07, 01);
		Line_Maintenance_Detail__c lmd7 = new Line_Maintenance_Detail__c(RecordTypeId=recordTypeStatus_update,Rope_Product_Spec__c=rps.id,Account__c=ac.Id,Asset__c=ast.Id ,Inspection_Date__c = d7,Outboard_End_in_use_1__c='B',Cropped_Length__c = 5,Status__c ='Installed');
		lmdList.add(lmd7);

		insert lmdList;
		Test.startTest();
		        // Schedule the test job
        String jobId = System.schedule('ScheduledApexTest',
            CRON_EXP, 
            new LMDtoRPSAutomaton());
        Test.stopTest();
	}
	
	@isTest static void test_method_two() {
		Account ac = new Account(Name='TestAcc',Account_Type__c='Agent',Category__c='1-Arborist');
		insert ac;
		Fleet__c f = new Fleet__c(Name='TestFleet',Operator__c=ac.Id,Retirement_Criteria_in_Years__c=2);
		insert f;
		Customer_Entitlement__c c = new Customer_Entitlement__c (Fleet__c =f.Id,Planned_Maintenance_Events_Per_Year__c ='1',Routine_Inspections_Per_Year__c ='1', Entitlement_Term__c ='2');
		insert c;
		Asset ast = new Asset(Name='TestAsset',AccountId=ac.id,End_User_Account__c=ac.id,Crane_Manufacturer__c='Manitex',Crane_Type__c='Crawler Crane',Asset_Entitlement__c=c.Id);
		insert ast;
		Rope_Product_Spec__c rps = new Rope_Product_Spec__c(Asset__c = ast.Id,Current_Length_meters__c=4);
		insert rps;
		Id recordTypeInspection = Schema.SObjectType.Line_Maintenance_Detail__c.getRecordTypeInfosByName().get('Inspection').getRecordTypeId();
		Id recordTypeCropping = Schema.SObjectType.Line_Maintenance_Detail__c.getRecordTypeInfosByName().get('Cropping').getRecordTypeId();
		Id recordTypeRotation = Schema.SObjectType.Line_Maintenance_Detail__c.getRecordTypeInfosByName().get('Rotation').getRecordTypeId();
		Id recordTypeRepair = Schema.SObjectType.Line_Maintenance_Detail__c.getRecordTypeInfosByName().get('Repair').getRecordTypeId();
		Id recordTypeStatus_update = Schema.SObjectType.Line_Maintenance_Detail__c.getRecordTypeInfosByName().get('Status Update').getRecordTypeId();
		Id recordTypeEFE = Schema.SObjectType.Line_Maintenance_Detail__c.getRecordTypeInfosByName().get('EFE').getRecordTypeId();
		Date d1 = Date.newInstance(2018, 04, 05);
		List<Line_Maintenance_Detail__c> lmdList = new List<Line_Maintenance_Detail__c>();
		Line_Maintenance_Detail__c lmd1 = new Line_Maintenance_Detail__c(RecordTypeId=recordTypeInspection,Rope_Product_Spec__c=rps.id,Account__c=ac.Id,Asset__c=ast.Id,Inspection_Date__c = d1,Outboard_End_in_use_1__c='A');
		lmdList.add(lmd1);
		Date d2 = Date.newInstance(2018, 04, 04);
		Line_Maintenance_Detail__c lmd2 = new Line_Maintenance_Detail__c(RecordTypeId=recordTypeInspection,Rope_Product_Spec__c=rps.id,Account__c=ac.Id,Asset__c=ast.Id ,Inspection_Date__c = d2,Outboard_End_in_use_1__c='B');
		lmdList.add(lmd2);
		Date d3 = Date.newInstance(2018, 04, 07);
		Line_Maintenance_Detail__c lmd3 = new Line_Maintenance_Detail__c(RecordTypeId=recordTypeCropping,Rope_Product_Spec__c=rps.id,Account__c=ac.Id,Asset__c=ast.Id ,Inspection_Date__c = d3,Outboard_End_in_use_1__c='B');
		lmdList.add(lmd3);
		Date d4 = Date.newInstance(2018, 04, 12);
		Line_Maintenance_Detail__c lmd4 = new Line_Maintenance_Detail__c(RecordTypeId=recordTypeRepair,Rope_Product_Spec__c=rps.id,Account__c=ac.Id,Asset__c=ast.Id ,Inspection_Date__c = d4,Outboard_End_in_use_1__c='B',Cropped_Length__c = 5);
		lmdList.add(lmd4);
		Equipment_Master__c equipMaster = new Equipment_Master__c(Name = 'Test master Equip');
		insert equipMaster;
		Equipment_Detail__c equipDetail = new Equipment_Detail__c(name='Test Equip',Equipment_Master__c=equipMaster.Id,Asset__c=ast.Id);
		insert equipDetail;
		Line_Maintenance_Detail__c lmd5 = new Line_Maintenance_Detail__c(RecordTypeId=recordTypeRotation,Rope_Product_Spec__c=rps.id,Account__c=ac.Id,Asset__c=ast.Id ,Inspection_Date__c = d4,Outboard_End_in_use_1__c='B',Cropped_Length__c = 5,Winch__c='testWinch',Equipment_Detail__c=equipDetail.Id);
		lmdList.add(lmd5);
		Line_Maintenance_Detail__c lmd6 = new Line_Maintenance_Detail__c(RecordTypeId=recordTypeStatus_update,Rope_Product_Spec__c=rps.id,Account__c=ac.Id,Asset__c=ast.Id ,Inspection_Date__c = d4,Outboard_End_in_use_1__c='B',Cropped_Length__c = 5,Status__C ='Spare');
		lmdList.add(lmd6);
		insert lmdList;
		LMDtoRPSAutomaton obj = new LMDtoRPSAutomaton();
		obj.execute(null);
	}
	
}