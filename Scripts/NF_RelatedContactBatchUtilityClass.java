/*
** Class:  NF_RelatedContactBatchUtilityClass
** Created by Neuraflash
** Description: Helper class for batch class processing.
*/
public without sharing class NF_RelatedContactBatchUtilityClass {
    public static Map<Id, List<Id>> childWOShareData = new Map<Id, List<Id>>(); //share record map for child records Workorders of Asset with parent fleet
    
    /*
        Method Name: utilcreateShareMapMethod
        Parameters: 1) List of Related_Contacts__c
                    2) String Value of Field name whose value will be used as Key to generate Map
                    3) Map of contact id against community user id
        Return Type: Map of Parentid with list of userid's to share record with
        Description: This method will be called on when Map of Parentid with
        list of userid's to share record needs to be created
    */

    public static Map<Id,List<Id>> utilcreateShareMapMethod(List<Related_Contacts__c> recordList,String fieldName,Map<Id,Id>mapofUsers){
        
        Map<Id,List<Id>> mapofShareRecords = new Map<Id,List<Id>>();
        // creating Map for removing sharing records against a fleet
        for(Related_Contacts__c eachRecord : recordList){
            Object obj = eachRecord.get(fieldName);
            List<Id> fleetList = new List<Id>();
            if(mapofShareRecords.containsKey(String.valueOf(obj))){
                fleetList.addAll(mapofShareRecords.get(String.valueOf(obj)));
                fleetList.add(mapofUsers.get(eachRecord.Contact__c));
            }else{
                fleetList.add(mapofUsers.get(eachRecord.Contact__c));
            }
            mapofShareRecords.put((Id)obj,fleetList);
        }

        return mapofShareRecords;

    }
    /*
        Method Name: shareRecordInsert
        Parameters:  1) Map of Parentid with list of userid's to share record with
        Return Type: List of inserted share records as list of SObjects
        Description: This method will be called record are to be inserted using Util class
    */
    public static List<SObject> shareRecordInsert(Map<Id,List<Id>> SObjectUserMap){
        NF_ApexShareServiceUtilClass shareService = new NF_ApexShareServiceUtilClass(SObjectUserMap,Label.NF_Read);
        List<SObject> insertedShares = shareService.shareRecords(true);
        return insertedShares;
    }
    public static List<SObject> shareRecordInsertwithAccess(Map<Id,List<Id>> SObjectUserMap, String accessLevel){
        NF_ApexShareServiceUtilClass shareService = new NF_ApexShareServiceUtilClass(SObjectUserMap,accessLevel);
        List<SObject> insertedShares = shareService.shareRecords(true);
        return insertedShares;
    }
    /*
        Method Name: shareRecordDelete
        Parameters:  1) Map of Parentid with list of userid's to share record with
        Return Type: List of deleted share records as list of SObjects
        Description: This method will be called when record are to be deleted using Util class
    */
    public static List<SObject> shareRecordDelete(Map<Id,List<Id>> sobjectUserMap){
        NF_ApexShareServiceUtilClass shareService = new NF_ApexShareServiceUtilClass(sobjectUserMap);
        List<SObject> deletedShares = shareService.deleteShares(true);
        return deletedShares;
    }
    /*
        Method Name: UtilcreateContactToUserMap
        Parameters:  1) List of Related Contacts
        Return Type: Map of Contact Id with community user id the contact is related with
        Description: This method will be called we need to find community user associated with the contact
    */
    public static Map<Id,Id> UtilcreateContactToUserMap(List<Related_Contacts__c> listofRecord){

        Map<Id,Id> contacttoUserMap = new Map<Id,Id>();
        Set<Id> setofContactID = new Set<Id>();

        for (Related_Contacts__c eachRecord : listofRecord) {
            setofContactID.add(eachRecord.Contact__c);
        }
        List<User> userList = [SELECT Id,ContactId FROM User WHERE ContactId IN:setofContactID AND IsActive = TRUE];
        if (!userList.isEmpty()) {
            for (User eachUser : userList) {
                contacttoUserMap.put(eachUser.ContactId, eachUser.Id);
            }
        }
        return contacttoUserMap;
    }
    /*
        Method Name: UtilFleetwithChildAsset
        Parameters:     1) Set of Fleet Id's
                        2) List of Fleet Records to be shared
                        3) Map of Fleet Id with List of all users to share that parent with
        Return Type: Map of child record Id with List of all users to share that record with
        Description: This method will be called when we require sharing at child object(Asset) level
    */
    public static Map<Id,List<Id>> UtilFleetwithChildAsset(Set<Id> setofFleetID,List<Related_Contacts__c> SharingRecordlist,Map<Id, List<Id>> mapofFleetUsers){

        Map<Id, List<Id>> mapofFleetAssetId = new Map<Id, List<Id>>();
        Map<Id, List<Id>> childShareData = new Map<Id, List<Id>>(); //share record map for child records
        Set<Id> setofWOAssets =  new Set<Id>();
        Map<Id, List<Id>> mapofAssetWOId = new Map<Id, List<Id>>();
        if(!setofFleetID.isEmpty()){
            List<Fleet__c> listofleets = [SELECT Id, (SELECT Id FROM Related_Asset__r) FROM Fleet__c WHERE Id IN: setofFleetID];
            for (Fleet__c fleet : listofleets) {
                List<Id> assetIDlist = new List<Id>();
                List<Asset> listAsset = fleet.Related_Asset__r;
                if(!listAsset.isEmpty()){
                    for (Asset eachAsset : listAsset) {
                        setofWOAssets.add(eachAsset.Id);
                        assetIDlist.add(eachAsset.Id);
                    }
                    mapofFleetAssetId.put(fleet.Id, assetIDlist); //map of each fleet with list of all Asset belonging to that fleet
                }
            }
            // Now fetch all child work orgders for each asset
            if(!setofWOAssets.isEmpty()){
                List<Asset> listofWOAssets = [SELECT Id, (SELECT Id FROM WorkOrders) FROM Asset WHERE Id IN: setofWOAssets];
                for(Asset eachAsset : listofWOAssets){
                    List<Id> allWOIDlist = new List<Id>();
                    List<WorkOrder> WOIdlist = eachAsset.WorkOrders;
                    for (WorkOrder eachWO : WOIdlist){
                        allWOIDlist.add(eachWO.Id);
                    }
                    mapofAssetWOId.put(eachAsset.Id, allWOIDlist); //map of each asset with list of all workorders belonging to that fleet
                }
            }
            if(mapofFleetAssetId.size() > 0){
                // creating Map for inserting child sharing records
                for (Related_Contacts__c eachRecord : SharingRecordlist) {
                    List<Id> listOfAssetId = mapofFleetAssetId.get(eachRecord.Fleet__c);
                    if(!listOfAssetId.isEmpty()){
                        for (Id assetId : listOfAssetId){
                            List<Id> childRecordUserList = new List<Id>();
                            childRecordUserList.addAll(mapofFleetUsers.get(eachRecord.Fleet__c));
                            List<Id> listofWOId = mapofAssetWOId.get(assetId);
                            if(!listofWOId.isEmpty()){
                                for(Id woId : listofWOId){
                                    NF_RelatedContactBatchUtilityClass.childWOShareData.put(woId, childRecordUserList); 
                                }
                            }
                            childShareData.put(assetId, childRecordUserList);
                        }
                    }
                }
            }
        }
        return childShareData;
    }

    /*
        Method Name:    UtilFleetwithChildwork Order
        Parameters:     NONE
        Description:    This method will be called when we require sharing at child object(Workorder) level
    */
    public static Map<Id,List<Id>> UtilFleetwithChildWorkOrder(){
        Map<Id, List<Id>> childWorkOrder =  NF_RelatedContactBatchUtilityClass.childWOShareData; //share record map for child records Workorders of Asset with parent fleet
        return childWorkOrder;
    }
    /*
        Method Name: UtilAssetwithChildInvoice
        Parameters:     1) Set of Asset Id's
                        2) List of Related Contact Records to be shared
                        3) Map of Asset Id with List of all users to share that parent with
        Return Type: Map of child record Id with List of all users to share that record with
        Description: This method will be called when we require sharing at child object(Invoice) level
    */
    public static Map<Id,List<Id>> UtilAssetwithChildInvoice(Set<Id> setofAssetID,List<Related_Contacts__c> SharingRecordlist,Map<Id, List<Id>> mapofAssetUsers){
        Map<Id, List<Id>> mapofAccountInvoiceId = new Map<Id, List<Id>>();
        Map<Id, Id> mapofAssetAccounts = new Map<Id, Id>();
        Map<Id, List<Id>> childShareData = new Map<Id, List<Id>>(); //share record map for child invoices
        if (!setofAssetID.isEmpty()) {
            List<Asset> assetList = [SELECT Id,AccountId FROM Asset WHERE Id IN:setofAssetID];
            if (!assetList.isEmpty()) {
                Set<Id> setOfAccID = new Set<Id>();
                for (Asset eachAsset : assetList) {
                    setOfAccID.add(eachAsset.AccountId);
                    mapofAssetAccounts.put(eachAsset.Id, eachAsset.AccountId); //map of asset with account id
                }
                if (!setOfAccID.isEmpty()) {
                    List<Id> invID = new List<Id>();
                    List<Account> listofAccts = [SELECT Id, (SELECT Id FROM Invoices1__r) FROM Account WHERE Id IN:setOfAccID];
                    for (Account acc : listofAccts) {
                        List<Invoice__c> listINV = acc.Invoices1__r;
                        if(!listINV.isEmpty()){
                            for (Invoice__c inv : listINV) {
                                invID.add(inv.Id);
                            }
                            mapofAccountInvoiceId.put(acc.Id, invID); //map of each account with list opf all invoices belonging to that account
                        }
                    }
                }
            }
        }
        if (mapofAccountInvoiceId.size() > 0) {

            // creating Map for inserting child sharing records
            for (Related_Contacts__c eachRecord : SharingRecordlist) {
                Id accId = mapofAssetAccounts.get(eachRecord.Asset__c);
                List<Id> listOfInvoiceId = mapofAccountInvoiceId.get(accId);
                if(!listOfInvoiceId.isEmpty()){
                    for (Id InvId : listOfInvoiceId) {
                        List<Id> childRecordUserList = new List<Id>();
                        childRecordUserList.addAll(mapofAssetUsers.get(eachRecord.Asset__c));
                        childShareData.put(InvId, childRecordUserList);
                    }
                }
            }
        }
        return childShareData;
    }
    /*
        Method Name: UtilAssetwithChildWorkOrder
        Parameters:     1) Set of Asset Id's
                        2) List of Asset Records to be shared
                        3) Map of Asset Id with List of all users to share that parent with
        Return Type: Map of child record Id with List of all users to share that record with
        Description: This method will be called when we require sharing at child object(Work Order) level
    */
    public static Map<Id,List<Id>> UtilAssetwithChildWorkOrder(Set<Id> setofAssetID,List<Related_Contacts__c> SharingRecordlist,Map<Id, List<Id>> mapofAssetUsers){
        Map<Id, List<Id>> mapofAssetWOId = new Map<Id, List<Id>>();
        Map<Id, List<Id>> childShareData = new Map<Id, List<Id>>(); //share record map for child work orders
        if(!setofAssetID.isEmpty()){
            List<Asset> listofAssets = [SELECT Id, (SELECT Id FROM WorkOrders) FROM Asset WHERE Id IN: setofAssetID];
            for (Asset eachAsset : listofAssets) {
                List<Id> allWOIDlist = new List<Id>();
                List<WorkOrder> WOIdlist = eachAsset.WorkOrders;
                if(!WOIdlist.isEmpty()){
                    for (WorkOrder eachWO : WOIdlist) {
                        allWOIDlist.add(eachWO.Id);
                    }
                    mapofAssetWOId.put(eachAsset.Id, allWOIDlist); //map of each asset with list of all workorgers belonging to that asset
                }
            }
            if(mapofAssetWOId.size() > 0){
                // creating Map for inserting child sharing records
                for (Related_Contacts__c eachRecord : SharingRecordlist) {
                    List<Id> listOfWOId = mapofAssetWOId.get(eachRecord.Asset__c);
                    if(!listOfWOId.isEmpty()){
                        for (Id WOId : listOfWOId){
                            List<Id> childRecordUserList = new List<Id>();
                            childRecordUserList.addAll(mapofAssetUsers.get(eachRecord.Asset__c));
                            childShareData.put(WOId, childRecordUserList);
                        }
                    }
                }
            }
        }
        return childShareData;
    }

    /*
        Method Name:    validateBatchCall
        Parameters:     1) List of Related Contact(Trigger.new)
                        2) Map of Related Contact(Trigger.OldMap)
                        3) Trigger Event
        Return Type:    None
        Description:    This method will be called to validate if we require to fire the batch for sharing records with community
                        user
    */
    public static void validateBatchCall(List<Related_Contacts__c> listRecords, Map<Id, Related_Contacts__c> oldRecordsMap, String operationType)
    {
        try
        {
            List<Id> listofRCIds = new List<Id>();
            String query = Label.NF_SharingBatchQuery + ' listofRCrecords';
            switch on operationType
            {
                when 'After Insert'
                {
                    for(Related_Contacts__c eachRC : listRecords)
                    {
                        listofRCIds.add(eachRC.Id);
                    }
                }
                when 'After Update'
                {
                    for(Related_Contacts__c eachRC : listRecords)
                    {
                        Related_Contacts__c oldRC = oldRecordsMap.get(eachRC.Id);
                        if (oldRC.Role__c != eachRC.Role__c)
                        {
                            if(NF_RelatedContactBatchUtilityClass.validateRelatedContactRole(eachRC) == false || NF_RelatedContactBatchUtilityClass.validateRelatedContactRole(oldRC) == true)
                            {
                                listofRCIds.add(eachRC.Id);
                            }
                            else if(NF_RelatedContactBatchUtilityClass.validateRelatedContactRole(eachRC) == true || NF_RelatedContactBatchUtilityClass.validateRelatedContactRole(oldRC) == false)
                            {
                                listofRCIds.add(eachRC.Id);
                            }
                        }
                        if (eachRC.Share_All_Asset_Data__c)
                        {
                            if (oldRecordsMap.get(eachRC.Id).Fleet__c != eachRC.Fleet__c)
                            {
                                listofRCIds.add(eachRC.Id);
                            }
                        } //if ends
                        else
                        {
                            if (oldRecordsMap.get(eachRC.Id).Asset__c != eachRC.Asset__c)
                            {
                                listofRCIds.add(eachRC.Id);
                            }
                        }
                        if(oldRecordsMap.get(eachRC.Id).Share_All_Asset_Data__c != eachRC.Share_All_Asset_Data__c)
                        {
                            listofRCIds.add(eachRC.Id);
                        }
                    }
                }
                when 'Before Delete'
                {
                    for(Id eachRC : oldRecordsMap.keySet())
                    {
                        listofRCIds.add(eachRC);
                    }
                }
            }

            // call batch only if Related contacts are validated for sharing records
            if(!listofRCIds.isEmpty())
            {
                //callBatch
                switch on operationType
                {
                    when 'After Insert'
                    {
                        NF_RelatedContactTriggerHelperBatch insertBatch = new NF_RelatedContactTriggerHelperBatch(listofRCIds, Label.NF_After_Insert ,query);
                        Database.executeBatch(insertBatch, 10);
                    }
                    when 'After Update'
                    {
                        NF_RelatedContactTriggerHelperBatch updateBatch = new NF_RelatedContactTriggerHelperBatch(listofRCIds, oldRecordsMap, Label.NF_After_Update ,query);
                        Database.executeBatch(updateBatch,10);
                    }
                    when 'Before Delete'
                    {
                        NF_RelatedContactTriggerHelperBatch deleteBatch = new NF_RelatedContactTriggerHelperBatch(oldRecordsMap, Label.NF_Before_Delete ,query);
                        deleteBatch.deleteTriggerShareRecords(oldRecordsMap);
                    }
                }
            }
        }
        catch(Exception ex)
        {
            if(NF_RelatedContactBatchUtilityClass.ValidateErrorLogCheck())
            {
                NF_ExceptionEventHandler errorHandler = new NF_ExceptionEventHandler(ex,null);
                errorHandler.createExceptionLog();
            }
        }
    }

    public static Boolean validateShareAllCheckbox(Related_Contacts__c rcRecords){
        if (rcRecords.Share_All_Asset_Data__c) {
            return true;
        }
        else{
            return false;
        }
    }
    public static Boolean validateRelatedContactRole(Related_Contacts__c rcRecords){
        if (rcRecords.Role__c == Label.NF_Manager || rcRecords.Role__c == Label.NF_Superintendent || rcRecords.Role__c == Label.NF_Purchasing) {
            return true;
        }
        else{
            return false;
        }
    }

    /**
     * check to see if the trigger is activated
     * 
     * @param   devName     trigger name
     * @return              boolean value indicating whether or not the trigger is active
     */
    public static Boolean validateTriggerActivation(String devName)
    {
        TriggerSetting__mdt metadata = [SELECT isActive__c FROM TriggerSetting__mdt WHERE DeveloperName =: devName];
        if(metadata.isActive__c)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public static Boolean ValidateErrorLogCheck(){
        TriggerSetting__mdt metadata = [SELECT DeveloperName,isActive__c FROM TriggerSetting__mdt WHERE DeveloperName =: Label.NF_ErrorLogCheck];
        if(metadata.isActive__c){
            return true;
        }
        else{
            return false;
        }
    }
}