//Developed by NeuraFlash Inc on 04-04-2018
// This is a helper class to update the RPS wrt Latest LMD 

public with sharing class LMDtoRPSAutomatonHelper {
	public static void updateRPSwithLMD() {

		// list of lmds to be updated
		List<Line_Maintenance_Detail__c> toupdateLMDList = new List<Line_Maintenance_Detail__c>();

		// query lmds from the server 
		List<Line_Maintenance_Detail__c> lmds = 
		[
			SELECT Id,Winch__c,Post_Cropping_Overall_Length__c,Inspection_Date__c,Status__c,Activity_Processed__c, 
			Rope_Product_Spec__c,Outboard_End_In_Use_1__c,RecordType.Name,Cropped_Length__c,Equipment_Detail__c,
			Outboard_End_of_the_Line_in_Use_A_or_B__c,Hours_Used__c 
			FROM Line_Maintenance_Detail__c 
			WHERE Rope_Product_Spec__c != null AND Activity_Processed__c = false AND Inspection_Date__c != null 
			ORDER BY Inspection_Date__c
		];

		// create a map where the key is the RPS id and the value is a list of LMDs tied to the RPS
		Map<Id,List<Line_Maintenance_Detail__c>> rpsLMDListMap = new Map<Id,List<Line_Maintenance_Detail__c>>();

		// output the list of lmds queried
		System.debug('LMDs queried ' + lmds);

		// iterate through the lmds queried from the server
		for (Line_Maintenance_Detail__c individualLMD : lmds) {
			// create a temp LMD list
			List<Line_Maintenance_Detail__c> temp = new List<Line_Maintenance_Detail__c>();
			// check to see if we have an entry with this LMD's RPS id in our map
			if (rpsLMDListMap.containsKey(individualLMD.Rope_Product_Spec__c)) {
				// if we have this RPS id/key in our map let's get the LMDs/value
				temp = rpsLMDListMap.get(individualLMD.Rope_Product_Spec__c);
			}
			// now let's add this LMD to the list of lmds with the same RPS id
			temp.add(individualLMD);
			// now let's updated our map with the RPS id/key and its LMDs/value
			rpsLMDListMap.put(individualLMD.Rope_Product_Spec__c, temp);
			// output our RPS => LMDs map content for debug purposes
			System.debug('Rope - LMD Map ' + rpsLMDListMap);
			// set the 'Activity Processed' field of this LMD to true
			individualLMD.Activity_Processed__c = true;
			// now let's add this LMD to our 'toUpdatedLmdList' 
			toupdateLMDList.add(individualLMD);
		}

		// list containing our RPSes that we are going to update
		List<Rope_Product_Spec__c> toupdateRPSList = new List<Rope_Product_Spec__c>();
		
		// create rps map by querying RPSes from the server where RPS id is in our 'rpsLMDListMap' keyset
		Map<Id,Rope_Product_Spec__c> mapRPS = new Map<Id,Rope_Product_Spec__c>
		(
			[Select Id,Current_Length_meters__c,Accumulated_Working_Hours__c 
			FROM Rope_Product_Spec__c where id in :rpsLMDListMap.keySet()]
		);

		// iterate through our keys of the 'rpsLmdListMap'
		for (Id individualRPSId:rpsLMDListMap.keySet()) {
			// create a new RPS with the current RPS id
			Rope_Product_Spec__c toupdateRPS = new Rope_Product_Spec__c(Id =individualRPSId);
			// get the most recent LMD for the list of LMDs tied to this RPS id/key
			Line_Maintenance_Detail__c mostRecentLMD = LMDtoRPSServiceClass.returnMostRecentLMD(rpsLMDListMap.get(individualRPSId));

			// if our most recent LMD has {RecordType == 'Status Update'} and {Status is not empty}
			if(mostRecentLMD.RecordType.Name == 'Status Update' && String.isNotEmpty(mostRecentLMD.Status__c)) 
			{
				// set the status of the RPS to be the status of the most recent LMD
				toupdateRPS.Status__c = mostRecentLMD.Status__c;
				// if our LMD status is 'Installed'
				if(mostRecentLMD.Status__c == 'Installed') 
				{
					// set our RPS EQP to be the most recent LMD EQP
					toupdateRPS.Equipment_Detail__c = mostRecentLMD.Equipment_Detail__c;
					// check to see if our RPS 'Install Date' is null
					if(toupdateRPS.Install_Date__c == null) {
						// set our RPS 'Install Date' to be the most recent LMD 'Inspection Date'
						toupdateRPS.Install_Date__c = mostRecentLMD.Inspection_Date__c;
					}
				}
				// if our LMD status is 'Spare' or 'Retired'
				else if (mostRecentLMD.Status__c == 'Spare' || mostRecentLMD.Status__c == 'Retired') 
				{
					// set our RPS EQP to be null
					toupdateRPS.Equipment_Detail__c = null;
				}
			}
			// if our most recent LMD doesn't have {RecordType == 'Status Update'} or {Status is empty}
			else 
			{
				// get our most recent LMD with the given RPS id with 'Record Type' == 'Status Update'
				Line_Maintenance_Detail__c mostRecentLMDbyRecordType = 
					LMDtoRPSServiceClass.returnMostRecentLMDbyRecordType(rpsLMDListMap.get(individualRPSId),'Status Update');

				// check to make sure our returned lmd is not null and its status is not null
				if(mostRecentLMDbyRecordType != null && mostRecentLMDbyRecordType.Status__c != null) 
				{
					// set our RPS status to be the status of the most recent LMD
					toupdateRPS.Status__c = mostRecentLMDbyRecordType.Status__c;
				}
			}

			// if the most recent LMD 'Record Type' is 'Rotation'
			if(mostRecentLMD.RecordType.Name == 'Rotation') 
			{
				if(mostRecentLMD.Winch__c != null) {
					toupdateRPS.Current_Postion__c = mostRecentLMD.Winch__c;
				}
				if(mostRecentLMD.Equipment_Detail__c != null){
					toupdateRPS.Equipment_Detail__c = mostRecentLMD.Equipment_Detail__c;
				}
			}
			// if the most recent LMD 'Record Type' isn't 'Rotation'
			else 
			{
				// get our most recent LMD with the given RPS id with 'Record Type' == 'Rotation'
				Line_Maintenance_Detail__c mostRecentLMDbyRecordType = 
					LMDtoRPSServiceClass.returnMostRecentLMDbyRecordType(rpsLMDListMap.get(individualRPSId),'Rotation');

				if (mostRecentLMDbyRecordType != null) 
				{
					if(mostRecentLMDbyRecordType.Winch__c != null) 
					{
						toupdateRPS.Current_Postion__c = mostRecentLMDbyRecordType.Winch__c;
					}
					if(mostRecentLMDbyRecordType.Equipment_Detail__c != null) 
					{
						toupdateRPS.Equipment_Detail__c = mostRecentLMDbyRecordType.Equipment_Detail__c;
					}
				}
			}

			// if our most Recent LMD 'Record Type' is 'EFE' and 'Outboard_End_of_the_Line_in_Use_A_or_B__c' isn't empty
			if (mostRecentLMD.RecordType.Name == 'EFE' && String.isNotEmpty(mostRecentLMD.Outboard_End_of_the_Line_in_Use_A_or_B__c)) 
			{
				toupdateRPS.Current_End_in_Use__c =  mostRecentLMD.Outboard_End_of_the_Line_in_Use_A_or_B__c;
			} 
			else 
			{
				if(LMDtoRPSServiceClass.returnMostRecentLMDbyRecordType(rpsLMDListMap.get(individualRPSId),'EFE') != null && LMDtoRPSServiceClass.returnMostRecentLMDbyRecordType(rpsLMDListMap.get(individualRPSId),'EFE').Outboard_End_of_the_Line_in_Use_A_or_B__c != null ) {
					toupdateRPS.Current_End_in_Use__c = LMDtoRPSServiceClass.returnMostRecentLMDbyRecordType(rpsLMDListMap.get(individualRPSId),'EFE').Outboard_End_of_the_Line_in_Use_A_or_B__c;
				}
			}

			// if our most Recent LMD 'Record Type' is 'Inspection'
			if (mostRecentLMD.RecordType.Name == 'Inspection')
			{
				toupdateRPS.Last_Inspection__c = mostRecentLMD.Id;
			}
			else 
			{
				if (LMDtoRPSServiceClass.returnMostRecentInspectionLMD(rpsLMDListMap.get(individualRPSId)) != null){
					toupdateRPS.Last_Inspection__c = LMDtoRPSServiceClass.returnMostRecentInspectionLMD(rpsLMDListMap.get(individualRPSId)).Id;
				}
			}

			// if our most Recent LMD 'Record Type' is 'Inspection'
			if (mostRecentLMD.RecordType.Name != 'Inspection')
			{
				toupdateRPS.Last_Maintenance_Event__c = mostRecentLMD.Id;
			}
			else 
			{
				if (LMDtoRPSServiceClass.returnMostRecentNonInspectionLMD(rpsLMDListMap.get(individualRPSId)) != null ){
					toupdateRPS.Last_Maintenance_Event__c = LMDtoRPSServiceClass.returnMostRecentNonInspectionLMD(rpsLMDListMap.get(individualRPSId)).Id;
				}
			}

			if (mapRPS.get(individualRPSId).Current_Length_meters__c != null && mapRPS.get(individualRPSId).Current_Length_meters__c != 0)
			{
				toupdateRPS.Current_Length_meters__c = mapRPS.get(individualRPSId).Current_Length_meters__c - LMDtoRPSServiceClass.returnTotalCroppedLength(rpsLMDListMap.get(individualRPSId));
			}
			
			if(mapRPS.get(individualRPSId).Accumulated_Working_Hours__c != null ) {
				toupdateRPS.Accumulated_Working_Hours__c = mapRPS.get(individualRPSId).Accumulated_Working_Hours__c + LMDtoRPSServiceClass.returnTotalWorkedHours(rpsLMDListMap.get(individualRPSId));
			}
			
			toupdateRPSList.add(toupdateRPS);
		}
		try{
			update toupdateRPSList;
			update toupdateLMDList;
		} catch(DmlException e) {
			throw e;
		}
	}
}