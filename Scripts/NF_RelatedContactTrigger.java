/*
** Trigger:  NF_RelatedContactTrigger
** Created by Neuraflash
** Description: Trigger for Sharing records.
*/
trigger NF_RelatedContactTrigger on Related_Contacts__c (after insert, after update, before delete)
{
    // check to make sure the trigger is on
    // dependant on 'NF_RelatedContactTrigger' 'Custom Label' and 
    // 'TriggerSetting' 'Custom Metadata Types'
    if(NF_RelatedContactBatchUtilityClass.validateTriggerActivation(Label.NF_RelatedContactTrigger))
    {
        if (Trigger.isAfter)
        {
            if (Trigger.isInsert)
            {
                NF_RelatedContactBatchUtilityClass.validateBatchCall(Trigger.new, null, 'After Insert');
            }
            if (Trigger.isUpdate)
            {
                NF_RelatedContactBatchUtilityClass.validateBatchCall(Trigger.new, Trigger.oldMap, 'After Update');
            }
        }
        if (Trigger.isBefore)
        {
            if (Trigger.isDelete)
            {
                NF_RelatedContactBatchUtilityClass.validateBatchCall(null, Trigger.oldMap, 'Before Delete');
            }
        }
    }
}