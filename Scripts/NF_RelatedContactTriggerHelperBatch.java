/*
** Class:  NF_RelatedContactTriggerHelperBatch
** Created by Neuraflash
** Description: Batch class for bulk processing of share records.
*/
global class NF_RelatedContactTriggerHelperBatch implements Database.Batchable<SObject>,Database.Stateful {
    //Variable Definition
    global final List<Id> listofRCrecords = new List<Id>();
    global final String TriggerEvent;
    global final Map<Id, Related_Contacts__c > oldRecordsMap;
    global final String query;
    global map<id,list<id>> childInvoiceShareData = new map<id,list<id>>();
    global map<id,list<id>> childRemoveInvoiceShareData = new map<id,list<id>>();
    Set<Id> setofFleetID = new Set<Id>();
    Set<Id> setofAssetID = new Set<Id>();
    Set<Id> setofAllAssetID = new Set<Id>();
    List<Related_Contacts__c> fleetSharingRecordlist = new List<Related_Contacts__c>();
    List<Related_Contacts__c> assetSharingRecordlist = new List<Related_Contacts__c>();
    List<Related_Contacts__c> invoiceSharingRecordlist = new List<Related_Contacts__c>();
    List<Related_Contacts__c> removeFleetRecordList = new List<Related_Contacts__c>();
    List<Related_Contacts__c> removeAssetRecordList = new List<Related_Contacts__c>();
    List<Related_Contacts__c> removeInvoiceSharingRecordlist = new List<Related_Contacts__c>();
    List<SObject> alldeleteShareData = new List<SObject>();
    List<SObject> allShareData = new List<SObject>();
    Map<Id, Id> contactToUserMap = new Map<Id, Id>();
    public String batchID;
    String DMLstatus;
    Boolean errorLogCheck;
    
    //Constructors
    global NF_RelatedContactTriggerHelperBatch(List<Id> listRelatedContacts, String TriggerEvent,String query) {
        this.listofRCrecords.addAll(listRelatedContacts);
        this.TriggerEvent = TriggerEvent;
        this.query = query;
        init();
    }
    global NF_RelatedContactTriggerHelperBatch(List<Id> listRelatedContacts, Map<Id, Related_Contacts__c > oldRecordsMap, String TriggerEvent,String query) {
        this.listofRCrecords.addAll(listRelatedContacts);
        this.oldRecordsMap = oldRecordsMap;
        this.TriggerEvent = TriggerEvent;
        this.query = query;
        init();
    }
    global NF_RelatedContactTriggerHelperBatch(Map<Id, Related_Contacts__c > oldRecordsMap, String TriggerEvent,String query) {
        this.listofRCrecords.addAll(oldRecordsMap.keySet());
        this.oldRecordsMap = oldRecordsMap;
        this.TriggerEvent = TriggerEvent;
        this.query = query;
        init();
    }
    public void init(){
        if(NF_RelatedContactBatchUtilityClass.ValidateErrorLogCheck()){
            errorLogCheck = true;
        }
        else{
            errorLogCheck = false;
        }
    }
    
    // Start Batch Execution
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Related_Contacts__c> scope) {
        batchID = BC.getJobId();
        try{
            switch on TriggerEvent{
                when 'After Insert'{
                    DMLStatus = createShareRecords(scope);
                }
                when 'After Update'{
                    DMLStatus = updateShareRecords(scope, oldRecordsMap);
                }
            }
            System.debug(DMLStatus);
        }
        catch(Exception ex){
            if(errorLogCheck){
                NF_ExceptionEventHandler errorHandler = new NF_ExceptionEventHandler(ex,batchID);
            }
        }
    }
    
    global void finish(Database.BatchableContext BC) {
        if(childInvoiceShareData.size() > 0){
            if(NF_RelatedContactBatchUtilityClass.validateTriggerActivation('NF_InvoiceSharingBatch')){
                NF_InvoiceSharingBatch insertBatch = new NF_InvoiceSharingBatch(childInvoiceShareData);
                Database.executeBatch(insertBatch,100);
            }
        }
        if(childRemoveInvoiceShareData.size() > 0){
            if(NF_RelatedContactBatchUtilityClass.validateTriggerActivation('NF_RemoveInvoiceSharingBatch')){
                NF_RemoveInvoiceSharingBatch deleteBatch = new NF_RemoveInvoiceSharingBatch(childRemoveInvoiceShareData);
                Database.executeBatch(deleteBatch,100);
            }
        }
    }   
    /*
    Method Name: createShareRecords
    Param: List of Related_Contacts__c
    Description: This method will be called on After Insert event on Related Contact Trigger
    */
    
    private String createShareRecords(List<Related_Contacts__c > scope) {
        
        contactToUserMap = NF_RelatedContactBatchUtilityClass.UtilcreateContactToUserMap(scope); //get all users belonging to contact
        /*
    Only trigger Sharing record creation if the contact is a community user's contact
    */
        
        if (contactToUserMap.size() > 0) {
            for (Related_Contacts__c record : scope) { //for starts
                if (NF_RelatedContactBatchUtilityClass.validateRelatedContactRole(record)) {
                    setofAssetID.add(record.Asset__c); //get all Asset ID to get accounts and invoices
                    invoiceSharingRecordlist.add(record);
                }
                // check if share all asset data is true
                if (NF_RelatedContactBatchUtilityClass.validateShareAllCheckbox(record)) {
                    fleetSharingRecordlist.add(record);
                    setofFleetID.add(record.Fleet__c);
                } else {
                    //share record at access level
                    setofAllAssetID.add(record.Asset__c);
                    assetSharingRecordlist.add(record);
                }
            } // for ends
        }// if ends
        
        /*
        Invoice Record sharing starts here
        */
        if(!invoiceSharingRecordlist.isEmpty()){
            Map<Id, List<Id>> mapofAssetUsers = NF_RelatedContactBatchUtilityClass.utilcreateShareMapMethod(invoiceSharingRecordlist, Label.NF_AssetAPI , contactToUserMap);
            childInvoiceShareData = NF_RelatedContactBatchUtilityClass.UtilAssetwithChildInvoice(setofAssetID,invoiceSharingRecordlist,mapofAssetUsers);
        }
        
        /*
        Invoice Record sharing ends here
        */
        
        /*
        Fleet Level Record sharing Starts here
        */
        if (!fleetSharingRecordlist.isEmpty()) { //share records at asset level
            //generate sharing record for Fleet__Share object
            Map<Id, List<Id>> sobjectUserMap = NF_RelatedContactBatchUtilityClass.utilcreateShareMapMethod(fleetSharingRecordlist, Label.NF_FleetAPI , contactToUserMap);
            Map<Id, List<Id>> childShareRecord = NF_RelatedContactBatchUtilityClass.UtilFleetwithChildAsset(setofFleetID,fleetSharingRecordlist,sobjectUserMap);
            Map<Id, List<Id>> childWOShareRecord = NF_RelatedContactBatchUtilityClass.UtilFleetwithChildWorkOrder();
            if (childWOShareRecord.size() > 0) {
                allShareData.addAll(NF_RelatedContactBatchUtilityClass.shareRecordInsertwithAccess(childWOShareRecord,'Read')); //insert new WorkOrder (child of Asset which is child of fleet) share records
            }
            if (sobjectUserMap.size() > 0) {
                allShareData.addAll(NF_RelatedContactBatchUtilityClass.shareRecordInsertwithAccess(sobjectUserMap,'Read')); //insert new fleet share records
            }
            if (childShareRecord.size() > 0) {
                allShareData.addAll(NF_RelatedContactBatchUtilityClass.shareRecordInsertwithAccess(childShareRecord,'Edit')); //insert new Asset (child of fleet) share records
            }
        }
        /*
        Fleet Level Record sharing ends here
        */
        /*
        Asset Level Record sharing Starts here
        */
        if (!assetSharingRecordlist.isEmpty()) { //share records at asset level
            //generate sharing record for Fleet__Share object
            Map<Id, List<Id>> sobjectUserMap = NF_RelatedContactBatchUtilityClass.utilcreateShareMapMethod(assetSharingRecordlist, Label.NF_AssetAPI , contactToUserMap);
            Map<Id, List<Id>> childShareRecord = NF_RelatedContactBatchUtilityClass.UtilAssetwithChildWorkOrder(setofAllAssetID,assetSharingRecordlist,sobjectUserMap);
            
            if (sobjectUserMap.size() > 0) {
                allShareData.addAll(NF_RelatedContactBatchUtilityClass.shareRecordInsertwithAccess(sobjectUserMap,'Edit')); //insert new Asset share records
            }
            if (childShareRecord.size() > 0) {
                allShareData.addAll(NF_RelatedContactBatchUtilityClass.shareRecordInsertwithAccess(childShareRecord,'Read')); //insert new Work Order (child of Asset) share records
            }
            
        }
        /*
        Asset Level Record sharing ends here
        */
        if (!allShareData.isEmpty()) {
            allShareData.sort();
            Database.SaveResult[] insertedShareResult = Database.insert(allShareData, false);
            List<Database.Error> listofErrors = new List<Database.Error>();
            for (Database.SaveResult sr : insertedShareResult) {
                if (!sr.isSuccess()) {
                    if(errorLogCheck){
                        NF_ExceptionEventHandler errorHandler = new NF_ExceptionEventHandler(sr.getErrors(),sr.getId(),batchID);
                    }
                    DMLstatus = Label.NF_RecordInsertFail ; //only one error with record ids
                } else {
                    DMLstatus = Label.NF_RecordInsertSuccess ;
                }
            }
        }
        
        return DMLstatus;
    }
    
    /*
    Method Name: updateShareRecords
    Param: Map of Trigger.New and Map of Trigger.old
    Description: This method will be called on After update event on Related Contact Trigger
    */
    
    private String updateShareRecords(List<Related_Contacts__c > newRecordsList, Map<Id, Related_Contacts__c > oldRecordsMap) {
        
        // variable decalartion
        Set<Id> setofOldFleetID = new Set<Id>();
        Set<Id> setofOldAssetID = new Set<Id>();
        
        contactToUserMap = NF_RelatedContactBatchUtilityClass.UtilcreateContactToUserMap(newRecordsList); //get all community users belonging to contact
        /*
        Only Sharing record creation, if the contact is a community user's contact
        */
        if (contactToUserMap.size() > 0) {
            for (Related_Contacts__c record : newRecordsList) { //for starts
                
                if (oldRecordsMap.get(record.Id).Role__c != record.Role__c){
                    if(!NF_RelatedContactBatchUtilityClass.validateRelatedContactRole(oldRecordsMap.get(record.Id))){
                        if(NF_RelatedContactBatchUtilityClass.validateRelatedContactRole(record)){
                            invoiceSharingRecordlist.add(record);
                            setofAssetID.add(record.Asset__c);
                        }
                    }
                    else{
                        if(!NF_RelatedContactBatchUtilityClass.validateRelatedContactRole(record)){
                            removeInvoiceSharingRecordlist.add(record);
                            setofAssetID.add(record.Asset__c);
                        }
                    }
                }
                // check if share all asset data is true
                if (NF_RelatedContactBatchUtilityClass.validateShareAllCheckbox(record)) { //Share all data is True in Trigger.new
                    if(!NF_RelatedContactBatchUtilityClass.validateShareAllCheckbox(oldRecordsMap.get(record.Id))){ //Share all data was false in Trigger.oldmap
                        setofOldAssetID.add(oldRecordsMap.get(record.Id).Asset__c); // this set id prepared to get child records for deletion of share record against them
                        removeAssetRecordList.add(oldRecordsMap.get(record.Id)); //remove older share records at asset level
                        setofFleetID.add(record.Fleet__c); // this set id prepared to get child records of fleets
                        fleetSharingRecordlist.add(record); //creating list of all fleet level record for sharing
                    }
                    else if (oldRecordsMap.get(record.Id).Fleet__c != record.Fleet__c) {
                        fleetSharingRecordlist.add(record); //creating list of all fleet level record for sharing
                        setofFleetID.add(record.Fleet__c); // this set id prepared to get child records of fleets
                        removeFleetRecordList.add(oldRecordsMap.get(record.Id)); //remove older share records
                        setofOldFleetID.add(oldRecordsMap.get(record.Id).Fleet__c); // this set id prepared to get child records for deletion of share record against them
                    }
                } //if ends
                else {
                    if(NF_RelatedContactBatchUtilityClass.validateShareAllCheckbox(oldRecordsMap.get(record.Id))){ //Share all data was True in Trigger.oldmap
                        setofOldFleetID.add(oldRecordsMap.get(record.Id).Fleet__c); // this set id prepared to get child records for deletion of share record against them
                        removeFleetRecordList.add(oldRecordsMap.get(record.Id)); //remove older share records at fleet level
                        setofAllAssetID.add(record.Asset__c); // this set id prepared to get child records of assets
                        assetSharingRecordlist.add(record); //creating list of all asset level record for sharing
                    }
                    else if (oldRecordsMap.get(record.Id).Asset__c != record.Asset__c) {
                        setofAllAssetID.add(record.Asset__c); // this set id prepared to get child records of assets
                        assetSharingRecordlist.add(record); //creating list of all asset level record for sharing
                        removeAssetRecordList.add(oldRecordsMap.get(record.Id)); //remove older shared records
                        setofOldAssetID.add(oldRecordsMap.get(record.Id).Asset__c); // this set id prepared to get child records for deletion of share record against them
                    }
                }//else ends
            }//for ends
        }//if ends
        
        if (fleetSharingRecordlist.size() > 0) { // fleet record to share with contacts on related contacts
            //generate sharing record for Fleet__Share object
            Map<Id, List<Id>> sobjectUserMap = NF_RelatedContactBatchUtilityClass.utilcreateShareMapMethod(fleetSharingRecordlist, Label.NF_FleetAPI , contactToUserMap);
            Map<Id, List<Id>> childShareRecord = NF_RelatedContactBatchUtilityClass.UtilFleetwithChildAsset(setofFleetID,fleetSharingRecordlist,sobjectUserMap);
            Map<Id, List<Id>> childWOShareRecord = NF_RelatedContactBatchUtilityClass.UtilFleetwithChildWorkOrder();
            if (childWOShareRecord.size() > 0) {
                allShareData.addAll(NF_RelatedContactBatchUtilityClass.shareRecordInsertwithAccess(childWOShareRecord,'Read')); //insert new WorkOrder (child of Asset which is child of fleet) share records
            }
            if (sobjectUserMap.size() > 0) {
                allShareData.addAll(NF_RelatedContactBatchUtilityClass.shareRecordInsertwithAccess(sobjectUserMap,'Read')); //insert new fleet share records
            }
            if (childShareRecord.size() > 0) {
                allShareData.addAll(NF_RelatedContactBatchUtilityClass.shareRecordInsertwithAccess(childShareRecord,'Edit')); //insert new Asset (child of fleet) share records
            }
        }
        if (assetSharingRecordlist.size() > 0) { // Asset record to share with contacts on related contacts
            //generate sharing record for Asset__Share object
            Map<Id, List<Id>> sobjectUserMap = NF_RelatedContactBatchUtilityClass.utilcreateShareMapMethod(assetSharingRecordlist, Label.NF_AssetAPI , contactToUserMap);
            Map<Id, List<Id>> childShareRecord = NF_RelatedContactBatchUtilityClass.UtilAssetwithChildWorkOrder(setofAllAssetID,assetSharingRecordlist,sobjectUserMap);
            
            if (sobjectUserMap.size() > 0) {
                allShareData.addAll(NF_RelatedContactBatchUtilityClass.shareRecordInsertwithAccess(sobjectUserMap,'Edit')); //insert new fleet share records
            }
            if (childShareRecord.size() > 0) {
                allShareData.addAll(NF_RelatedContactBatchUtilityClass.shareRecordInsertwithAccess(childShareRecord,'Read')); //insert new Work Order (child of Asset) share records
            }
        }
        if (removeFleetRecordList.size() > 0) {
            // creating Map for removing sharing records against a fleet
            Map<Id, List<Id>> sobjectUserMap = NF_RelatedContactBatchUtilityClass.utilcreateShareMapMethod(removeFleetRecordList, Label.NF_FleetAPI , contactToUserMap);
            Map<Id, List<Id>> childShareRecord = NF_RelatedContactBatchUtilityClass.UtilFleetwithChildAsset(setofOldFleetID,removeFleetRecordList,sobjectUserMap); //child assets of fleet
            Map<Id, List<Id>> childWOShareRecord = NF_RelatedContactBatchUtilityClass.UtilFleetwithChildWorkOrder();
            if (childWOShareRecord.size() > 0) {
                allShareData.addAll(NF_RelatedContactBatchUtilityClass.shareRecordDelete(childWOShareRecord)); //insert new WorkOrder (child of Asset which is child of fleet) share records
            }
            if (sobjectUserMap.size() > 0) {
                alldeleteShareData.addAll(NF_RelatedContactBatchUtilityClass.shareRecordDelete(sobjectUserMap)); //delete old fleet share records
            }
            if (childShareRecord.size() > 0) {
                alldeleteShareData.addAll(NF_RelatedContactBatchUtilityClass.shareRecordDelete(childShareRecord)); //delete old Asset (child of fleet) share records
            }
        }
        if (removeAssetRecordList.size() > 0) {
            // creating Map for removing sharing records agains a fleet.
            Map<Id, List<Id>> sobjectUserMap = NF_RelatedContactBatchUtilityClass.utilcreateShareMapMethod(removeAssetRecordList, Label.NF_AssetAPI , contactToUserMap);
            Map<Id, List<Id>> childShareRecord = NF_RelatedContactBatchUtilityClass.UtilAssetwithChildWorkOrder(setofOldAssetID,removeAssetRecordList,sobjectUserMap);
            
            if (sobjectUserMap.size() > 0) {
                alldeleteShareData.addAll(NF_RelatedContactBatchUtilityClass.shareRecordDelete(sobjectUserMap)); //delete old asset share records
            }
            if (childShareRecord.size() > 0) {
                alldeleteShareData.addAll(NF_RelatedContactBatchUtilityClass.shareRecordDelete(childShareRecord)); //delete old Work Orders (child of Asset) share records
            }
        }
        /*
        Invoice Record sharing starts here
        */
        if(!invoiceSharingRecordlist.isEmpty()){
            Map<Id, List<Id>> mapofAssetUsers = NF_RelatedContactBatchUtilityClass.utilcreateShareMapMethod(invoiceSharingRecordlist, Label.NF_AssetAPI , contactToUserMap);
            childInvoiceShareData = NF_RelatedContactBatchUtilityClass.UtilAssetwithChildInvoice(setofAssetID,invoiceSharingRecordlist,mapofAssetUsers);

        }
        if(removeInvoiceSharingRecordlist.size() > 0){
            // creating Map for removing Invoice sharing records
            Map<Id, List<Id>> mapofAssetUsers = NF_RelatedContactBatchUtilityClass.utilcreateShareMapMethod(removeInvoiceSharingRecordlist, Label.NF_AssetAPI , contactToUserMap);
            childRemoveInvoiceShareData = NF_RelatedContactBatchUtilityClass.UtilAssetwithChildInvoice(setofAssetID,removeInvoiceSharingRecordlist,mapofAssetUsers);
        }
        /*
        Invoice Record sharing ends here
        */
        /*
        Sharing Record Deletion Starts here
        */
        if (!alldeleteShareData.isEmpty()) {
            alldeleteShareData.sort();
            Database.DeleteResult[] deletedShareResult = Database.delete(alldeleteShareData, false);
            for (Database.DeleteResult sr : deletedShareResult) {
                if (!sr.isSuccess()) {
                    if(errorLogCheck){
                        NF_ExceptionEventHandler errorHandler = new NF_ExceptionEventHandler(sr.getErrors(),sr.getId(),batchID);
                    }
                } else {
                    DMLstatus = Label.NF_RecordDeleteSuccess ;
                }
            }
        }
        /*
        Sharing Record Deletion ends here
        */
        /*
        Sharing Record Insertion Starts here
        */
        if (!allShareData.isEmpty()) {
            allShareData.sort();
            Database.SaveResult[] insertedShareResult = Database.insert(allShareData, false);
            for (Database.SaveResult sr : insertedShareResult) {
                if (!sr.isSuccess()) {
                    if(errorLogCheck){
                        NF_ExceptionEventHandler errorHandler = new NF_ExceptionEventHandler(sr.getErrors(),sr.getId(),batchID);
                    }
                    DMLstatus = Label.NF_RecordInsertFail + sr.getErrors();
                } else {
                    DMLstatus = Label.NF_RecordInsertSuccess;
                }
            }
        }
        /*
        Sharing Record Insertion ends here
        */
        
        return DMLstatus;
        
        
    }
    /*
    Method Name: deleteTriggerShareRecords
    Param: Map of Related_Contacts__c from Trigger.old
    Description: This method will be called on After Insert event on Related Contact Trigger
    */
    public String deleteTriggerShareRecords(Map<Id, Related_Contacts__c > oldRecordsMap) {
        
        contactToUserMap = NF_RelatedContactBatchUtilityClass.UtilcreateContactToUserMap(oldRecordsMap.values()); //get all users belonging to contact
        
        /*
        Only trigger Sharing record deletion if the contact is a community user's contact
        */
        if (contactToUserMap.size() > 0) {
            for (Related_Contacts__c record : oldRecordsMap.values()) { //for starts
                if (NF_RelatedContactBatchUtilityClass.validateRelatedContactRole(record)) {
                    setofAssetID.add(record.Asset__c); //get all fleet ID to get accounts and invoices
                    removeInvoiceSharingRecordlist.add(record);
                }
                // check if share all asset data is true
                if (NF_RelatedContactBatchUtilityClass.validateShareAllCheckbox(record)) {
                    removeFleetRecordList.add(record);
                    setofFleetID.add(record.Fleet__c);
                } //if ends
                else {
                    removeAssetRecordList.add(record);
                }
            }//else ends
        }
        if (removeFleetRecordList.size() > 0) {
            // creating Map for removing sharing records against a fleet level
            Map<Id, List<Id>> sobjectUserMap = NF_RelatedContactBatchUtilityClass.utilcreateShareMapMethod(removeFleetRecordList, Label.NF_FleetAPI , contactToUserMap);
            Map<Id, List<Id>> childShareRecord = NF_RelatedContactBatchUtilityClass.UtilFleetwithChildAsset(setofFleetID,removeFleetRecordList,sobjectUserMap); //child assets of fleet
            Map<Id, List<Id>> childWOShareRecord = NF_RelatedContactBatchUtilityClass.UtilFleetwithChildWorkOrder();
            if (childWOShareRecord.size() > 0) {
                allShareData.addAll(NF_RelatedContactBatchUtilityClass.shareRecordDelete(childWOShareRecord)); //insert new WorkOrder (child of Asset which is child of fleet) share records
            }
            if (sobjectUserMap.size() > 0) {
                alldeleteShareData.addAll(NF_RelatedContactBatchUtilityClass.shareRecordDelete(sobjectUserMap)); //delete old fleet share records
            }
            if (childShareRecord.size() > 0) {
                alldeleteShareData.addAll(NF_RelatedContactBatchUtilityClass.shareRecordDelete(childShareRecord)); //delete old Asset (child of fleet) share records
            }
        }
        if (removeAssetRecordList.size() > 0) {
            // creating Map for removing sharing records against an Asset.
            Map<Id, List<Id>> sobjectUserMap = NF_RelatedContactBatchUtilityClass.utilcreateShareMapMethod(removeAssetRecordList, Label.NF_AssetAPI , contactToUserMap);
            Map<Id, List<Id>> childShareRecord = NF_RelatedContactBatchUtilityClass.UtilAssetwithChildWorkOrder(setofAllAssetID,removeAssetRecordList,sobjectUserMap);
            
            if (sobjectUserMap.size() > 0) {
                alldeleteShareData.addAll(NF_RelatedContactBatchUtilityClass.shareRecordDelete(sobjectUserMap)); // delete old asset share records
            }
            if (childShareRecord.size() > 0) {
                alldeleteShareData.addAll(NF_RelatedContactBatchUtilityClass.shareRecordDelete(childShareRecord)); //delete old Work Orders (child of Asset) share records
            }
            
        }
        if(removeInvoiceSharingRecordlist.size() > 0){
            // creating Map for removing Invoice sharing records
            Map<Id, List<Id>> mapofAssetUsers = NF_RelatedContactBatchUtilityClass.utilcreateShareMapMethod(removeInvoiceSharingRecordlist, Label.NF_AssetAPI , contactToUserMap);
            childRemoveInvoiceShareData = NF_RelatedContactBatchUtilityClass.UtilAssetwithChildInvoice(setofAssetID,removeInvoiceSharingRecordlist,mapofAssetUsers);
            if(childRemoveInvoiceShareData.size() > 0){
                if(NF_RelatedContactBatchUtilityClass.validateTriggerActivation('NF_RemoveInvoiceSharingBatch')){
                    NF_RemoveInvoiceSharingBatch deleteBatch = new NF_RemoveInvoiceSharingBatch(childRemoveInvoiceShareData);
                    Database.executeBatch(deleteBatch,100);
                }
            }
        }
        if (!alldeleteShareData.isEmpty()) {
            alldeleteShareData.sort();
            Database.DeleteResult[] deletedShareResult = Database.delete(alldeleteShareData, false);
            for (Database.DeleteResult sr : deletedShareResult) {
                if (!sr.isSuccess()) {
                    if(errorLogCheck){
                        NF_ExceptionEventHandler errorHandler = new NF_ExceptionEventHandler(sr.getErrors(),sr.getId(),batchID);
                    }
                    DMLstatus = Label.NF_RecordDeleteFail;
                } else {
                    DMLstatus = Label.NF_RecordDeleteSuccess ;
                }
            }
        }
        return DMLstatus;
        
    }   
}