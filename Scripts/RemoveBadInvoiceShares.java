

Map<Id, User> users = new Map<Id, User>([Select Id, AccountId from User]);
Map<Id, Group> groups = new Map<Id, Group>([Select Id, RelatedId from Group]);
Map<Id, UserRole> roles = new Map<Id, UserRole>([select id, PortalAccountId  from UserRole]);

List<Invoice__Share> shares = [select id, Parent.Name, Parent.Account__c, Parent.Account__r.ParentId, UserOrGroupId, RowCause from Invoice__Share 
                               Where LastModifiedDate = LAST_N_DAYS:40 AND RowCause = 'Manual'];

Integer count = 0;
Set<Id> shareIds = new Set<Id>();
for (Invoice__Share share : shares) {
    User user = users.get(share.UserOrGroupId);
    
    if (user != null) {
        If (user.AccountId != null && share.parent.Account__c != user.AccountId && share.Parent.Account__r.ParentId != user.AccountId) {
            System.debug('share.parent.Account__c: ' + share.parent.Account__c + ' user.AccountId: ' + user.AccountId + ' share.Parent.Account__r.ParentId: ' + share.Parent.Account__r.ParentId);
            count++;
            System.debug('Invoice: ' +  share.Parent.Name);
            System.debug('Share ID: ' +  share.Id);
            shareIds.add(share.Id);
            System.debug('User Id: ' +  user.id);
        }
    } else {
        Group g = groups.get(share.UserOrGroupId);
        if (g != null) {
            UserRole role = roles.get(g.RelatedId); 
            If (role != null) {
                If (role.PortalAccountId  != null &&
                    share.parent.Account__c !=role.PortalAccountId  && 
                    share.Parent.Account__r.ParentId != role.PortalAccountId ) {
                    //count++;
                    System.debug('Invoice: ' +  share.Parent.Name);
                    System.debug('Share ID: ' +  share.Id);
                    shareIds.add(share.Id);
                }
            }
        }
    }
}

List<Invoice__Share> sharesToDel = [Select id from Invoice__Share Where Id in : shareIds];

//delete sharesToDel;

System.debug('count: ' + sharesToDel.Size());

System.debug('count: ' +  count);



