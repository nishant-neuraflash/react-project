public class Connect_ReportsController {
    public class Report {
        public String Id {
            get { return Id; }
            set { Id = value; }
        }
        
        public String WorkOrderId {
            get { return WorkOrderId; }
            set { WorkOrderId = value; }
        }
        
        public String AssetId {
            get { return AssetId; }
            set { AssetId = value; }
        }
        
        public String AssetName {
            get { return AssetName; }
            set { AssetName = value; }
        }
        
        public String Entity {
            get { return Entity; }
            set { Entity = value; }
        }

        public DateTime EventDate {
            get { return EventDate; }
            set { EventDate = value; }
        }
        
        public String LocationOfService {
            get { return LocationOfService; }
            set { LocationOfService = value; }
        }
        
        public String WorkTypeName {
            get { return WorkTypeName; }
            set { WorkTypeName = value; }
        }
    }
    
    private static Id contactId;
    private static Id accountId;
    private static List<Id> assetIds;
    
    static {
        if (UserInfo.getUserType() == 'Standard') {
            contactId = [SELECT Id FROM Contact WHERE Email = :UserInfo.getUserEmail()].Id;
        }
        else {
            contactId = [SELECT ContactId FROM User WHERE Id = :UserInfo.getUserId()].ContactId;
        }
        accountId = [SELECT AccountId FROM Contact WHERE Id = :contactId].AccountId;
        assetIds = new List<Id>();
        for (Related_Contacts__c rc : [SELECT Asset__c FROM Related_Contacts__c WHERE Asset__c != '' AND Contact__c = :contactId]) {
            assetIds.add(rc.Asset__c);
        }
    }

    /* 
     * return the reports attached to the work orders
     */ 
    @RemoteAction
    public static List<Report> getReports() {
        // query content document links
        List<ContentDocumentLink> cdls = [SELECT ContentDocumentId, LinkedEntityId, ContentDocument.FileExtension 
                                          FROM ContentDocumentLink 
                                          WHERE LinkedEntityId IN (
                                              SELECT Id FROM WorkOrder 
                                              WHERE 
                                              AccountId = :accountId AND 
                                              RecordType.Name != 'Service Onboarding' AND 
                                              RecordType.Name != 'Internal Samson Support' AND 
                                              Asset.Id IN :assetIds)
                                         ];
        // query work orders
        List<WorkOrder> workOrders = [SELECT Id, Asset.Id, Asset.Name, WorkOrderNumber, Inspection_Date__c, WorkType.Name, Location_of_Service__c
                                      FROM WorkOrder WHERE AccountId = :accountId AND Asset.Id IN :assetIds];
        Map<Id, WorkOrder> workOrdersMap = new Map<Id, WorkOrder>(workOrders);
        List<Report> reports = new List<Report>();
        
        for (ContentDocumentLink cdl : cdls) {
            if (cdl.ContentDocument.FileExtension != null && cdl.ContentDocument.FileExtension.toUpperCase() == 'PDF') {
                Report report = new Report();
                report.Id = cdl.ContentDocumentId;
                report.EventDate = workOrdersMap.get(cdl.LinkedEntityId).Inspection_Date__c;
                report.AssetId = workOrdersMap.get(cdl.LinkedEntityId).Asset.Id;
                report.WorkOrderId = workOrdersMap.get(cdl.LinkedEntityId).Id;
                report.AssetName = workOrdersMap.get(cdl.LinkedEntityId).Asset.Name;
                report.WorkTypeName = workOrdersMap.get(cdl.LinkedEntityId).WorkType.Name;
                report.Entity = workOrdersMap.get(cdl.LinkedEntityId).WorkOrderNumber;
                report.LocationOfService = workOrdersMap.get(cdl.LinkedEntityId).Location_of_Service__c;
                
                reports.add(report);
            }
        }
        return reports;
    }
}