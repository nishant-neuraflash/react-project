
// these lmds are sorted by event date from the oldest to the most recent
lmdBatch = list of all unprocessed lmds 

for each lmd in lmdBatch {

    set rps = lmd.rps // this rps could be a mainline or a tail for all types except for asset activity
    set tailRps = lmd.tailRps // this is only used for asset activity, when we have a tail rps here it is assumed that the above rps is a mainline
    set eqp = lmd.eqp // equipment detail tied to the lmd

    // start of 'status update'
    if lmd.recordType = 'Status Update' {
        if (lmd.eventDate > rps.lastMaintenanceEventLmd.eventDate) {
            set rps.lastMaintenanceEventLmd = lmd
        }
        
        if (lmd.status is not blank) {
            set rps.status = lmd.status

            // start of installed case
            if (lmd.status = 'Installed'){

                if (lmd.equipmentDetail is not blank) {
                    set rps.equipmentDetail = lmd.equipmentDetail
                } else {
                    set lmd.supportStatus = 'under review'
                    set lmd.fieldServiceNotes + ' ; ' + 'some notes'
                }
  
                if (rps.installDate is blank) {
                    set rps.installDate = lmd.eventDate
                }

                if (lmd.endInUse is not blank) {
                    rps.endInUse = lmd.endInUse
                } else {
                    set lmd.supportStatus = 'under review'
                    set lmd.fieldServiceNotes + ' ; ' + 'some notes'
                }
            }  // end of installed case
            
            // start of 'spare' or 'retired' case
            else if (lmd.status = 'spare' or 'retired') {
                set rps.equipmentDetail = blank
            } // end of 'spare' or 'retired' case           
        }

    }
    // end of status update

    // start of 'rotation' case
    if lmd.recordType = 'rotation' {
        if (rps.lastMaintenanceEventLmd == null || lmd.eventDate > rps.lastMaintenanceEventLmd.eventDate) {
            set rps.lastMaintenanceEventLmd = lmd
        }

        // case in which we are processing rotations out of sync
        if (rps.equipmentDetail == null || (rps.equipmentDetail.Name not equal lmd.fromWinch)) {
            set lmd.supportStatus = 'under review'
            set rps.dispostion = 'under review'

            // get the final note from the team
            set lmd.fieldServiceNotes = lmd.fieldServiceNotes + ' ; ' + 'rotation from different equipment than indicated on rps'

            // keep track of flag for error catching (talk to James)
        }

        if (lmd.eventDate > rps.lastRotationDate) {
            set rps.equipmentDetail = lmd.equipmentDetail
            set rps.lastRotationDate = lmd.eventDate
        }
        If (rps.lastInspectionEventLmd == null or lmd.eventDate > rps.lastInspectionEventLmd) and (rps.lastMaintenanceEventLmd == null or lmd.eventDate > rps.lastMaintenanceEventLmd.eventDate)
            if (lmd.reportedWorkingHours != null && lmd.reportedWorkingHours > 0) {
                set rps.lastReportedWorkingHours = lmd.reportedWorkingHours
            }
            if (lmd.reportedOperations != null && lmd.reportedOperations > 0) {
                set rps.lastReportedOperations = lmd.reportedOperations
            }
        }

    } // end of rotation case

    // start of 'EFE' case
    if lmd.recordType = 'EFE' {
        if (rps.lastMaintenanceEventLmd == null || lmd.eventDate > rps.lastMaintenanceEventLmd.eventDate) {
            set rps.lastMaintenanceEventLmd = lmd
        }

        if (lmd.outboardEndUse not blank) {
            set rps.outboardEndUse = lmd.outboardEndUse
        }
        else {
             set lmd.supportStatus = 'under review'
             set lmd.fieldServiceNotes = lmd.fieldServiceNotes + ' ; ' + 'EFE entered without appropriate data'
        }
             

        if (lmd.eventDate > rps.lastInspectionEventLmd.eventDate && rps.lastMaintenanceEventLmd.eventDate) {
            set rps.lastReportedWorkingHours = lmd.reportedWorkingHours
            set rps.lastReportedOperations = lmd.reportedOperations
        }

    } // end of EFE case

    // start of 'inspection' case
    if lmd.recordType = 'inspection' {
        if (lmd.eventDate > rps.lastInspectionEventLmd.eventDate) {
            set rps.lastInspectionEventLmd = lmd
        }

        if (lmd.lineDisposition = 'under review') {
            set rps.lineDisposition = lmd.lineDisposition
        }

        if (lmd.eventDate > rps.lastInspectionEventLmd.eventDate && rps.lastMaintenanceEventLmd.eventDate) {
            set rps.lastReportedWorkingHours = lmd.reportedWorkingHours
            set rps.lastReportedOperations = lmd.reportedOperations
        }

    } // end of 'inspection' case

    // start of 'cropping' or 'repair' case
    if lmd.recordType = 'cropping' or 'repair' {
        if (lmd.eventDate > rps.lastMaintenanceEventLmd.eventDate) {
            set rps.lastMaintenanceEventLmd = lmd
        }

        if (lmd.croppedLength != 0))
		{
			rps.currentLengthMeters = Max(0, rps.currentLengthMeters - (lmd.croppedLength + 3))
        }
        
        if (lmd.eventDate > rps.lastInspectionEventLmd.eventDate && rps.lastMaintenanceEventLmd.eventDate) {
            set rps.lastReportedWorkingHours = lmd.reportedWorkingHours
            set rps.lastReportedOperations = lmd.reportedOperations
        }

    } // end of 'cropping' or 'repair' case


    // start of 'asset activity' case
    if lmd.recordType = 'asset activity' {
        set rps.accumulatedWorkingHours = rps.accumulatedWorkingHours + lmd.hoursUsed
        set rps.accumulatedOperations = rps.accumulatedOperations + 1

        set tailRps.accumulatedWorkingHours = tailRps.accumulatedWorkingHours + lmd.hoursUsed
        set tailRps.accumulatedOperations = tailRps.accumulatedOperations + 1
        
        if lmd.hoursUsed = 0 {
            set lmd.supportStatus = 'under review'
            set lmd.fieldServiceNotes = lmd.fieldServiceNotes + ' ; ' + 'dates entered incorrectly, hours used not calculated correctly.'
        }

    } // end of 'asset activity' case


    // start of 'testing' case
    if lmd.recordType = 'testing' {
        if (lmd.eventDate > rps.lastMaintenanceEventLmd.eventDate) {
            set rps.lastMaintenanceEventLmd = lmd
        }

    } // end of testing case

    // start of 'equipment survey' case
    if lmd.recordType = 'equipment survey' {
        if (lmd.eventDate > eqp.lastInspectionEventLmd.eventDate) {
            set eqp.lastInspectionEventLmd = lmd
        }

        set eqp.dispostion = lmd.equipmentDisposition

    } // end of equipment survey case

    set lmd.processed = true
}

