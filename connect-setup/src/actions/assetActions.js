// update all assets
export function UpdateAllAssets(allAssets) {
  return {
    type: 'UPDATE_ALL_ASSETS',
    allAssets,
  };
}

// update account assets
export function UpdateAccountAssets(accountId) {
  return {
    type: 'UPDATE_ACCOUNT_ASSETS',
    accountId,
  };
}

// update selected asset
export function UpdateSelectedAsset(selectedAssetId) {
  return {
    type: 'UPDATE_SELECTED_ASSET',
    selectedAssetId,
  };
}
