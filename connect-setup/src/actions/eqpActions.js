// update equipment details all
export function UpdateEquipmentDetailsAll(equipmentDetailsAll) {
  return {
    type: 'UPDATE_EQUIPMENT_DETAILS_ALL',
    equipmentDetailsAll,
  };
}

// update equipment detials asset
export function UpdateEquipmentDetailsAsset(selectedAssetId, assets) {
  return {
    type: 'UPDATE_EQUIPMENT_DETAILS_ASSET',
    selectedAssetId,
    assets,
  };
}

// update selected equipment detail
export function UpdateSelectedEquipmentDetail(selectedEquipmentDetailId) {
  return {
    type: 'UPDATE_SELECTED_EQUIPMENT_DETAIL',
    selectedEquipmentDetailId,
  };
}

export function UpdateAssetId(assetId) {
  return {
    type: 'UPDATE_ASSET_ID',
    assetId,
  };
}

export function UpdateSelEqpId(selEqpId) {
  return {
    type: 'UPDATE_SEL_EQP_ID',
    selEqpId,
  };
}
