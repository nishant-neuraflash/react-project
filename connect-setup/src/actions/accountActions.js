// update accounts
export function UpdateAccounts(accounts) {
  return {
    type: 'UPDATE_ACCOUNTS',
    accounts,
  };
}

// update selected account
export function UpdateSelectedAccount(selectedAccountId) {
  return {
    type: 'UPDATE_SELECTED_ACCOUNT',
    selectedAccountId,
  };
}
