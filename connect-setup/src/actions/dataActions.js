
// update path to static resources
export function UpdatePath(path) {
  return {
    type: 'UPDATE_PATH',
    path,
  };
}

export function dummy() {
  return 'dummy';
}
