
// update rope prod specs all
export function UpdateRpsesAll(rpsesAll) {
  return {
    type: 'UPDATE_RPSES_ALL',
    rpsesAll,
  };
}

// update rope prod specs for selected asset
export function UpdateRpsesAsset(initial, rpses) {
  return {
    type: 'UPDATE_RPSES_ASSET',
    initial,
    rpses,
  };
}

// update rope prod specs for selected asset (filtered)
export function UpdateRpsesAssetFiltered(initial, rpses) {
  return {
    type: 'UPDATE_RPSES_ASSET_FILTERED',
    initial,
    rpses,
  };
}

// update slected rope prod spec
export function UpdateSelectedRps(selectedRps) {
  return {
    type: 'UPDATE_SELECTED_RPS',
    selectedRps,
  };
}

export function UpdateAssetId(assetId) {
  return {
    type: 'UPDATE_ASSET_ID',
    assetId,
  };
}
