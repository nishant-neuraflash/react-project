// get accounts
export function getAccounts(props) {
  window.connect_setup_controller.getAccounts(
    (accounts, event) => {
      if (event.statusCode === 200) {
        // update accounts
        props.accountActions.UpdateAccounts(accounts);
        props.appActions.UpdateCallBackCount();
      }
    },
    { escape: false, dataType: 'json', timeout: 60000 },
  );
}

// get assets
export function loadAssets(props) {
  window.connect_setup_controller.loadAssets(
    (result, event) => {
      if (event.statusCode === 200) {
        // update assets
        props.assetActions.UpdateAllAssets(result);
        // update selected asset to be the first asset
        if (result !== undefined && result.length > 0) {
          props.assetActions.UpdateSelectedAsset(result[0].Id);
          props.rpsActions.UpdateAssetId(result[0].Id);
          props.eqpActions.UpdateAssetId(result[0].Id);
        }
        props.appActions.UpdateCallBackCount();
      }
    },
    { escape: false, dataType: 'json', timeout: 60000 },
  );
}

// get rope prod specs
export function loadRopeProductSpecs(props) {
  window.connect_setup_controller.loadRopeProductSpecs(
    (result, event) => {
      if (event.statusCode === 200) {
        props.rpsActions.UpdateRpsesAll(result);
        props.rpsActions.UpdateRpsesAsset(true, result);
        props.rpsActions.UpdateRpsesAssetFiltered(true, result);
        // update selected equipment detail to be the first equipment detail of the asset
        // props.UpdateSelectedRopeProdSpec(-1);
        props.appActions.UpdateCallBackCount();
      }
    },
    { escape: false, dataType: 'json', timeout: 60000 },
  );
}

// get equipment details
export function loadEquipmentDetails(props) {
  window.connect_setup_controller.loadEquipmentDetails(
    (result, event) => {
      if (event.statusCode === 200) {
        props.eqpActions.UpdateEquipmentDetailsAll(result);
        props.eqpActions.UpdateEquipmentDetailsAsset();
        props.appActions.UpdateCallBackCount();
      }
    },
    { escape: false, dataType: 'json', timeout: 60000 },
  );
}

export function loadLineMaintenanceDetails(props) {
  window.connect_setup_controller.loadLineMaintenanceDetails(
    (result, event) => {
      if (event.statusCode === 200) {
        props.lmdActions.UpdateLmdsAll(result);
        props.appActions.UpdateCallBackCount();
      }
    },
    { escape: false, dataType: 'json', timeout: 60000 },
  );
}

export function getCDLs(props) {
  window.connect_setup_controller.getCDLs(
    (cdls, event) => {
      if (event.statusCode === 200) {
        props.lmdActions.UpdateCDLs(cdls);
        props.appActions.UpdateCallBackCount();
      }
    },
    { escape: false, dataType: 'json', timeout: 60000 },
  );
}

export function updateEqpXYCord(eqp) {
  window.connect_setup_controller.updateEqpXYCord(JSON.stringify(eqp), () => { }, {
    escape: false,
    dataType: 'json',
    timeout: 60000,
  });
}
