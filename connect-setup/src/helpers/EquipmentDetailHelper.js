export function getEqpFillColor(disposition, type) {
  if (disposition) {
    if (disposition === 'Continue Use') {
      return '#41AD49';
    } else if (disposition === 'Under Review') {
      return '#FFD400';
    } else if (disposition === 'Upgrade' || disposition === 'Repair') {
      return '#D2232A';
    }
  }
  return '#FFF';
  // return type === 'Chalk' ? '#073F80' : '#BFCFD6';
}

function toNumber(text) {
  return isNaN(text) ? 0 : parseInt(text, 10);
}

export function getTransform(rotation_, x_, y_, width, height) {
  const rotation = toNumber(rotation_);
  const x = toNumber(x_) + (width / 2);
  const y = toNumber(y_) + (height / 2);

  return `rotate(${rotation} ${x} ${y})`;
}

function getEqpFillColorHelper(disposition) {
  if (disposition) {
    if (disposition === 'Continue Use') {
      return '#41AD49';
    } else if (disposition === 'Under Review') {
      return '#FFD400';
    } else if (disposition === 'Upgrade' || disposition === 'Repair') {
      return '#D2232A';
    }
  }
  return '#FFF';
}

export function getDrumColor(rpses, dispo) {
  let color = '#FFF';
  for (let i = 0; i < rpses.length; i++) {
    const rps = rpses[i];
    if (rps && rps.Disposition__c) {
      const disposition = rps.Disposition__c;
      if (disposition === 'Continue Use' && color !== '#FFD400' && color !== '#D2232A') {
        color = '#41AD49';
      } else if (disposition === 'Under Review' && color !== '#D2232A') {
        color = '#FFD400';
      } else if (disposition === 'Retire/Discard' || disposition === 'Repair') {
        color = '#D2232A';
      } else if (color === '#FFF') {
        color = '#909090';
      }
    }
  }
  return color;
}

export function getDrumColorInfoBox(rps) {
  if (rps && rps.Disposition__c) {
    const disposition = rps.Disposition__c;
    if (disposition === 'Continue Use') {
      return '#41AD49';
    } else if (disposition === 'Under Review') {
      return '#FFD400';
    } else if (disposition === 'Retire/Discard' || disposition === 'Repair') {
      return '#D2232A';
    }
    return '#909090';
  }
  return '#FFF';
}
