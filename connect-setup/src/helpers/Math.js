export function toNumber(text) {
  return isNaN(text) ? 0 : parseInt(text, 10);
}

export function max(a, b) {
  return a > b ? a : b;
}

function dist(p1, p2) {
  const a = p1.x - p2.x;
  const b = p1.y - p2.y;

  return Math.sqrt(a * a + b * b);
}

function pointIsToRight(A, B, P) {
  // subtracting co-ordinates of point A from
  // B and P, to make A as origin
  B.x -= A.x;
  B.y -= A.y;
  P.x -= A.x;
  P.y -= A.y;

  // Determining cross Product
  const crossProduct = B.x * P.y - B.y * P.x;

  return crossProduct >= 0;
}

export function degreeOf3Points(p1, p2, p3) {
  const p12 = dist(p1, p2);
  const p13 = dist(p1, p3);
  const p23 = dist(p2, p3);
  const radian = Math.acos((p12 ** 2 + p13 ** 2 - p23 ** 2) / (2 * p12 * p13));
  const degree = Math.round((radian * 180) / Math.PI);
  const toRight = pointIsToRight(p2, p3, p1);

  return toRight ? degree : 360 - degree;
}
