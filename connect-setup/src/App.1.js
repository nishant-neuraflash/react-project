import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import styled, { keyframes } from 'styled-components';
// components
import * as Controller from './controllers/controller';
import AssetPage from './components/AssetPage/AssetPage';
// actions
import * as assetActions from './actions/assetActions';
import * as rpsActions from './actions/rpsActions';
import * as eqpActions from './actions/eqpActions';
import * as lmdActions from './actions/lmdActions';
import * as appActions from './actions/appActions';
import * as dataActions from './actions/dataActions';

// helper js function
import { getPath } from './helpers/helper';

const spin = keyframes`
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
`;

const Container = styled.div`
  /* Center align */
  margin: auto;
  width: 1px;
  text-align: center;
`;

const Loader = styled.div`
  border: 5px solid #f3f3f3;
  border-radius: 50%;
  border-top: 5px solid #000;
  width: 50px;
  height: 50px;
  animation: ${spin} 2s linear infinite;
  margin-left: 50px;
`;

const divStyle = {
  width: '1070px',
  margin: '10px',
};

function getUrlParameter() {
  const parts = window.parent.location.href.split('/');
  let i = 0;
  for (i = 0; i < parts.length; i++) {
    if (parts[i] === 'asset') {
      return parts[i + 1];
    }
  }

  const query = window.parent.location.search.substring(1);
  const vars = query.split('&');
  for (i = 0; i < vars.length; i++) {
    const pair = vars[i].split('=');
    if (pair[0] === 'assetId') {
      return pair[1];
    }
  }
  return '';
}

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.AssetId = getUrlParameter();
  }

  componentWillMount() {
    this.props.dataActions.UpdatePath(getPath(this.props.title)); // update path to the images
    Controller.loadAssets(this.props); // get/update assets also update selected asset
    Controller.loadRopeProductSpecs(this.props); // get/update rope prod specs
    Controller.loadEquipmentDetails(this.props); // get/update equipment details
    Controller.loadLineMaintenanceDetails(this.props); // get/update line maintenance details
    Controller.getCDLs(this.props); // get/update content document links
  }

  render() {
    if (this.props.appReducer.callBackCount <= 4) {
      return (
        <Container>
          <h1> Loading... </h1>
          <Loader />
        </Container>
      );
    }

    return (
      <div style={divStyle}>
        {' '}
        <AssetPage assetId={this.AssetId} />{' '}
      </div>
    );
  }
}

App.propTypes = {
  title: PropTypes.string.isRequired,
  appReducer: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
  dataActions: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
};

const mapStateToProps = (state) => ({
  appReducer: state.appReducer,
  rpsReducerAll: state.rpsReducerAll,
});

function mapDispatchToProps(dispatch) {
  return {
    assetActions: bindActionCreators(assetActions, dispatch),
    rpsActions: bindActionCreators(rpsActions, dispatch),
    eqpActions: bindActionCreators(eqpActions, dispatch),
    lmdActions: bindActionCreators(lmdActions, dispatch),
    appActions: bindActionCreators(appActions, dispatch),
    dataActions: bindActionCreators(dataActions, dispatch),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(App);
