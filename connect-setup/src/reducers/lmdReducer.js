export default function reducer(state = { lmdsAll: null, cdls: [] }, action) {
  switch (action.type) {
    // update line maintenance details all
    case 'UPDATE_LMDS_ALL': {
      return {
        ...state,
        lmdsAll: action.lmdsAll,
      };
    }
    case 'UPDATE_CDLS': {
      return {
        ...state,
        cdls: action.cdls,
      };
    }
    default:
      return state;
  }
}
