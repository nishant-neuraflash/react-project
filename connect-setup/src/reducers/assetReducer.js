export default function reducer(
  state = {
    allAssets: [],
    accountAssets: [],
    selectedAsset: null,
  },
  action,
) {
  switch (action.type) {
    // update all assets
    case 'UPDATE_ALL_ASSETS':
      return {
        ...state,
        allAssets: action.allAssets,
      };
    // update account assets
    case 'UPDATE_ACCOUNT_ASSETS':
      return {
        ...state,
        accountAssets: state.allAssets.filter((x) => x.AccountId === action.accountId),
        selectedAsset: null,
      };
    // update selected asset
    case 'UPDATE_SELECTED_ASSET': {
      return {
        ...state,
        selectedAsset: state.accountAssets.find((x) => x.Id === action.selectedAssetId),
      };
    }
    default:
      return state;
  }
}
