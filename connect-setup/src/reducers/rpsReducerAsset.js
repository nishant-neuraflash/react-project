export default function reducer(state = {
  rpsesAsset: null,
  rpsesAssetFiltered: null,
  assetId: null,
}, action) {
  switch (action.type) {
    // update rope prod specs for asset
    case 'UPDATE_RPSES_ASSET': {
      if (action.initial) {
        return {
          ...state,
          rpsesAsset: action.rpses.filter(x => x.Asset__c === state.assetId),
        };
      }
      return {
        ...state,
        rpsesAsset: action.rpses,
      };
    }
    // update rope prod specs for asset (filtered)
    case 'UPDATE_RPSES_ASSET_FILTERED': {
      if (action.initial) {
        return {
          ...state,
          rpsesAssetFiltered: action.rpses.filter(x => x.Asset__c === state.assetId && x.Rope_Line__c === 'Mainline'),
        };
      }
      return {
        ...state,
        rpsesAssetFiltered: action.rpses,
      };
    }
    // update asset id for later use
    case 'UPDATE_ASSET_ID': {
      return {
        ...state,
        assetId: action.assetId,
      };
    }
    default:
      return state;
  }
}
