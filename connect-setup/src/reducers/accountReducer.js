export default function reducer(
  state = {
    accounts: null,
    selectedAccount: null,
  },
  action,
) {
  switch (action.type) {
    // update accounts
    case 'UPDATE_ACCOUNTS':
      return {
        ...state,
        accounts: action.accounts,
      };
    // update selected account
    case 'UPDATE_SELECTED_ACCOUNT':
      return {
        ...state,
        selectedAccount: state.accounts.find((x) => x.Id === action.selectedAccountId),
      };
    default:
      return state;
  }
}
