export default function reducer(state = {
  toggle: 'Mainline',
}, action) {
  switch (action.type) {
    case 'UPDATE_TOGGLE':
      return {
        ...state,
        toggle: action.toggle,
      };
    default:
      return state;
  }
}
