export default function reducer(state = {
  rpsesAll: null,
}, action) {
  switch (action.type) {
    // update rope prod specs all
    case 'UPDATE_RPSES_ALL': {
      return {
        ...state,
        rpsesAll: action.rpsesAll,
      };
    }
    default:
      return state;
  }
}
