export default function reducer(state = {
  callBackCount: 0,
}, action) {
  switch (action.type) {
    // update path
    case 'UPDATE_CALL_BACK_COUNT':
      return {
        ...state,
        callBackCount: state.callBackCount + 1,
      };
    default:
      return state;
  }
}
