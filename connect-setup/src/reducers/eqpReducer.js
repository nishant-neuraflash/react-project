export default function reducer(
  state = {
    equipmentDetailsAll: null,
    equipmentDetailsAsset: null,
    selectedEquipmentDetail: null,
    assetId: null,
    selEqpId: '',
  },
  action,
) {
  switch (action.type) {
    // update equipment details all
    case 'UPDATE_EQUIPMENT_DETAILS_ALL': {
      return {
        ...state,
        equipmentDetailsAll: action.equipmentDetailsAll,
        callBackCount: state.callBackCount + 1,
      };
    }
    // update equipment details for asset
    case 'UPDATE_EQUIPMENT_DETAILS_ASSET': {
      // this if statment is added for 'UpdateEqpXYCordinate'
      if (action.assets) {
        return {
          ...state,
          equipmentDetailsAsset: action.assets,
        };
      }
      if (!action.selectedAssetId && state.equipmentDetailsAll) {
        return {
          ...state,
          equipmentDetailsAsset: state.equipmentDetailsAll.filter(
            (x) => x.Asset__c === state.assetId,
          ),
        };
      }
      return {
        ...state,
        equipmentDetailsAsset: state.equipmentDetailsAll.filter(
          (x) => x.Asset__c === action.selectedAssetId,
        ),
      };
    }
    // update selected equipment detail
    case 'UPDATE_SELECTED_EQUIPMENT_DETAIL': {
      // if there are no equipment details for this asset
      if (!state.equipmentDetailsAsset || state.equipmentDetailsAsset.length === 0) {
        return {
          ...state,
        };
      }
      // if no id passed select the firt equipment detail
      const id =
        action.selectedEquipmentDetailId === -1
          ? state.equipmentDetailsAsset[0].Id
          : action.selectedEquipmentDetailId;
      return {
        ...state,
        selectedEquipmentDetail: state.equipmentDetailsAsset.find((x) => x.Id === id),
      };
    }
    // update asset id for later use
    case 'UPDATE_ASSET_ID': {
      return {
        ...state,
        assetId: action.assetId,
      };
    }
    // update selEqpId
    case 'UPDATE_SEL_EQP_ID': {
      return {
        ...state,
        selEqpId: action.selEqpId,
      };
    }
    default:
      return state;
  }
}
