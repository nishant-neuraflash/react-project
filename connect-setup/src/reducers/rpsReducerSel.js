export default function reducer(state = {
  selectedRps: null,
}, action) {
  switch (action.type) {
    // update selected rope prod spec
    case 'UPDATE_SELECTED_RPS': {
      return {
        ...state,
        selectedRps: action.selectedRps,
      };
    }
    default:
      return state;
  }
}
