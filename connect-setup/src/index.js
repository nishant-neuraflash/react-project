import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createBrowserHistory } from 'history';
import { BrowserRouter } from 'react-router-dom';
import configureStore from './store';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

// Create browser history to use in the Redux store
const baseUrl = window.location.href;
const history = createBrowserHistory({ basename: baseUrl });

// Get the application-wide store instance,
// prepopulating with state from the server where available.
const initialState = window.initialReduxState;
const store = configureStore(history, initialState);

const rootElement = document.getElementById('root');

if (process.env.NODE_ENV !== 'production') {
  console.log('Looks like we are in development mode!');
}

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <App {...root.dataset} />
    </BrowserRouter>
  </Provider>,
  rootElement,
);

registerServiceWorker();
