import React from 'react';
import PropTypes from 'prop-types';

// components
import Header from './Header/Header';
import SubHeader from './SubHeader/SubHeader';
import Asset from './Asset/Asset';
import InfoBox from './InfoBox/InfoBox';
import RpsTable from './RpsTable/RpsTable';
import LmdTable from './LmdTable/LmdTable';
import MooringActTable from './MooringActTable/MooringActTable';

const tableLableStyle = {
  borderRadius: '3px',
  backgroundColor: '#000000',
  color: '#ffffff',
  width: '260px',
  fontSize: '13px',
  padding: '1px 10px 1px 10px',
};

const assetPageStyle = {
  font: '12px Arial, Helvetica, sans-serif',
};

const AssetPage = props => (
  <div style={assetPageStyle}>
    <Header assetId={props.assetId} />
    <Asset />
    <InfoBox />
    <SubHeader />
    <div style={tableLableStyle}>
      LINE INFORMATION & STATUS
    </div>
    <RpsTable />
    <div style={tableLableStyle}>
      MAINTENANCE ACTIVITIES
    </div>
    <LmdTable />
    <div style={tableLableStyle}>
      MOORING ACTIVITIES
    </div>
    <MooringActTable />
  </div>
);

AssetPage.defaultProps = {
  assetId: '',
};

AssetPage.propTypes = {
  assetId: PropTypes.string,
};

export default AssetPage;
