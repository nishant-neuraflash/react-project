import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import moment from 'moment';
// helpers
import { getEqpFillColor, getDrumColorInfoBox } from '../../../helpers/EquipmentDetailHelper';
import { environment } from '../../../helpers/helper';
// styles
import styles from './InfoBox.less';

function GetLineInfo(data) {
  if (data.line === undefined || (data.line === null && !data.equipment)) {
    return { Text: null, Id: '' };
  }
  if (data.line === null) {
    return { Text: <span>No line associated with this winch.</span>, Id: '' };
  }
  const lmds = data.line.Line_Maintenance_Details__r;
  if (!lmds || lmds.length === 0) {
    return { Text: <span>No maintenance history associated with this line.</span>, Id: '' };
  }
  const lmd = lmds[0];
  const date = moment.utc(lmd.Inspection_Date__c).format('MM/DD/YY');
  const strands = lmd.Full_Cut_Strands__c;
  const extAbr1 = lmd.External_Abrasion_1__c;
  const extAbr2 = lmd.External_Abrasion_2__c;
  const intAbr1 = lmd.Internal_Abrasion_1__c;
  const intAbr2 = lmd.Internal_Abrasion_2__c;
  let text = `on ${date}`;

  if (strands > 2 || extAbr1 >= 6 || extAbr2 >= 6 || intAbr1 >= 6 || intAbr2 >= 6) {
    text += ' indicates';
    if (strands > 2) {
      text += ' CUT STRANDS';
    }
    if (extAbr1 >= 6 || extAbr2 >= 6 || intAbr1 >= 6 || intAbr2 >= 6) {
      if (strands > 2) {
        text += ',';
      }
      text += ' ABRASION';
    }
    text += '. Required further inspection and/or repair.';
  }
  text += ` | Reported Condition: ${lmd.Support_Status__c}`;

  return { Text: <span><b>Inspection </b>{text}</span>, Id: lmd.Id };
}

function GetTailInfo(data) {
  if (data.tail === undefined || (data.tail === null && !data.equipment)) {
    return { Text: null, Id: '' };
  }
  if (data.tail === null) {
    return { Text: <span>No tail associated with this winch.</span>, Id: '' };
  }
  const lmds = data.tail.Line_Maintenance_Details__r;
  if (!lmds || lmds.length === 0) {
    return { Text: <span>No maintenance history associated with this tail.</span>, Id: '' };
  }
  const lmd = lmds[0];
  const date = moment.utc(lmd.Inspection_Date__c).format('MM/DD/YY');
  const strands = lmd.Full_Cut_Strands__c;
  const bearing = lmd.Bearing_Point_Condition__c;
  let text = `on ${date}`;
  if (strands > 2 || bearing === 'Bad' || bearing === 'Very Bad') {
    text += ' indicates';
    if (strands > 2) {
      text += ' CUT STRANDS';
    }
    if (bearing === 'Bad' || bearing === 'Very Bad') {
      if (strands > 2) {
        text += ',';
      }
      text += ' BEARING POINT CONDITION';
    }

    text += '. Required further inspection and/or repair.';
  }
  text += ` | Reported Condition: ${lmd.Support_Status__c}`;

  return { Text: <span><b>Inspection </b>{text}</span>, Id: lmd.Id };
}

function GetEquipmentInfo(data) {
  if (data.equipment === undefined) {
    return { Text: null, Id: '' };
  }
  const lmds = data.equipment.Line_Maintenance_Details__r;
  if (!lmds || lmds.length === 0) {
    return { Text: <span>No maintenance history associated with this equipment.</span>, Id: '' };
  }
  const lmd = lmds[0];
  const date = moment.utc(lmd.Inspection_Date__c).format('MM/DD/YY');
  const scoringIssue = lmd.Scoring__c;
  const pittingIssue = lmd.Pitting_Rust__c;
  const mobilityIssue = lmd.Mobility__c;
  let text = `on ${date}`;

  if (scoringIssue || pittingIssue || mobilityIssue) {
    text += ' indicates';
    if (scoringIssue) {
      text += ' SCORING';
    }
    if (pittingIssue) {
      if (scoringIssue) {
        text += ',';
      }
      text += ' PITTING/RUST';
    }
    if (mobilityIssue) {
      if (scoringIssue || pittingIssue) {
        text += ' AND';
      }
      text += ' MOBILITY';
    }
    text += ' issues';
  }
  text += ` | Reported Condition: ${lmd.Support_Status__c}`;

  return { Text: <span><b>Inspection </b>{text}</span>, Id: lmd.Id };
}

function getHeaderText(data) {
  // if (data.line || data.tail) {
  //   let position = '';
  //   const rps = data.line ? data.line : data.tail;
  //   const lmds = rps.Line_Maintenance_Details__r;
  //   if (lmds && lmds.length > 0) {
  //     position = lmds[0].Line_Position__c;
  //   }
  //   let text = rps.Rope_Line__c;
  //   if (position) {
  //     text += ` - ${position}`;
  //   }
  //   if (data.equipment) {
  //     text += ` - ${rps.Equipment_Detail__r.Name}`;
  //   }
  //   return text;
  // }
  // if (data.equipment) {
  //   return `${data.equipment.Equipment_Type__c} - ${data.equipment.Name}`;
  // }
  let result = '';
  if (data.equipment) {
    if (data.equipment.Name) {
      result += data.equipment.Name;
    }
    if (data.equipment.Location__c) {
      result += ` - ${data.equipment.Location__c}`;
    }
    if (data.equipment.Ship_SIde__c) {
      result += ` - ${data.equipment.Ship_SIde__c}`;
    }
  } else if (data.line || data.tail) {
    let rps = null;
    if (data.line) {
      rps = data.line;
      result = 'MainLine';
    } else {
      rps = data.tail;
      result = 'Tail';
    }
    if (rps.Status__c) {
      result += ` - ${rps.Status__c}`;
    }
  }
  return result;
}

const InfoBox = (props) => {
  const { data } = props.infoBoxReducer;
  if (!data) {
    return <div />;
  }
  const lineInfo = GetLineInfo(data);
  const lineText = lineInfo.Text;
  const lineLmdId = lineInfo.Id;
  const equipmentInfo = GetEquipmentInfo(data);
  const equipmentText = equipmentInfo.Text;
  const equipmentLmdId = equipmentInfo.Id;
  const tailInfo = GetTailInfo(data);
  const tailText = tailInfo.Text;
  const tailLmdId = tailInfo.Id;
  let lmdLink = 'https://samsonportal.force.com/connect/s/line-maintenance-detail/';
  if (environment() !== 'production') {
    lmdLink = 'https://sambox-samsonportal.cs21.force.com/chevron/s/line-maintenance-detail/'; // sandbox
  }


  let lineColor = '#000';
  let tailColor = '#000';
  let equpColor = '#000';
  if (lineText && data.line) {
    lineColor = getDrumColorInfoBox(data.line);
    if (lineColor === '#FFF') {
      lineColor = '#000';
    }
  }
  if (tailText && data.tail) {
    tailColor = getDrumColorInfoBox(data.tail);
    if (tailColor === '#FFF') {
      tailColor = '#000';
    }
  }
  if (equipmentText && data.equipment) {
    equpColor = getEqpFillColor(data.equipment.Disposition__c);
    if (equpColor === '#FFF') {
      equpColor = '#000';
    }
  }
  return (
    <div>
      <div className={styles.tableLableStyle}>
        LATEST MAINTENANCE ACTIVITIES
      </div>
      <div className={styles.cardStyle}>
        <div className={styles.gridStyle}>
          <div className={styles.gridItemStyle}>
            <br />
            {lineText && <div style={{ color: lineColor, marginRight: '4px' }}>Mainline: <br /></div>}
            {equipmentText && <div style={{ color: equpColor, marginRight: '4px' }}>Equipment: <br /></div>}
            {tailText && <div style={{ color: tailColor, marginRight: '4px' }}>Tail: <br /></div>}
          </div>
          <div className={styles.gridItemStyle}>
            <div style={{ fontWeight: 'bold' }}>{getHeaderText(data)}<br /></div>
            {/* MainLine */}
            {lineText &&
              <div className={styles.infoBoxRow}>{lineText}
                {data.line &&
                  <span> | Current Line Status:&nbsp;
                    <span style={{ fontWeight: 'bold', color: lineColor }}>
                      {data.line.Disposition__c}
                    </span>
                  </span>
                }
                <br />
              </div>}
            {/* Equipment */}
            {equipmentText &&
              <div className={styles.infoBoxRow}>
                {equipmentText}
                {data.equipment &&
                  <span> | Current Equipment Status:&nbsp;
                    <span style={{ fontWeight: 'bold', color: equpColor }}>
                      {data.equipment.Disposition__c}
                    </span>
                  </span>
                }
                <br />
              </div>
            }
            {/* Tail */}
            {tailText &&
              <div className={styles.infoBoxRow}> {tailText}
                {data.tail &&
                  <span> | Current Tail Status:&nbsp;
                    <span style={{ fontWeight: 'bold', color: tailColor }}>
                      {data.tail.Disposition__c}
                    </span>
                  </span>
                }
                <br />
              </div>
            }
          </div>
          <div className={styles.gridItemStyle}>
            <br /> {/* empty detail */}
            {lineLmdId &&
              <div>
                <a
                  rel="noopener noreferrer"
                  href={`${lmdLink}${lineLmdId}`}
                >Details
                </a>
              </div>}
            {(lineText && !lineLmdId) && <br /> } {/* break if has line but no lmd */}
            {equipmentLmdId &&
              <div>
                <a
                  rel="noopener noreferrer"
                  href={`${lmdLink}${equipmentLmdId}`}
                >Details
                </a>
              </div>}
            {(equipmentText && !equipmentLmdId) && <br /> } {/* br if has equipment but no lmd */}
            {tailLmdId &&
              <div>
                <a
                  rel="noopener noreferrer"
                  href={`${lmdLink}${tailLmdId}`}
                >Details
                </a>
              </div>}
          </div>
        </div>
      </div>
    </div>
  );
};

InfoBox.propTypes = {
  infoBoxReducer: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
};

const mapStateToProps = state => ({
  infoBoxReducer: state.infoBoxReducer,
});

export default connect(mapStateToProps, null)(InfoBox);
