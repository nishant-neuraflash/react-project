import React from 'react';
import PropTypes from 'prop-types';
import Chalk from './Chalk';

const Chalks = (props) => (
  <React.Fragment>
    {/* draw Chalks */}
    {props.data.map((d) => (
      <Chalk
        key={`${d.Id}_group`}
        data={d}
        handleClick={props.handleClick}
        selEqpId={props.selEqpId}
        handleUpdateEqpXYCord={props.handleUpdateEqpXYCord}
      />
    ))}
  </React.Fragment>
);

Chalks.propTypes = {
  data: PropTypes.array.isRequired, // eslint-disable-line react/forbid-prop-types
  handleClick: PropTypes.func.isRequired, // eslint-disable-line react/forbid-prop-types
  handleUpdateEqpXYCord: PropTypes.func.isRequired, // eslint-disable-line react/forbid-prop-types
  selEqpId: PropTypes.string.isRequired,
};

export default Chalks;
