import React from 'react';
import PropTypes from 'prop-types';
// from Winch component
import SplitDrum from './SplitDrum';
import SingleDrum from './SingleDrum';
// helpers
import { getEqpFillColor, getDrumColor } from '../../../../../helpers/EquipmentDetailHelper';
import withDrapAndDrop from '../../Equipment/Equipment';

const Winch = (props) => {
  const { rpses, data, selEqpId, x, y, rotation } = props;
  const { Id, Asset__r: asset, Disposition__c: disposition, Winch_drum_Type__c: winchType } = data;
  const glow = selEqpId === Id;
  const wallFillColor = getEqpFillColor(disposition);
  const drumFillColor = getDrumColor(rpses);

  const innerProps = {
    id: Id,
    x,
    y,
    maxEqupVer: asset.Max_Num_Equip_Vertical__c,
    rotation,
    wallFillColor,
    drumFillColor,
    glow,
    handleClick: props.handleClick(data),
    handleMouseDown: props.handleMouseDown,
  };

  return winchType === 'Split Drum' ? (
    <SplitDrum {...innerProps} />
  ) : (
    <SingleDrum {...innerProps} />
  );
};

Winch.defaultProps = {
  rpses: [],
};

Winch.propTypes = {
  rpses: PropTypes.array, // eslint-disable-line react/forbid-prop-types
  data: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
  selEqpId: PropTypes.string.isRequired,
  x: PropTypes.number.isRequired,
  y: PropTypes.number.isRequired,
  rotation: PropTypes.number.isRequired,
  handleClick: PropTypes.func.isRequired, // eslint-disable-line react/forbid-prop-types
  handleMouseDown: PropTypes.func.isRequired, // eslint-disable-line react/forbid-prop-types
};

export default withDrapAndDrop(Winch);
