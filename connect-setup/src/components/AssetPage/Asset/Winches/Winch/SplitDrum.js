import React from 'react';
import PropTypes from 'prop-types';
// helpers
import { toNumber } from '../../../../../helpers/Math';
import { getTransform } from '../../../../../helpers/EquipmentDetailHelper';

// Available Vertical Height in Vessel: 146px
const SplitDrum = (props) => {
  const x = toNumber(props.x);
  const y = toNumber(props.y);
  const maxEqupVer = Math.max(3, toNumber(props.maxEqupVer));
  const vesselVerWidth = 146;
  const eqpWidth = (vesselVerWidth - maxEqupVer * 8) / maxEqupVer;
  const radius = eqpWidth * 0.676;
  const circleDisX = eqpWidth * 0.5;
  const circleDisY = eqpWidth * 0.27;
  const wallHeight = eqpWidth * 0.568;
  const wallWidth = eqpWidth * 0.108;
  const drumHeight = eqpWidth * 0.189;
  const leftDrumWidth = eqpWidth * 0.405;
  const rightDrumWidth = eqpWidth * 0.27;
  const width = wallWidth * 3 + leftDrumWidth + rightDrumWidth;

  return (
    <g>
      {props.glow && (
        <circle
          key={`${props.id}_glow`}
          cx={x + circleDisX}
          cy={y + circleDisY}
          r={radius}
          style={{ stroke: '#00AEEF', strokeWidth: '3px', strokeDasharray: '3' }}
          fill="#FFF"
        />
      )}
      <g
        style={{ cursor: 'pointer' }}
        onClick={props.handleClick}
        onMouseDown={props.handleMouseDown}
        transform={getTransform(props.rotation, x, y, width, wallHeight)}
      >
        {/* rec 1 (wall) */}
        <rect
          x={x}
          y={y}
          width={wallWidth}
          height={wallHeight}
          fill={props.wallFillColor}
          strokeWidth="1"
          stroke="#000000"
        />
        {/* rec 2 (drum) */}
        <rect
          x={x + wallWidth}
          y={y + wallHeight / 3}
          width={leftDrumWidth}
          height={drumHeight}
          fill={props.drumFillColor}
          strokeWidth="1"
          stroke="#000000"
        />
        {/* rec 3 (wall) */}
        <rect
          x={x + wallWidth + leftDrumWidth}
          y={y}
          width={wallWidth}
          height={wallHeight}
          fill={props.wallFillColor}
          strokeWidth="1"
          stroke="#000000"
        />
        {/* rec 4 (drum) */}
        <rect
          x={x + wallWidth + leftDrumWidth + wallWidth}
          y={y + wallHeight / 3}
          width={rightDrumWidth}
          height={drumHeight}
          fill={props.drumFillColor}
          strokeWidth="1"
          stroke="#000000"
        />
        {/* rec 5 (wall) */}
        <rect
          x={x + wallWidth + leftDrumWidth + wallWidth + rightDrumWidth}
          y={y}
          width={wallWidth}
          height={wallHeight}
          fill={props.wallFillColor}
          strokeWidth="1"
          stroke="#000000"
        />
      </g>
    </g>
  );
};

SplitDrum.defaultProps = {
  rotation: 0,
};

SplitDrum.propTypes = {
  id: PropTypes.string.isRequired,
  x: PropTypes.number.isRequired,
  y: PropTypes.number.isRequired,
  maxEqupVer: PropTypes.string.isRequired,
  handleClick: PropTypes.func.isRequired, // eslint-disable-line react/forbid-prop-types
  handleMouseDown: PropTypes.func.isRequired, // eslint-disable-line react/forbid-prop-types
  wallFillColor: PropTypes.string.isRequired,
  drumFillColor: PropTypes.string.isRequired,
  rotation: PropTypes.number,
  glow: PropTypes.bool.isRequired,
};

export default SplitDrum;
