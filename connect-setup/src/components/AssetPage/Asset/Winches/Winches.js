import React from 'react';
import PropTypes from 'prop-types';
// components
import Winch from './Winch/Winch';

const Winches = (props) => {
  const { data, rpsesAsset, selEqpId, handleClick, handleUpdateEqpXYCord } = props;
  const childProps = {
    selEqpId,
    handleClick,
    handleUpdateEqpXYCord,
  };
  return (
    <g>
      {data.map((d) => (
        <Winch
          key={d.Id}
          data={d}
          rpses={rpsesAsset.filter((x) => x.Equipment_Detail__c === d.Id)}
          {...childProps}
        />
      ))}
    </g>
  );
};

Winches.defaultProps = {
  rpsesAsset: [],
};

Winches.propTypes = {
  data: PropTypes.array.isRequired, // eslint-disable-line react/forbid-prop-types
  rpsesAsset: PropTypes.array, // eslint-disable-line react/forbid-prop-types
  handleClick: PropTypes.func.isRequired, // eslint-disable-line react/forbid-prop-types
  handleUpdateEqpXYCord: PropTypes.func.isRequired, // eslint-disable-line react/forbid-prop-types
  selEqpId: PropTypes.string.isRequired,
};

export default Winches;
