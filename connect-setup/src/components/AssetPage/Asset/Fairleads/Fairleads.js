import React from 'react';
import PropTypes from 'prop-types';
import Fairlead from './Fairlead';

const Fairleads = (props) => (
  <React.Fragment>
    {/* draw Chalks */}
    {props.data.map((d) => (
      <Fairlead
        key={`${d.Id}_group`}
        data={d}
        handleClick={props.handleClick}
        selEqpId={props.selEqpId}
        handleUpdateEqpXYCord={props.handleUpdateEqpXYCord}
      />
    ))}
  </React.Fragment>
);

Fairleads.propTypes = {
  data: PropTypes.array.isRequired, // eslint-disable-line react/forbid-prop-types
  handleClick: PropTypes.func.isRequired, // eslint-disable-line react/forbid-prop-types
  handleUpdateEqpXYCord: PropTypes.func.isRequired, // eslint-disable-line react/forbid-prop-types
  selEqpId: PropTypes.string.isRequired,
};

export default Fairleads;
