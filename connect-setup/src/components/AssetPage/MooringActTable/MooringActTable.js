import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
// components
import Table from 'custom_react_table';
// helpers
import { environment } from '../../../helpers/helper';

const MooringActTable = (props) => {
  let commLink = 'https://samsonportal.force.com/connect/s/'; // production
  if (environment() !== 'production') {
    commLink = 'https://sambox-samsonportal.cs21.force.com/chevron/s/'; // sandbox
  }
  const columns = [
    {
      id: 'Port_Name__c',
      head: {
        type: 'TEXT',
        info: { label: 'PORT NAME' },
      },
      body: {
        type: 'LINK',
        info: { link: `${commLink}detail/`, linkId: 'Work_Order__c' },
      },
    },
    {
      id: 'Port_Country__c',
      head: {
        type: 'TEXT',
        info: { label: 'PORT COUNTRY' },
      },
      body: {
        type: 'TEXT',
        info: {},
      },
    },
    {
      id: 'Sheltered_Exposed_Port__c',
      head: {
        type: 'TEXT',
        info: { label: 'PORT/TERMINAL TYPE' },
      },
      body: {
        type: 'TEXT',
        info: {},
      },
    },
    {
      id: 'Date_Time_of_All_Fast__c',
      head: {
        type: 'TEXT',
        info: { label: 'ALL FAST' },
      },
      body: {
        type: 'DATE_TIME',
        info: { dateTimeFormat: 'll' },
      },
    },
    {
      id: 'Date_Time_of_All_Let_Go__c',
      head: {
        type: 'TEXT',
        info: { label: 'ALL LET GO' },
      },
      body: {
        type: 'DATE_TIME',
        info: { dateTimeFormat: 'll' },
      },
    },
    {
      id: 'Equipment_Detail__r.Name',
      head: {
        type: 'TEXT',
        info: { label: 'WINCH #' },
      },
      body: {
        type: 'TEXT',
        info: {},
      },
    },
    {
      id: 'Ship_Side__c',
      head: {
        type: 'TEXT',
        info: { label: 'SHIP SIDE' },
      },
      body: {
        type: 'TEXT',
        info: {},
      },
    },
    {
      id: 'Outboard_End_of_the_Line_in_Use_A_or_B__c',
      head: {
        type: 'TEXT',
        info: { label: 'OUTBOARD END' },
      },
      body: {
        type: 'TEXT',
        info: {},
      },
    },
    {
      id: 'Inspector_Observation__c',
      head: {
        type: 'TEXT_CENTER',
        info: { label: 'ACTIVITY' },
      },
      body: {
        type: 'POPUP_ICON',
        info: { iconUrl: `${props.dataReducer.path}Exclamation.png` },
      },
    },
    {
      id: 'Hours_Used__c',
      head: {
        type: 'NUMBER',
        info: { label: 'HOURS USED' },
      },
      body: {
        type: 'NUMBER',
        info: { toFixed: 1 },
      },
    },
  ];
  const lmds = (props.lmdReducer.lmdsAll && props.rpsReducerSel.selectedRps) ?
    props.lmdReducer.lmdsAll.filter(x =>
      x.Rope_Product_Spec__c === props.rpsReducerSel.selectedRps.Id && x.RecordType.Name === 'Asset Activity') : [];
  const count = lmds.length;
  for (let i = 0; i < count; i++) {
    const lmd = lmds[i];
    if (lmd.Severe_Loading_Conditions__c && !lmd.Inspector_Observation__c) {
      lmd.Inspector_Observation__c = 'Crew have noted severe loading conditions during this mooring operation. No further details were provided.';
    }
  }
  return (
    <Table
      columns={columns}
      data={lmds}
      highlightedRow={{}}
      width={1020}
      height={150}
      order="desc"
      orderBy="Inspection_Date__c"
    />
  );
};

MooringActTable.propTypes = {
  lmdReducer: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
  rpsReducerSel: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
  dataReducer: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
};

const mapStateToProps = state => ({
  lmdReducer: state.lmdReducer,
  rpsReducerSel: state.rpsReducerSel,
  dataReducer: state.dataReducer,
});

export default connect(mapStateToProps, null)(MooringActTable);
