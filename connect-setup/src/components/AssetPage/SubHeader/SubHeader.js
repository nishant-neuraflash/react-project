import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import * as eqpActions from '../../../actions/eqpActions';
import * as rpsActions from '../../../actions/rpsActions';
import * as headerActions from '../../../actions/headerActions';
import * as infoBoxActions from '../../../actions/infoBoxActions';
// styles
import styles from './SubHeader.less';
// helpers
import { environment } from '../../../helpers/helper';

const grayButton = {
  backgroundColor: '#e7e7e7',
  background: '#e7e7e7',
  border: 'none',
  color: '#000000',
  padding: '1px',
  textAlign: 'center',
  textDecoration: 'none',
  display: 'inline-block',
  margin: '.5px',
  cursor: 'pointer',
  width: '70px',
};

const greenButton = {
  backgroundColor: '#00AEEF',
  background: '#00AEEF',
  border: '1px solid #00AEEF',
  boxShadow: '0 0 5px #00AEEF',
  color: '#ffffff',
  padding: '1px',
  textAlign: 'center',
  textDecoration: 'none',
  display: 'inline-block',
  margin: '.5px',
  cursor: 'pointer',
  width: '70px',
};

function getOptionValue(rps) {
  let position = '';
  const lmds = rps.Line_Maintenance_Details__r;
  if (lmds && lmds.length > 0) {
    position = lmds[0].Line_Position__c;
  }
  let text = rps.Rope_Line__c;
  if (position) {
    text += ` - ${position}`;
  }
  text += ` - ${rps.Equipment_Detail__r.Name}`;
  return text;
}

class SubHeader extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      lineType: 'Mainline',
    };
    if (environment() === 'production') {
      this.selectURL = 'https://samsonportal.force.com/connect/resource/Select_Arrow';
    } else {
      this.selectURL = 'https://sambox-samsonportal.cs21.force.com/chevron/resource/Select_Arrow';
    }
  }

  handlMainlineClick = () => {
    this.handleToggleClick('Mainline');
  };

  handlTailClick = () => {
    this.handleToggleClick('Mooring Tails');
  };

  handleToggleClick = (lineType) => {
    if (this.state.lineType !== lineType) {
      const rpsesAssetFiltered = this.props.rpsReducerAsset.rpsesAsset.filter(x =>
        x.Rope_Line__c === lineType);
      this.props.headerActions.UpdateToggle(lineType);
      this.props.rpsActions.UpdateRpsesAssetFiltered(false, rpsesAssetFiltered);
      const { selEqpId } = this.props.eqpReducer;
      let selectedRps = null;
      if (rpsesAssetFiltered && selEqpId) {
        selectedRps = rpsesAssetFiltered.find(x => x.Equipment_Detail__c === selEqpId);
      }
      this.props.rpsActions.UpdateSelectedRps(selectedRps); // update selected rope prod spec
      this.setState({ lineType });
    }
  }

  handleChangeRopeProdSpec = (event) => {
    const rps =
    this.props.rpsReducerAsset.rpsesAssetFiltered.find(x => x.Id === event.target.value);
    const winch =
    this.props.eqpReducer.equipmentDetailsAsset.find(x => x.Id === rps.Equipment_Detail__c);

    this.props.rpsActions.UpdateSelectedRps(rps); // update selected rps
    this.props.eqpActions.UpdateSelEqpId(winch ? winch.Id : ''); // update selected winch
    this.updateInfoBox(rps, winch); // update info box with selected rps
  };

  // update the info box with the given rps
  updateInfoBox = (rps, winch) => {
    let infoBoxData = null;
    if (rps) {
      infoBoxData = { line: null, tail: null };
      // assign the selected mainline/tail
      if (rps.Rope_Line__c === 'Mainline') {
        infoBoxData.line = rps;
      } else {
        infoBoxData.tail = rps;
      }
      // get the mainline or tail from the winch
      if (winch) {
        infoBoxData.equipment = winch;
        let mainline = null;
        let tail = null;
        // if winch has rope prod specs
        if (winch.Rope_Product_Specs__r) {
          mainline = winch.Rope_Product_Specs__r.find(x => x.Rope_Line__c === 'Mainline');
          tail = winch.Rope_Product_Specs__r.find(x => x.Rope_Line__c === 'Mooring Tails');
        }
        // if the winch has a mainline and the passed rps is not a line
        if (mainline && !infoBoxData.line) {
          infoBoxData.line = this.props.rpsReducerAsset.rpsesAsset.find(x => x.Id === mainline.Id);
        }
        // if the winch has a tail and the passed rps is not a tail
        if (tail && !infoBoxData.tail) {
          infoBoxData.tail = this.props.rpsReducerAsset.rpsesAsset.find(x => x.Id === tail.Id);
        }
      }
    }
    this.props.infoBoxActions.UpdateInfoBoxData(infoBoxData);
  }

  render() {
    const assetR = this.props.assetReducer;
    if (!assetR.assets || assetR.assets.length === 0 || !assetR.selectedAsset) {
      if (!assetR.assets || assetR.assets.length === 0) {
        return <div > No Assets! </div>;
      }
      return <div > No Selected Asset! </div>;
    }
    const rpsRedSel = this.props.rpsReducerSel;
    const rpsesAssetFiltered =
    this.props.rpsReducerAsset.rpsesAssetFiltered.filter(x => x.Equipment_Detail__c);

    return (
      <div>
        <div style={{ float: 'right', width: '300px' }}>
          <div className={styles.gridItemStyle}>
            <div className={styles.selectContainer}>
              <label className={styles.selectLableStyle}>Lines</label>
              <select
                value={rpsRedSel.selectedRps ? rpsRedSel.selectedRps.Id : ''}
                onChange={this.handleChangeRopeProdSpec}
                className={styles.selectStyle}
                style={{ backgroundImage: `url(${this.selectURL})` }}
              >
                {rpsesAssetFiltered &&
                  rpsesAssetFiltered.map((rps, index) => (
                    <option key={index} value={rps.Id}>{getOptionValue(rps)}</option>
                  ))
                }
              </select>
            </div>
          </div>
        </div>
        <div style={{ float: 'right' }}>
          <button style={this.state.lineType === 'Mainline' ? greenButton : grayButton} onClick={this.handlMainlineClick}>
            Mainlines
          </button>
          <button style={this.state.lineType === 'Mooring Tails' ? greenButton : grayButton} onClick={this.handlTailClick}>
            Tails
          </button>
        </div>
        <br />
      </div>
    );
  }
}

SubHeader.propTypes = {
  assetReducer: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
  rpsReducerAsset: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
  eqpReducer: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
  rpsActions: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
  headerActions: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
  rpsReducerSel: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
  eqpActions: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
  infoBoxActions: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
};

const mapStateToProps = state => ({
  assetReducer: state.assetReducer,
  rpsReducerSel: state.rpsReducerSel,
  rpsReducerAsset: state.rpsReducerAsset,
  eqpReducer: state.eqpReducer,
});

function mapDispatchToProps(dispatch) {
  return {
    rpsActions: bindActionCreators(rpsActions, dispatch),
    headerActions: bindActionCreators(headerActions, dispatch),
    eqpActions: bindActionCreators(eqpActions, dispatch),
    infoBoxActions: bindActionCreators(infoBoxActions, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(SubHeader);
