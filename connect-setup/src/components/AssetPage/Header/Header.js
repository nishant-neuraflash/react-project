import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import Select from 'react-select';
import * as accountActions from '../../../actions/accountActions';
import * as assetActions from '../../../actions/assetActions';
import * as rpsActions from '../../../actions/rpsActions';
import * as eqpActions from '../../../actions/eqpActions';
import * as headerActions from '../../../actions/headerActions';
import * as infoBoxActions from '../../../actions/infoBoxActions';
import * as appActions from '../../../actions/appActions';

// styles
import styles from './Header.less';
// helpers
import { environment } from '../../../helpers/helper';

class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      lineType: 'Mainline',
    };
    if (environment() === 'production') {
      this.selectURL = 'https://samsonportal.force.com/connect/resource/Select_Arrow';
      this.commLink = 'https://samsonportal.force.com/connect/s/'; // production
    } else {
      this.selectURL = 'https://sambox-samsonportal.cs21.force.com/chevron/resource/Select_Arrow';
      this.commLink = 'https://sambox-samsonportal.cs21.force.com/chevron/s/'; // sandbox
    }
  }

  componentWillMount() {
    if (this.props.assetId) {
      this.changeAsset(this.props.assetId);
    } else {
      const { rpsesAssetFiltered } = this.props.rpsReducerAsset;
      const eqpsAsset = this.props.eqpReducer.equipmentDetailsAsset;
      this.updateRpsAndLmd(rpsesAssetFiltered, eqpsAsset);
    }
  }

  handleChangeAsset = (event) => {
    this.changeAsset(event.target.value);
  };

  changeAsset = (selectedAsset) => {
    const assetId = selectedAsset.value;
    this.props.assetActions.UpdateSelectedAsset(assetId); // update asset
    const rpsesAsset = this.props.rpsReducerAll.rpsesAll.filter((x) => x.Asset__c === assetId);
    this.props.rpsActions.UpdateRpsesAsset(false, rpsesAsset); // update rpses
    const rpsesAssetFiltered = rpsesAsset.filter((x) => x.Rope_Line__c === this.state.lineType);
    this.props.rpsActions.UpdateRpsesAssetFiltered(false, rpsesAssetFiltered);
    this.props.eqpActions.UpdateEquipmentDetailsAsset(assetId); // update equipments
    const eqpsAsset = this.props.eqpReducer.equipmentDetailsAll.filter(
      (x) => x.Asset__c === assetId,
    );
    this.updateRpsAndLmd(rpsesAssetFiltered, eqpsAsset);
  };

  updateRpsAndLmd = (rpsesAssetFiltered, eqpsAsset) => {
    let selectedRps = null;
    let winch = null;
    if (rpsesAssetFiltered && rpsesAssetFiltered.length > 0) {
      [selectedRps] = rpsesAssetFiltered;
      winch = eqpsAsset.find((x) => x.Id === selectedRps.Equipment_Detail__c);
    }
    this.props.rpsActions.UpdateSelectedRps(selectedRps); // update selected rope prod spec
    this.props.eqpActions.UpdateSelEqpId(winch ? winch.Id : ''); // update selected winch
    this.updateInfoBox(selectedRps, winch); // update info box with selected rps
  };

  // update the info box with the given rps
  updateInfoBox = (rps, winch) => {
    let infoBoxData = null;
    if (rps) {
      infoBoxData = { line: null, tail: null };
      // assign the selected mainline/tail
      if (rps.Rope_Line__c === 'Mainline') {
        infoBoxData.line = rps;
      } else {
        infoBoxData.tail = rps;
      }
      // get the mainline or tail from the winch
      if (winch) {
        infoBoxData.equipment = winch;
        let mainline = null;
        let tail = null;
        // if winch has rope prod specs
        if (winch.Rope_Product_Specs__r) {
          mainline = winch.Rope_Product_Specs__r.find((x) => x.Rope_Line__c === 'Mainline');
          tail = winch.Rope_Product_Specs__r.find((x) => x.Rope_Line__c === 'Mooring Tails');
        }
        // if the winch has a mainline and the passed rps is not a line
        if (mainline && !infoBoxData.line) {
          infoBoxData.line = this.props.rpsReducerAsset.rpsesAsset.find(
            (x) => x.Id === mainline.Id,
          );
        }
        // if the winch has a tail and the passed rps is not a tail
        if (tail && !infoBoxData.tail) {
          infoBoxData.tail = this.props.rpsReducerAsset.rpsesAsset.find((x) => x.Id === tail.Id);
        }
      }
    }
    this.props.infoBoxActions.UpdateInfoBoxData(infoBoxData);
  };

  handleChangeAccount = (selectedAccount) => {
    this.changeAsset({ value: '' });
    this.props.accountActions.UpdateSelectedAccount(selectedAccount.value);
    this.props.assetActions.UpdateAccountAssets(selectedAccount.value);
  };

  handleLmpClilck = (assetId) => () => {
    window.parent.location.href = `${
      this.commLink
      }connect-reports?assetId=${assetId}&workTypeNameId=Line Management Plan`;
  };

  handleMsClilck = (assetId) => () => {
    window.parent.location.href = `${this.commLink}maintenance-activities?assetId=${assetId}`;
  };

  handleReportsClilck = (assetId) => () => {
    window.parent.location.href = `${this.commLink}connect-reports?assetId=${assetId}`;
  };

  render() {
    const { accountReducer } = this.props;
    const { accounts, selectedAccount } = accountReducer;
    const accountOptions = accounts.map((a) => ({
      value: a.Id,
      label: a.Name,
    }));
    let accountValue = null;
    if (selectedAccount) {
      accountValue = { value: selectedAccount.Id, label: selectedAccount.Name };
    }
    const assetR = this.props.assetReducer;
    const { accountAssets, selectedAsset } = assetR;
    const assetOptions = accountAssets.map((a) => ({
      value: a.Id,
      label: a.Name,
    }));
    let assetValue = null;
    if (selectedAsset) {
      assetValue = { value: selectedAsset.Id, label: selectedAsset.Name };
    }
    return (
      <div>
        <br />
        <h5>Select Account</h5>
        <div style={{ width: '400px' }}>
          <Select
            value={accountValue}
            onChange={this.handleChangeAccount}
            options={accountOptions}
            isSearchable
          />
        </div>
        <br />
        <div className={styles.cardStyle}>
          <div className={styles.gridStyle}>
            {/* (Column 1) Account Dropdown List */}
            <div className={styles.gridItemStyle}>
              <div>Vessels</div>
              <Select value={assetValue} onChange={this.changeAsset} options={assetOptions} />
            </div>

            {/* (Column 2) Lines Dropdown List */}
            <div className={styles.gridItemStyle} />

            {/* (Column 3) Reports Button */}
            <div className={styles.gridItemStyle}>
              {selectedAsset && (
                <div
                  className={styles.buttonStyle}
                  onClick={this.handleReportsClilck(selectedAsset.Id)}
                >
                  <img src={`${this.props.dataReducer.path}Report.png`} alt="Reports" width="35" />
                  Reports
                </div>
              )}
            </div>

            {/* (Column 4) Line Management Plan */}
            <div className={styles.gridItemStyle}>
              {selectedAsset && (
                <div
                  className={styles.buttonStyle}
                  onClick={this.handleLmpClilck(selectedAsset.Id)}
                >
                  <img
                    src={`${this.props.dataReducer.path}Compliance.png`}
                    alt="Line Management Plan"
                    width="35"
                  />
                  Line Management Plan
                </div>
              )}
            </div>

            {/* (Column 5) Maintenance Schedule */}
            <div className={styles.gridItemStyle}>
              {selectedAsset && (
                <div className={styles.buttonStyle} onClick={this.handleMsClilck(selectedAsset.Id)}>
                  <img
                    src={`${this.props.dataReducer.path}Calendar.png`}
                    alt="Maintenance Schedule"
                    width="32"
                  />
                  Maintenance Schedule
                </div>
              )}
            </div>
            {/* (Column 5) END */}
          </div>
        </div>
      </div>
    );
  }
}

Header.defaultProps = {
  assetId: '',
};

Header.propTypes = {
  assetReducer: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
  rpsReducerAll: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
  rpsReducerAsset: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
  rpsReducerSel: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
  eqpReducer: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
  dataReducer: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types

  assetActions: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
  rpsActions: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
  eqpActions: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
  infoBoxActions: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
  headerActions: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
  appActions: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types

  assetId: PropTypes.string,
};

const mapStateToProps = (state) => ({
  accountReducer: state.accountReducer,
  assetReducer: state.assetReducer,
  rpsReducerAll: state.rpsReducerAll,
  rpsReducerAsset: state.rpsReducerAsset,
  rpsReducerSel: state.rpsReducerSel,
  eqpReducer: state.eqpReducer,
  dataReducer: state.dataReducer,
});

function mapDispatchToProps(dispatch) {
  return {
    accountActions: bindActionCreators(accountActions, dispatch),
    assetActions: bindActionCreators(assetActions, dispatch),
    rpsActions: bindActionCreators(rpsActions, dispatch),
    eqpActions: bindActionCreators(eqpActions, dispatch),
    infoBoxActions: bindActionCreators(infoBoxActions, dispatch),
    headerActions: bindActionCreators(headerActions, dispatch),
    appActions: bindActionCreators(appActions, dispatch),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Header);
