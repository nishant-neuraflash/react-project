import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
// components
import Table from 'custom_react_table';
// helpers
import { environment } from '../../../helpers/helper';

class LmdTable extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.commLink = 'https://samsonportal.force.com/connect/s/'; // production
    this.fileLink = 'https://samsonportal.force.com/connect/sfc/servlet.shepherd/version/download/';
    if (environment() !== 'production') {
      this.commLink = 'https://sambox-samsonportal.cs21.force.com/chevron/s/'; // sandbox
      this.fileLink = 'https://sambox-samsonportal.cs21.force.com/chevron/sfc/servlet.shepherd/version/download/';
    }
    this.columns = [
      {
        id: 'RecordType.Name',
        head: {
          type: 'TEXT',
          info: { label: 'WORK TYPE' },
        },
        body: {
          type: 'LINK',
          info: { link: `${this.commLink}detail/`, linkId: 'Work_Order__c' },
        },
      },
      {
        id: 'Inspection_Date__c',
        head: {
          type: 'TEXT',
          info: { label: 'EVENT DATE' },
        },
        body: {
          type: 'DATE_TIME',
          info: { dateTimeFormat: 'll' },
        },
      },
      {
        id: 'Support_Status__c',
        head: {
          type: 'TEXT',
          info: { label: 'SUPPORT STATUS' },
        },
        body: {
          type: 'TEXT',
          info: {},
        },
      },
      {
        id: 'Work_Order__r.Facilitating_Party__c',
        head: {
          type: 'TEXT',
          info: { label: 'FACILITATING PARTY' },
        },
        body: {
          type: 'TEXT',
          info: {},
        },
      },
      {
        id: 'Work_Order__r.Event_Type__c',
        head: {
          type: 'TEXT',
          info: { label: 'EVENT TYPE' },
        },
        body: {
          type: 'TEXT',
          info: {},
        },
      },
      {
        id: 'Work_Order__r.WorkType.Name',
        head: {
          type: 'TEXT',
          info: { label: 'DESCRIPTION' },
        },
        body: {
          type: 'TEXT',
          info: {},
        },
      },
      // {
      //   id: 'Work_Order__r.Rout_Insp_href__c',
      //   head: {
      //     type: 'TEXT_CENTER',
      //     info: { label: 'EVENT REPORT' },
      //   },
      //   body: {
      //     type: 'ICON_LINK',
      //     info: {
      //       link: this.fileLink,
      //       iconUrl: `${props.dataReducer.path}HOME_Invoices_CYAN_60_70px.png`,
      //       linkId: 'Work_Order__r.Rout_Insp_href__c',
      //     },
      //   },
      // },
      {
        id: 'hasReport',
        head: {
          type: 'TEXT_CENTER',
          info: { label: 'EVENT REPORT' },
        },
        body: {
          type: 'ICON',
          info: {
            iconUrl: `${props.dataReducer.path}HOME_Invoices_CYAN_60_70px.png`,
            handleClick: this.handleEventReportClick,
            linkId: 'Work_Order__c',
          },
        },
      },
    ];
  }

  // handleWorkOrderClick = (event, id) => {
  //   window.parent.location.href = `${this.commLink}detail/${id}`;
  // };

  handleEventReportClick = (row) => {
    window.parent.location.href = `${this.commLink}connect-reports?assetId=${row.Asset__c}&workOrderId=${row.Work_Order__c}`;
  };

  render() {
    const { lmdsAll, cdls } = this.props.lmdReducer;
    const { selectedRps } = this.props.rpsReducerSel;
    let data = [];
    if (lmdsAll && selectedRps) {
      /* eslint no-param-reassign: ["error", { "props": false }] */
      data = lmdsAll.filter(x => x.Rope_Product_Spec__c === selectedRps.Id && x.RecordType.Name !== 'Asset Activity');
      data.forEach((d) => { d.hasReport = cdls.some(x => x.LinkedEntityId === d.Work_Order__c); });
    }
    return (
      <Table
        columns={this.columns}
        data={data}
        selectedRow={{}}
        width={1020}
        height={150}
        order="desc"
        orderBy="Inspection_Date__c"
      />);
  }
}


LmdTable.propTypes = {
  lmdReducer: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
  rpsReducerSel: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
  dataReducer: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
};

const mapStateToProps = state => ({
  lmdReducer: state.lmdReducer,
  rpsReducerSel: state.rpsReducerSel,
  dataReducer: state.dataReducer,
});

export default connect(mapStateToProps, null)(LmdTable);
