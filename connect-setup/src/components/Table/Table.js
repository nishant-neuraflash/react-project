import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

// styles
import styles from './Table.css';
// helpers
import { getSvgIcon } from '../Icons/Icon';

function getProperty(propertyName, object) {
  if (!propertyName) return '';

  const parts = propertyName.split('.');
  const partLength = parts.length;
  let property = object;

  for (let i = 0; i < partLength; i++) {
    property = property[parts[i]];
    if (!property) return '';
    if (Array.isArray(property)) {
      if (property.length === 0) return '';
      [property] = property;
      if (!property) return '';
    }
  }
  return property;
}

// Line_Maintenance_Details__r.Work_Order__r.WorkType.Name

function getCell(d, indexRow, column, indexCol) {
  const value = getProperty(column.id, d);
  const linkValue = getProperty(column.linkId, d);
  const key = `${indexRow}-${indexCol}`;

  switch (column.row_type) {
    case 'button':
      return (
        <td className={styles.tdStyle} key={`${indexRow}-${indexCol}`} style={{ width: column.width }}>
          <button
            key={key}
            className={styles.buttonLinkStyle}
            onClick={e => column.handleClick(e, d.Id)}
          >
            {value}
          </button>
        </td>
      );
    case 'link': {
      const link = `${column.link}${getProperty(column.linkId, d)}`;
      return (
        <td className={styles.tdStyle} key={`${indexRow}-${indexCol}`} style={{ width: column.width }}>
          <a key={key} href={link}>
            {value}
          </a>
        </td>
      );
    }
    case 'date':
      return (
        <td className={styles.tdStyle} key={key} style={{ width: column.width }}>
          {moment.utc(value).format(column.dateTimeFormat)}
        </td>
      );
    case 'icon': {
      return (
        <td
          className={styles.tdStyle}
          key={key}
          style={{ width: column.width, textAlign: column.textAlign }}
        >
          {value &&
            <div onClick={e => column.handleClick(e, linkValue)}>
              <img src={`${column.path}${column.icon_row}`} alt={column.icon_row} height="20" width="20" style={{ cursor: 'pointer' }} />
            </div>}
        </td>
      );
    }
    case 'svgIcon':
      return (
        <td className={styles.tdStyle} key={key} style={{ width: column.width }}>
          {value && getSvgIcon({
            name: column.icon_row,
            key,
            fill: value,
            id: d.Id,
            handleClick: column.handleClick,
          })}
        </td>
      );
    case 'number':
      return (
        <td className={styles.tdNumberStyle} key={key} style={{ width: column.width }}>
          {value}
        </td>
      );
    default:
      return (
        <td className={styles.tdStyle} key={key} style={{ width: column.width }}>
          {value}
        </td>
      );
  }
}

function getHeaderCell(column, orderBy, order) {
  if (column.head_type === 'icon') {
    return (
      <img src={`${column.path}${column.icon_head}`} alt={column.icon_head} height="20" width="20" title={column.title} />
    );
  }
  if (column.head_type === 'svgIcon') {
    return (getSvgIcon({ type: column.icon_head }));
  }
  if (orderBy === column.id) {
    return column.label + (order === 'desc' ? ' 🔽' : ' 🔼');
  }
  return column.label;
}

function getSorted(data, orderBy, order) {
  if (order === 'desc') {
    return data.sort((a, b) => (getProperty(orderBy, b) < getProperty(orderBy, a) ? -1 : 1));
  }
  return data.sort((a, b) => (getProperty(orderBy, a) < getProperty(orderBy, b) ? -1 : 1));
}

class Table extends React.Component {
  constructor(props) {
    super(props);
    this.state = { order: props.order, orderBy: props.orderBy };
    this.sortedData = [];
  }

  componentDidUpdate() {
    if (!this.props.selectedRow) {
      return;
    }

    const count = this.sortedData.length;
    let i;
    let rowIndex;
    for (i = 0; i < count; i++) {
      if (this.sortedData[i].Id === this.props.selectedRow.Id) {
        rowIndex = i;
        break;
      }
    }

    if (rowIndex > 2) {
      this.bodyNode.scrollTop = (rowIndex - 2) * 25;
    } else {
      this.bodyNode.scrollTop = 0;
    }
  }

  // sort event handler
  handleSort = (event, column) => {
    if (column.type === 'icon') return;

    const orderBy = column.id;
    const order = (this.state.orderBy === orderBy && this.state.order === 'asc') ? 'desc' : 'asc';
    this.setState({ order, orderBy });
  };

  render() {
    this.sortedData = getSorted(this.props.data, this.state.orderBy, this.state.order);
    return (
      <div className={styles.cardStyle}>
        <table className={styles.tableStyle}>
          <thead className={styles.theadStyle}>
            <tr className={styles.trStyle}>
              {this.props.columnData.map(column => (
                <th
                  className={styles.thStyle}
                  key={column.id}
                  onClick={e => this.handleSort(e, column)}
                  style={{ minWidth: column.width, maxWidth: column.width }}
                  unselectable="on"
                >
                  {getHeaderCell(column, this.state.orderBy, this.state.order)}
                </th>
              ))
              }
            </tr>
          </thead>
          <tbody
            ref={(node) => { this.bodyNode = node; }}
            style={{
              display: 'block',
              width: '100%',
              height: `${this.props.height}px`,
              overflow: 'auto',
            }}
          >
            {this.sortedData.map((d, indexRow) => (
              <tr
                className={
                  (this.props.selectedRow !== null && d.Id === this.props.selectedRow.Id) ?
                  styles.selectedRowStyle : styles.trStyle}
                key={indexRow}
              >
                {this.props.columnData.map((column, indexCol) =>
                  getCell(d, indexRow, column, indexCol))}
              </tr>
              ))
            }
          </tbody>
        </table>
      </div>
    );
  }
}

Table.defaultProps = {
  order: 'asc',
  orderBy: 'Id',
};

Table.propTypes = {
  columnData: PropTypes.array.isRequired, // eslint-disable-line react/forbid-prop-types
  data: PropTypes.array.isRequired, // eslint-disable-line react/forbid-prop-types
  selectedRow: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
  height: PropTypes.string.isRequired,
  order: PropTypes.string,
  orderBy: PropTypes.string,
};

export default Table;
