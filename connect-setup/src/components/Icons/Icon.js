import React from 'react';

export function getSvgIcon(obj) {
  switch (obj.name) {
    case 'circle':
      return (
        <svg key={obj.key} height="20" width="20">
          <circle key={obj.key} cx="10" cy="10" r="10" fill={obj.fill} />
        </svg>
      );
    default:
      return (
        <div />
      );
  }
}

export function dummy() {
  return '';
}
